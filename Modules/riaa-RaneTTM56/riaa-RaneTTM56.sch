EESchema Schematic File Version 4
LIBS:riaa-RaneTTM56-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C18
U 1 1 5A219380
P 2450 1750
F 0 "C18" H 2475 1850 50  0000 L CNN
F 1 "47P" H 2475 1650 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2488 1600 50  0001 C CNN
F 3 "" H 2450 1750 50  0000 C CNN
	1    2450 1750
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:CP-Device-riaav3-rescue C4
U 1 1 5A219386
P 2750 2900
F 0 "C4" H 2775 3000 50  0000 L CNN
F 1 "100u" H 2775 2800 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 2788 2750 50  0001 C CNN
F 3 "" H 2750 2900 50  0000 C CNN
	1    2750 2900
	-1   0    0    1   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R9
U 1 1 5A21938C
P 3200 2350
F 0 "R9" V 3280 2350 50  0000 C CNN
F 1 "91k" V 3200 2350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 3130 2350 50  0001 C CNN
F 3 "" H 3200 2350 50  0000 C CNN
	1    3200 2350
	0    1    1    0   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R20
U 1 1 5A2193AA
P 5800 2300
F 0 "R20" H 5870 2346 50  0000 L CNN
F 1 "20k" H 5870 2255 50  0000 L CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 5730 2300 50  0001 C CNN
F 3 "" H 5800 2300 50  0000 C CNN
	1    5800 2300
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C6
U 1 1 5A2193B0
P 3150 2750
F 0 "C6" H 3175 2850 50  0000 L CNN
F 1 "33n" H 3175 2650 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3188 2600 50  0001 C CNN
F 3 "" H 3150 2750 50  0000 C CNN
	1    3150 2750
	0    1    1    0   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C12
U 1 1 5A2193C2
P 5600 1850
F 0 "C12" H 5625 1950 50  0000 L CNN
F 1 "22u" H 5625 1750 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 5638 1700 50  0001 C CNN
F 3 "" H 5600 1850 50  0000 C CNN
	1    5600 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3500 2750 3400 2750
Wire Wire Line
	3000 2750 3000 2350
Wire Wire Line
	2750 2350 3000 2350
Wire Wire Line
	3850 2750 3800 2750
Wire Wire Line
	3850 2350 3800 2350
Connection ~ 3850 2350
Wire Wire Line
	5750 1850 5800 1850
Wire Wire Line
	5800 1850 5800 2150
Connection ~ 1750 1500
Text Label 1600 1500 0    60   ~ 0
inL
Wire Wire Line
	2750 1700 2750 2350
Wire Wire Line
	2750 1700 3200 1700
Connection ~ 2750 2350
Connection ~ 3000 2350
Wire Wire Line
	3850 1600 3800 1600
Connection ~ 5800 1850
Text Label 6150 1850 0    60   ~ 0
outL
Text Label 8800 5950 0    60   ~ 0
+15v
Text Label 9000 6500 0    60   ~ 0
-15
$Comp
L riaa-RaneTTM56-rescue:PWR_FLAG-power-riaav3-rescue #FLG?
U 1 1 5A219422
P 1750 1500
F 0 "#FLG?" H 1750 1595 50  0001 C CNN
F 1 "PWR_FLAG" H 1750 1680 50  0000 C CNN
F 2 "" H 1750 1500 50  0000 C CNN
F 3 "" H 1750 1500 50  0000 C CNN
	1    1750 1500
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:PWR_FLAG-power-riaav3-rescue #FLG?
U 1 1 5A21942E
P 6800 5850
F 0 "#FLG?" H 6800 5945 50  0001 C CNN
F 1 "PWR_FLAG" H 6800 6030 50  0000 C CNN
F 2 "" H 6800 5850 50  0000 C CNN
F 3 "" H 6800 5850 50  0000 C CNN
	1    6800 5850
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R6
U 1 1 5A21943A
P 9300 5950
F 0 "R6" V 9380 5950 50  0000 C CNN
F 1 "2.2" V 9300 5950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 9230 5950 50  0001 C CNN
F 3 "" H 9300 5950 50  0000 C CNN
	1    9300 5950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 5850 6800 5950
Connection ~ 6800 5950
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R8
U 1 1 5A219443
P 9350 6350
F 0 "R8" V 9430 6350 50  0000 C CNN
F 1 "2.2" V 9350 6350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 9280 6350 50  0001 C CNN
F 3 "" H 9350 6350 50  0000 C CNN
	1    9350 6350
	0    -1   -1   0   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:Conn_01x03-riaav3-rescue P2
U 1 1 5A21944A
P 10250 5950
F 0 "P2" H 10250 6150 50  0000 C CNN
F 1 "CONN_01X03" V 10350 5950 50  0000 C CNN
F 2 "resistance-grospad:connection-3-padcarre2mm" H 10250 5950 50  0001 C CNN
F 3 "" H 10250 5950 50  0000 C CNN
	1    10250 5950
	1    0    0    1   
$EndComp
Text Notes 6450 5550 0    60   ~ 0
R14 et R13 -> 100k -> +36dB\n\n5mv en entrée -> ~500mV en sortie
Text Notes 8450 1200 0    60   ~ 0
Edit:\n1/01/19: R13 et 514 100k -> 47k\n
$Comp
L riaa-RaneTTM56-rescue:Conn_01x02-riaav3-rescue P3
U 1 1 5C2B43BF
P 7550 1850
F 0 "P3" H 7550 1950 50  0000 C CNN
F 1 "Conn_01x02" H 7550 1650 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 7550 1850 50  0001 C CNN
F 3 "" H 7550 1850 50  0001 C CNN
	1    7550 1850
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:Conn_01x02-riaav3-rescue P1
U 1 1 5C2B4EAA
P 850 1500
F 0 "P1" H 850 1600 50  0000 C CNN
F 1 "Conn_01x02" H 850 1300 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 850 1500 50  0001 C CNN
F 3 "" H 850 1500 50  0001 C CNN
	1    850  1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9900 6150 9950 6150
Wire Wire Line
	8400 6000 8400 5950
Connection ~ 8400 5950
$Comp
L riaa-RaneTTM56-rescue:CP-Device-riaav3-rescue C11
U 1 1 5C2DE234
P 8400 6150
F 0 "C11" H 8425 6250 50  0000 L CNN
F 1 "10u" H 8425 6050 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 8438 6000 50  0001 C CNN
F 3 "" H 8400 6150 50  0000 C CNN
	1    8400 6150
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:CP-Device-riaav3-rescue C13
U 1 1 5C2DFB69
P 8750 6400
F 0 "C13" H 8775 6500 50  0000 L CNN
F 1 "10u" H 8775 6300 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 8788 6250 50  0001 C CNN
F 3 "" H 8750 6400 50  0000 C CNN
	1    8750 6400
	-1   0    0    1   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C9
U 1 1 5C2E00AD
P 7350 6100
F 0 "C9" H 7375 6200 50  0000 L CNN
F 1 "100n" H 7375 6000 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 7388 5950 50  0001 C CNN
F 3 "" H 7350 6100 50  0000 C CNN
	1    7350 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 6050 9950 6050
Wire Wire Line
	9950 6050 9950 6150
Connection ~ 9950 6150
Wire Wire Line
	3850 2350 3850 2750
Wire Wire Line
	6750 5950 6800 5950
Wire Wire Line
	2750 2350 2750 2400
Wire Wire Line
	3000 2350 3050 2350
Wire Wire Line
	5800 1850 7350 1850
Wire Wire Line
	6800 5950 7350 5950
Wire Wire Line
	9950 6150 9950 6350
Wire Wire Line
	1750 1500 1750 1600
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5C5785C9
P 5800 2600
F 0 "#PWR?" H 5800 2350 50  0001 C CNN
F 1 "GNDA" H 5805 2427 50  0000 C CNN
F 2 "" H 5800 2600 50  0001 C CNN
F 3 "" H 5800 2600 50  0001 C CNN
	1    5800 2600
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5C57D2FB
P 7350 1950
F 0 "#PWR?" H 7350 1700 50  0001 C CNN
F 1 "GNDA" H 7355 1777 50  0000 C CNN
F 2 "" H 7350 1950 50  0001 C CNN
F 3 "" H 7350 1950 50  0001 C CNN
	1    7350 1950
	0    1    1    0   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:TL072-Amplifier_Operational-riaav3-rescue U1
U 3 1 5C582D5B
P 6900 6250
F 0 "U1" H 6858 6296 50  0000 L CNN
F 1 "TL072" H 6858 6205 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6900 6250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 6900 6250 50  0001 C CNN
	3    6900 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 6350 9200 6350
Wire Wire Line
	9450 5950 10050 5950
Wire Wire Line
	9500 6350 9950 6350
Wire Wire Line
	9000 6350 9000 6550
Connection ~ 7350 5950
Wire Wire Line
	8400 5950 9150 5950
Connection ~ 8750 6550
Wire Wire Line
	8750 6550 9000 6550
Wire Wire Line
	6800 6550 7350 6550
Connection ~ 7350 6550
Wire Wire Line
	7350 6250 7350 6550
Wire Wire Line
	5450 1850 3850 1850
Wire Wire Line
	3850 1600 3850 1850
Connection ~ 3850 1850
Wire Wire Line
	3850 1850 3850 2350
Wire Wire Line
	2450 1500 2450 1600
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R2
U 1 1 5A219374
P 1750 1750
F 0 "R2" V 1830 1750 50  0000 C CNN
F 1 "47k" V 1750 1750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 1680 1750 50  0001 C CNN
F 3 "" H 1750 1750 50  0000 C CNN
	1    1750 1750
	-1   0    0    1   
$EndComp
$Comp
L Device:L L1
U 1 1 5E5155A7
P 2250 1500
F 0 "L1" V 2069 1500 50  0000 C CNN
F 1 "L" V 2160 1500 50  0000 C CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2250 1500 50  0001 C CNN
F 3 "~" H 2250 1500 50  0001 C CNN
	1    2250 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	2400 1500 2450 1500
Connection ~ 2450 1500
Wire Wire Line
	2450 1500 3200 1500
Wire Wire Line
	1750 1500 2000 1500
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E5312CF
P 1050 1600
F 0 "#PWR?" H 1050 1350 50  0001 C CNN
F 1 "GNDA" H 1055 1427 50  0000 C CNN
F 2 "" H 1050 1600 50  0001 C CNN
F 3 "" H 1050 1600 50  0001 C CNN
	1    1050 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 1500 1750 1500
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E53A365
P 1750 1900
F 0 "#PWR?" H 1750 1650 50  0001 C CNN
F 1 "GNDA" H 1755 1727 50  0000 C CNN
F 2 "" H 1750 1900 50  0001 C CNN
F 3 "" H 1750 1900 50  0001 C CNN
	1    1750 1900
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E53D156
P 2450 1900
F 0 "#PWR?" H 2450 1650 50  0001 C CNN
F 1 "GNDA" H 2455 1727 50  0000 C CNN
F 2 "" H 2450 1900 50  0001 C CNN
F 3 "" H 2450 1900 50  0001 C CNN
	1    2450 1900
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R5
U 1 1 5A21937A
P 2750 2550
F 0 "R5" V 2830 2550 50  0000 C CNN
F 1 "162" H 2750 2550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2680 2550 50  0001 C CNN
F 3 "" H 2750 2550 50  0000 C CNN
	1    2750 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	2750 2700 2750 2750
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E55383C
P 2750 3050
F 0 "#PWR?" H 2750 2800 50  0001 C CNN
F 1 "GNDA" H 2755 2877 50  0000 C CNN
F 2 "" H 2750 3050 50  0001 C CNN
F 3 "" H 2750 3050 50  0001 C CNN
	1    2750 3050
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R10
U 1 1 5E556506
P 3650 2350
F 0 "R10" V 3730 2350 50  0000 C CNN
F 1 "7K5" V 3650 2350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 3580 2350 50  0001 C CNN
F 3 "" H 3650 2350 50  0000 C CNN
	1    3650 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 2350 3400 2350
Wire Wire Line
	3400 2350 3400 2750
Connection ~ 3400 2350
Wire Wire Line
	3400 2350 3350 2350
Connection ~ 3400 2750
Wire Wire Line
	3400 2750 3300 2750
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C7
U 1 1 5E5591D7
P 3650 2750
F 0 "C7" H 3675 2850 50  0000 L CNN
F 1 "10n" H 3675 2650 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3688 2600 50  0001 C CNN
F 3 "" H 3650 2750 50  0000 C CNN
	1    3650 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 2600 5800 2450
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C19
U 1 1 5E563507
P 2450 3700
F 0 "C19" H 2475 3800 50  0000 L CNN
F 1 "47P" H 2475 3600 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2488 3550 50  0001 C CNN
F 3 "" H 2450 3700 50  0000 C CNN
	1    2450 3700
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:CP-Device-riaav3-rescue C3
U 1 1 5E563511
P 2750 4850
F 0 "C3" H 2775 4950 50  0000 L CNN
F 1 "100u" H 2775 4750 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 2788 4700 50  0001 C CNN
F 3 "" H 2750 4850 50  0000 C CNN
	1    2750 4850
	-1   0    0    1   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R7
U 1 1 5E56351B
P 3200 4300
F 0 "R7" V 3280 4300 50  0000 C CNN
F 1 "91k" V 3200 4300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 3130 4300 50  0001 C CNN
F 3 "" H 3200 4300 50  0000 C CNN
	1    3200 4300
	0    1    1    0   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R17
U 1 1 5E563525
P 5800 4250
F 0 "R17" H 5870 4296 50  0000 L CNN
F 1 "20k" H 5870 4205 50  0000 L CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 5730 4250 50  0001 C CNN
F 3 "" H 5800 4250 50  0000 C CNN
	1    5800 4250
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C5
U 1 1 5E56352F
P 3150 4700
F 0 "C5" H 3175 4800 50  0000 L CNN
F 1 "33n" H 3175 4600 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3188 4550 50  0001 C CNN
F 3 "" H 3150 4700 50  0000 C CNN
	1    3150 4700
	0    1    1    0   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C10
U 1 1 5E563539
P 5600 3800
F 0 "C10" H 5625 3900 50  0000 L CNN
F 1 "22u" H 5625 3700 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 5638 3650 50  0001 C CNN
F 3 "" H 5600 3800 50  0000 C CNN
	1    5600 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3500 4700 3400 4700
Wire Wire Line
	3000 4700 3000 4300
Wire Wire Line
	2750 4300 3000 4300
Wire Wire Line
	3850 4700 3800 4700
Wire Wire Line
	3850 4300 3800 4300
Connection ~ 3850 4300
Wire Wire Line
	5750 3800 5800 3800
Wire Wire Line
	5800 3800 5800 4100
Connection ~ 1750 3450
Wire Wire Line
	2750 3650 2750 4300
Wire Wire Line
	2750 3650 3200 3650
Connection ~ 2750 4300
Connection ~ 3000 4300
Wire Wire Line
	3850 3550 3800 3550
Connection ~ 5800 3800
Text Label 6150 3800 0    60   ~ 0
outR
$Comp
L riaa-RaneTTM56-rescue:PWR_FLAG-power-riaav3-rescue #FLG?
U 1 1 5E563554
P 1750 3450
F 0 "#FLG?" H 1750 3545 50  0001 C CNN
F 1 "PWR_FLAG" H 1750 3630 50  0000 C CNN
F 2 "" H 1750 3450 50  0000 C CNN
F 3 "" H 1750 3450 50  0000 C CNN
	1    1750 3450
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:Conn_01x02-riaav3-rescue P5
U 1 1 5E56355E
P 7550 3800
F 0 "P5" H 7550 3900 50  0000 C CNN
F 1 "Conn_01x02" H 7550 3600 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 7550 3800 50  0001 C CNN
F 3 "" H 7550 3800 50  0001 C CNN
	1    7550 3800
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:Conn_01x02-riaav3-rescue P4
U 1 1 5E563568
P 850 3450
F 0 "P4" H 850 3550 50  0000 C CNN
F 1 "Conn_01x02" H 850 3250 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 850 3450 50  0001 C CNN
F 3 "" H 850 3450 50  0001 C CNN
	1    850  3450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 4300 3850 4700
Wire Wire Line
	2750 4300 2750 4350
Wire Wire Line
	3000 4300 3050 4300
Wire Wire Line
	5800 3800 7350 3800
Wire Wire Line
	1750 3450 1750 3550
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E563577
P 5800 4550
F 0 "#PWR?" H 5800 4300 50  0001 C CNN
F 1 "GNDA" H 5805 4377 50  0000 C CNN
F 2 "" H 5800 4550 50  0001 C CNN
F 3 "" H 5800 4550 50  0001 C CNN
	1    5800 4550
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E563581
P 7350 3900
F 0 "#PWR?" H 7350 3650 50  0001 C CNN
F 1 "GNDA" H 7355 3727 50  0000 C CNN
F 2 "" H 7350 3900 50  0001 C CNN
F 3 "" H 7350 3900 50  0001 C CNN
	1    7350 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 3800 3850 3800
Wire Wire Line
	3850 3550 3850 3800
Connection ~ 3850 3800
Wire Wire Line
	3850 3800 3850 4300
Wire Wire Line
	2450 3450 2450 3550
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R1
U 1 1 5E56359A
P 1750 3700
F 0 "R1" V 1830 3700 50  0000 C CNN
F 1 "47k" V 1750 3700 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 1680 3700 50  0001 C CNN
F 3 "" H 1750 3700 50  0000 C CNN
	1    1750 3700
	-1   0    0    1   
$EndComp
$Comp
L Device:L L2
U 1 1 5E5635A4
P 2250 3450
F 0 "L2" V 2069 3450 50  0000 C CNN
F 1 "L" V 2160 3450 50  0000 C CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2250 3450 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	2400 3450 2450 3450
Connection ~ 2450 3450
Wire Wire Line
	2450 3450 3200 3450
Wire Wire Line
	1750 3450 2000 3450
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E5635B2
P 1050 3550
F 0 "#PWR?" H 1050 3300 50  0001 C CNN
F 1 "GNDA" H 1055 3377 50  0000 C CNN
F 2 "" H 1050 3550 50  0001 C CNN
F 3 "" H 1050 3550 50  0001 C CNN
	1    1050 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 3450 1750 3450
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E5635BD
P 1750 3850
F 0 "#PWR?" H 1750 3600 50  0001 C CNN
F 1 "GNDA" H 1755 3677 50  0000 C CNN
F 2 "" H 1750 3850 50  0001 C CNN
F 3 "" H 1750 3850 50  0001 C CNN
	1    1750 3850
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E5635C7
P 2450 3850
F 0 "#PWR?" H 2450 3600 50  0001 C CNN
F 1 "GNDA" H 2455 3677 50  0000 C CNN
F 2 "" H 2450 3850 50  0001 C CNN
F 3 "" H 2450 3850 50  0001 C CNN
	1    2450 3850
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R3
U 1 1 5E5635D1
P 2750 4500
F 0 "R3" V 2830 4500 50  0000 C CNN
F 1 "162" H 2750 4500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2680 4500 50  0001 C CNN
F 3 "" H 2750 4500 50  0000 C CNN
	1    2750 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	2750 4650 2750 4700
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E5635DC
P 2750 5000
F 0 "#PWR?" H 2750 4750 50  0001 C CNN
F 1 "GNDA" H 2755 4827 50  0000 C CNN
F 2 "" H 2750 5000 50  0001 C CNN
F 3 "" H 2750 5000 50  0001 C CNN
	1    2750 5000
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:R-Device-riaav3-rescue R4
U 1 1 5E5635E6
P 3650 4300
F 0 "R4" V 3730 4300 50  0000 C CNN
F 1 "7K5" V 3650 4300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 3580 4300 50  0001 C CNN
F 3 "" H 3650 4300 50  0000 C CNN
	1    3650 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 4300 3400 4300
Wire Wire Line
	3400 4300 3400 4700
Connection ~ 3400 4300
Wire Wire Line
	3400 4300 3350 4300
Connection ~ 3400 4700
Wire Wire Line
	3400 4700 3300 4700
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C8
U 1 1 5E5635F6
P 3650 4700
F 0 "C8" H 3675 4800 50  0000 L CNN
F 1 "10n" H 3675 4600 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3688 4550 50  0001 C CNN
F 3 "" H 3650 4700 50  0000 C CNN
	1    3650 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 4550 5800 4400
$Comp
L riaa-RaneTTM56-rescue:TL072-Amplifier_Operational-riaav3-rescue U1
U 1 1 5C59670A
P 3500 1600
F 0 "U1" H 3500 1967 50  0000 C CNN
F 1 "TL072" H 3500 1876 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3500 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3500 1600 50  0001 C CNN
	1    3500 1600
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:TL072-Amplifier_Operational-riaav3-rescue U1
U 2 1 5E5712E2
P 3500 3550
F 0 "U1" H 3500 3917 50  0000 C CNN
F 1 "TL072" H 3500 3826 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3500 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3500 3550 50  0001 C CNN
	2    3500 3550
	1    0    0    -1  
$EndComp
Text Label 1600 3450 0    60   ~ 0
inR
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C2
U 1 1 5E57EEBF
P 2000 1750
F 0 "C2" H 2025 1850 50  0000 L CNN
F 1 "150p" H 2025 1650 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2038 1600 50  0001 C CNN
F 3 "" H 2000 1750 50  0000 C CNN
	1    2000 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1500 2000 1600
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E57EECA
P 2000 1900
F 0 "#PWR?" H 2000 1650 50  0001 C CNN
F 1 "GNDA" H 2005 1727 50  0000 C CNN
F 2 "" H 2000 1900 50  0001 C CNN
F 3 "" H 2000 1900 50  0001 C CNN
	1    2000 1900
	1    0    0    -1  
$EndComp
Connection ~ 2000 1500
Wire Wire Line
	2000 1500 2100 1500
$Comp
L riaa-RaneTTM56-rescue:C-Device-riaav3-rescue C1
U 1 1 5E581B6A
P 2000 3700
F 0 "C1" H 2025 3800 50  0000 L CNN
F 1 "47P" H 2025 3600 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2038 3550 50  0001 C CNN
F 3 "" H 2000 3700 50  0000 C CNN
	1    2000 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3450 2000 3550
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E581B75
P 2000 3850
F 0 "#PWR?" H 2000 3600 50  0001 C CNN
F 1 "GNDA" H 2005 3677 50  0000 C CNN
F 2 "" H 2000 3850 50  0001 C CNN
F 3 "" H 2000 3850 50  0001 C CNN
	1    2000 3850
	1    0    0    -1  
$EndComp
Connection ~ 2000 3450
Wire Wire Line
	2000 3450 2100 3450
Wire Wire Line
	7350 6550 8750 6550
Wire Wire Line
	7350 5950 8400 5950
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E51D969
P 10050 5850
F 0 "#PWR?" H 10050 5600 50  0001 C CNN
F 1 "GNDA" H 10055 5677 50  0000 C CNN
F 2 "" H 10050 5850 50  0001 C CNN
F 3 "" H 10050 5850 50  0001 C CNN
	1    10050 5850
	0    1    1    0   
$EndComp
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E524A7A
P 8400 6300
F 0 "#PWR?" H 8400 6050 50  0001 C CNN
F 1 "GNDA" H 8405 6127 50  0000 C CNN
F 2 "" H 8400 6300 50  0001 C CNN
F 3 "" H 8400 6300 50  0001 C CNN
	1    8400 6300
	1    0    0    -1  
$EndComp
$Comp
L riaa-RaneTTM56-rescue:GNDA-power-riaav3-rescue #PWR?
U 1 1 5E525831
P 8750 6250
F 0 "#PWR?" H 8750 6000 50  0001 C CNN
F 1 "GNDA" H 8755 6077 50  0000 C CNN
F 2 "" H 8750 6250 50  0001 C CNN
F 3 "" H 8750 6250 50  0001 C CNN
	1    8750 6250
	-1   0    0    1   
$EndComp
$EndSCHEMATC
