EESchema Schematic File Version 4
LIBS:10band-constantQ-cache
EELAYER 29 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R105
U 1 1 5A53186E
P 1350 1750
F 0 "R105" V 1430 1750 50  0000 C CNN
F 1 "100k" V 1350 1750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1280 1750 50  0001 C CNN
F 3 "" H 1350 1750 50  0000 C CNN
	1    1350 1750
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R104
U 1 1 5A5318CF
P 2700 1600
F 0 "R104" V 2780 1600 50  0000 C CNN
F 1 "10k" V 2700 1600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 2630 1600 50  0001 C CNN
F 3 "" H 2700 1600 50  0000 C CNN
	1    2700 1600
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R102
U 1 1 5A5319D6
P 3250 800
F 0 "R102" V 3330 800 50  0000 C CNN
F 1 "10k" V 3250 800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3180 800 50  0001 C CNN
F 3 "" H 3250 800 50  0000 C CNN
	1    3250 800 
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R101
U 1 1 5A531A1C
P 3250 600
F 0 "R101" V 3330 600 50  0000 C CNN
F 1 "10" V 3250 600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3180 600 50  0001 C CNN
F 3 "" H 3250 600 50  0000 C CNN
	1    3250 600 
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R107
U 1 1 5A531A8B
P 3950 2150
F 0 "R107" V 4030 2150 50  0000 C CNN
F 1 "10k" V 3950 2150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3880 2150 50  0001 C CNN
F 3 "" H 3950 2150 50  0000 C CNN
	1    3950 2150
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R112
U 1 1 5A531ADB
P 4900 3300
F 0 "R112" V 4980 3300 50  0000 C CNN
F 1 "10k" V 4900 3300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4830 3300 50  0001 C CNN
F 3 "" H 4900 3300 50  0000 C CNN
	1    4900 3300
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R110
U 1 1 5A531F3B
P 5450 2800
F 0 "R110" V 5530 2800 50  0000 C CNN
F 1 "20k" V 5450 2800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5380 2800 50  0001 C CNN
F 3 "" H 5450 2800 50  0000 C CNN
	1    5450 2800
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R111
U 1 1 5A5320C9
P 4500 3000
F 0 "R111" V 4580 3000 50  0000 C CNN
F 1 "10" V 4500 3000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4430 3000 50  0001 C CNN
F 3 "" H 4500 3000 50  0000 C CNN
	1    4500 3000
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R109
U 1 1 5A532101
P 4500 2750
F 0 "R109" V 4580 2750 50  0000 C CNN
F 1 "10k" V 4500 2750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4430 2750 50  0001 C CNN
F 3 "" H 4500 2750 50  0000 C CNN
	1    4500 2750
	0    1    1    0   
$EndComp
Text GLabel 4650 3000 2    60   Input ~ 0
BST
Text GLabel 6050 3400 2    60   Output ~ 0
SIG
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R106
U 1 1 5A53227E
P 5250 2050
F 0 "R106" V 5450 2050 50  0000 C CNN
F 1 "100" V 5250 2050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5180 2050 50  0001 C CNN
F 3 "" H 5250 2050 50  0000 C CNN
	1    5250 2050
	0    1    1    0   
$EndComp
Text GLabel 5350 600  2    60   Input ~ 0
CUT
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR01
U 1 1 5A53263D
P 4300 1950
F 0 "#PWR01" H 4300 1700 50  0001 C CNN
F 1 "GND" H 4300 1800 50  0000 C CNN
F 2 "" H 4300 1950 50  0000 C CNN
F 3 "" H 4300 1950 50  0000 C CNN
	1    4300 1950
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR02
U 1 1 5A53267C
P 5250 3500
F 0 "#PWR02" H 5250 3250 50  0001 C CNN
F 1 "GND" H 5250 3350 50  0000 C CNN
F 2 "" H 5250 3500 50  0000 C CNN
F 3 "" H 5250 3500 50  0000 C CNN
	1    5250 3500
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR03
U 1 1 5A5326B4
P 3000 1800
F 0 "#PWR03" H 3000 1550 50  0001 C CNN
F 1 "GND" H 3000 1650 50  0000 C CNN
F 2 "" H 3000 1800 50  0000 C CNN
F 3 "" H 3000 1800 50  0000 C CNN
	1    3000 1800
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C101
U 1 1 5A5328DE
P 950 1500
F 0 "C101" H 975 1600 50  0000 L CNN
F 1 "C" H 975 1400 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 988 1350 50  0001 C CNN
F 3 "" H 950 1500 50  0000 C CNN
	1    950  1500
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR04
U 1 1 5A53292E
P 1350 2100
F 0 "#PWR04" H 1350 1850 50  0001 C CNN
F 1 "GND" H 1350 1950 50  0000 C CNN
F 2 "" H 1350 2100 50  0000 C CNN
F 3 "" H 1350 2100 50  0000 C CNN
	1    1350 2100
	1    0    0    -1  
$EndComp
Text GLabel 2050 1300 1    60   Input ~ 0
v+
Text GLabel 2050 1900 3    60   Input ~ 0
v-
NoConn ~ 5450 3700
NoConn ~ 5450 3100
$Comp
L 10band-constantQ-rescue:CONN_01X03-10band-constantQ-rescue P103
U 1 1 5A532F1D
P 250 3100
F 0 "P103" H 250 3300 50  0000 C CNN
F 1 "CONN_01X03" V 350 3100 50  0000 C CNN
F 2 "resistance-grospad:connection-3-padcarre2mm" H 250 3100 50  0001 C CNN
F 3 "" H 250 3100 50  0000 C CNN
	1    250  3100
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C102
U 1 1 5A5330C1
P 1400 2950
F 0 "C102" H 1425 3050 50  0000 L CNN
F 1 "C" H 1425 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 1438 2800 50  0001 C CNN
F 3 "" H 1400 2950 50  0000 C CNN
	1    1400 2950
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C103
U 1 1 5A53315F
P 1600 2950
F 0 "C103" H 1625 3050 50  0000 L CNN
F 1 "C" H 1625 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 1638 2800 50  0001 C CNN
F 3 "" H 1600 2950 50  0000 C CNN
	1    1600 2950
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C104
U 1 1 5A5331A8
P 1800 2950
F 0 "C104" H 1825 3050 50  0000 L CNN
F 1 "C" H 1825 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 1838 2800 50  0001 C CNN
F 3 "" H 1800 2950 50  0000 C CNN
	1    1800 2950
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C105
U 1 1 5A5331F7
P 2000 2950
F 0 "C105" H 2025 3050 50  0000 L CNN
F 1 "C" H 2025 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2038 2800 50  0001 C CNN
F 3 "" H 2000 2950 50  0000 C CNN
	1    2000 2950
	-1   0    0    1   
$EndComp
Text GLabel 2900 2750 2    60   Input ~ 0
v-
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR05
U 1 1 5A5341BD
P 1200 3450
F 0 "#PWR05" H 1200 3200 50  0001 C CNN
F 1 "GND" H 1200 3300 50  0000 C CNN
F 2 "" H 1200 3450 50  0000 C CNN
F 3 "" H 1200 3450 50  0000 C CNN
	1    1200 3450
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:CONN_01X02-10band-constantQ-rescue P102
U 1 1 5A534504
P 5650 2100
F 0 "P102" H 5650 2250 50  0000 C CNN
F 1 "out" V 5750 2100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" H 5650 2100 50  0001 C CNN
F 3 "" H 5650 2100 50  0000 C CNN
	1    5650 2100
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR06
U 1 1 5A5345D1
P 5100 2250
F 0 "#PWR06" H 5100 2000 50  0001 C CNN
F 1 "GND" H 5100 2100 50  0000 C CNN
F 2 "" H 5100 2250 50  0000 C CNN
F 3 "" H 5100 2250 50  0000 C CNN
	1    5100 2250
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R108
U 1 1 5A534D9C
P 5300 2150
F 0 "R108" V 5380 2150 50  0000 C CNN
F 1 "75" V 5300 2150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5230 2150 50  0001 C CNN
F 3 "" H 5300 2150 50  0000 C CNN
	1    5300 2150
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:CONN_01X02-10band-constantQ-rescue P101
U 1 1 5A53525F
P 600 1550
F 0 "P101" H 600 1700 50  0000 C CNN
F 1 "in" V 700 1550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" H 600 1550 50  0001 C CNN
F 3 "" H 600 1550 50  0000 C CNN
	1    600  1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 1700 3700 1700
Wire Wire Line
	3700 2150 3800 2150
Wire Wire Line
	3700 800  3700 1700
Wire Wire Line
	3700 3300 4750 3300
Connection ~ 3700 1700
Wire Wire Line
	5050 3300 5200 3300
Wire Wire Line
	4100 2150 4250 2150
Wire Wire Line
	4250 2150 4250 2750
Wire Wire Line
	4250 2750 4350 2750
Connection ~ 4250 2150
Wire Wire Line
	4650 2750 5000 2750
Wire Wire Line
	5000 2750 5000 2050
Wire Wire Line
	4900 2050 5000 2050
Connection ~ 5000 2050
Wire Wire Line
	5450 2050 5400 2050
Wire Wire Line
	4250 3000 4350 3000
Connection ~ 4250 2750
Wire Wire Line
	5300 2800 5200 2800
Wire Wire Line
	5200 2800 5200 3300
Connection ~ 5200 3300
Wire Wire Line
	5850 3400 5900 3400
Wire Wire Line
	5600 2800 5900 2800
Wire Wire Line
	5900 2800 5900 3400
Connection ~ 5900 3400
Wire Wire Line
	2850 1600 2950 1600
Wire Wire Line
	2950 600  2950 800 
Wire Wire Line
	2950 800  3100 800 
Connection ~ 2950 1600
Wire Wire Line
	3100 600  2950 600 
Connection ~ 2950 800 
Wire Wire Line
	3400 800  3700 800 
Wire Wire Line
	3400 600  5350 600 
Wire Wire Line
	1850 1700 1700 1700
Wire Wire Line
	1700 1700 1700 2150
Wire Wire Line
	1700 2150 2500 2150
Wire Wire Line
	2500 2150 2500 1600
Wire Wire Line
	2450 1600 2500 1600
Connection ~ 2500 1600
Wire Wire Line
	1100 1500 1350 1500
Wire Wire Line
	1350 1600 1350 1500
Connection ~ 1350 1500
Wire Wire Line
	2000 2750 2000 2800
Connection ~ 2000 3100
Wire Wire Line
	1800 2800 1800 2750
Wire Wire Line
	1600 2800 1600 2750
Connection ~ 1600 3100
Wire Wire Line
	1400 2800 1400 2750
Connection ~ 1800 3100
Wire Wire Line
	1200 3200 1200 3450
Wire Wire Line
	5150 2150 5100 2150
Wire Wire Line
	5100 2150 5100 2250
Wire Wire Line
	800  2000 1350 2000
Wire Wire Line
	1350 1900 1350 2000
Connection ~ 1350 2000
Wire Wire Line
	800  1600 800  2000
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U107
U 1 1 5A53C238
P 1800 5950
F 0 "U107" H 1750 6150 50  0000 L CNN
F 1 "TL072" H 1750 5700 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 1800 5950 50  0001 C CNN
F 3 "" H 1800 5950 50  0000 C CNN
	1    1800 5950
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R121
U 1 1 5A53C23E
P 1700 4900
F 0 "R121" V 1780 4900 50  0000 C CNN
F 1 "11k" V 1700 4900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1630 4900 50  0001 C CNN
F 3 "" H 1700 4900 50  0000 C CNN
	1    1700 4900
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R131
U 1 1 5A53C244
P 2000 5150
F 0 "R131" H 2080 5150 50  0000 C CNN
F 1 "1k5" V 2000 5150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1930 5150 50  0001 C CNN
F 3 "" H 2000 5150 50  0000 C CNN
	1    2000 5150
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R141
U 1 1 5A53C24A
P 1050 6100
F 0 "R141" V 1130 6100 50  0000 C CNN
F 1 "22k" V 1050 6100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 980 6100 50  0001 C CNN
F 3 "" H 1050 6100 50  0000 C CNN
	1    1050 6100
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R151
U 1 1 5A53C250
P 1800 6850
F 0 "R151" V 1880 6850 50  0000 C CNN
F 1 "10k" V 1800 6850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1730 6850 50  0001 C CNN
F 3 "" H 1800 6850 50  0000 C CNN
	1    1800 6850
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C127
U 1 1 5A53C256
P 1700 5400
F 0 "C127" H 1725 5500 50  0000 L CNN
F 1 "2n" H 1725 5300 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 1738 5250 50  0001 C CNN
F 3 "" H 1700 5400 50  0000 C CNN
	1    1700 5400
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C126
U 1 1 5A53C25C
P 850 5400
F 0 "C126" H 875 5500 50  0000 L CNN
F 1 "2n" H 875 5300 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 888 5250 50  0001 C CNN
F 3 "" H 850 5400 50  0000 C CNN
	1    850  5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 5050 1700 5150
Wire Wire Line
	850  5150 1700 5150
Connection ~ 1700 5150
Wire Wire Line
	850  5150 850  5250
Wire Wire Line
	850  6300 850  5550
Wire Wire Line
	850  6300 1050 6300
Wire Wire Line
	1800 6250 1800 6300
Wire Wire Line
	1050 6250 1050 6300
Connection ~ 1050 6300
Wire Wire Line
	1700 5550 1700 5600
Wire Wire Line
	1700 5600 1050 5600
Wire Wire Line
	1050 5600 1050 5950
Connection ~ 1700 5600
Wire Wire Line
	1900 5650 1900 5350
Wire Wire Line
	1900 5350 2250 5350
Wire Wire Line
	2250 5350 2250 5150
Wire Wire Line
	2250 4900 2200 4900
Wire Wire Line
	2150 5150 2250 5150
Connection ~ 2250 5150
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR07
U 1 1 5A53C275
P 2200 4900
F 0 "#PWR07" H 2200 4650 50  0001 C CNN
F 1 "GND" H 2200 4750 50  0000 C CNN
F 2 "" H 2200 4900 50  0000 C CNN
F 3 "" H 2200 4900 50  0000 C CNN
	1    2200 4900
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C138
U 1 1 5A53C27B
P 1800 6500
F 0 "C138" H 1825 6600 50  0000 L CNN
F 1 "10u" H 1825 6400 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 1838 6350 50  0001 C CNN
F 3 "" H 1800 6500 50  0000 C CNN
	1    1800 6500
	-1   0    0    1   
$EndComp
Connection ~ 1800 6300
Wire Wire Line
	1800 6700 1800 6650
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV109
U 1 1 5A53C283
P 1800 7150
F 0 "RV109" H 1800 7050 50  0000 C CNN
F 1 "100k" H 1800 7150 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 1800 7150 50  0001 C CNN
F 3 "" H 1800 7150 50  0000 C CNN
	1    1800 7150
	1    0    0    -1  
$EndComp
Text GLabel 2100 7400 3    60   Input ~ 0
BST
Text GLabel 1500 7400 3    60   Input ~ 0
CUT
Wire Wire Line
	2100 7400 2100 7150
Wire Wire Line
	2100 7150 2050 7150
Wire Wire Line
	1550 7150 1500 7150
Wire Wire Line
	1500 7150 1500 7400
Text GLabel 1500 5850 0    60   Input ~ 0
v-
Text GLabel 2100 5850 2    60   Input ~ 0
v+
Text GLabel 1700 4750 1    60   Input ~ 0
SIG
Connection ~ 3700 2150
$Comp
L 10band-constantQ-rescue:PWR_FLAG-power-10band-constantQ-rescue #FLG018
U 1 1 5A54813A
P 2850 3100
F 0 "#FLG018" H 2850 3195 50  0001 C CNN
F 1 "PWR_FLAG" H 2850 3280 50  0000 C CNN
F 2 "" H 2850 3100 50  0000 C CNN
F 3 "" H 2850 3100 50  0000 C CNN
	1    2850 3100
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:PWR_FLAG-power-10band-constantQ-rescue #FLG019
U 1 1 5A548568
P 1350 2000
F 0 "#FLG019" H 1350 2095 50  0001 C CNN
F 1 "PWR_FLAG" H 1350 2180 50  0000 C CNN
F 2 "" H 1350 2000 50  0000 C CNN
F 3 "" H 1350 2000 50  0000 C CNN
	1    1350 2000
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C1
U 1 1 5A9BB239
P 2200 2950
F 0 "C1" H 2225 3050 50  0000 L CNN
F 1 "C" H 2225 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2238 2800 50  0001 C CNN
F 3 "" H 2200 2950 50  0000 C CNN
	1    2200 2950
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C2
U 1 1 5A9BB589
P 2600 2950
F 0 "C2" H 2625 3050 50  0000 L CNN
F 1 "C" H 2625 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2638 2800 50  0001 C CNN
F 3 "" H 2600 2950 50  0000 C CNN
	1    2600 2950
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C3
U 1 1 5A9BC088
P 2400 2950
F 0 "C3" H 2425 3050 50  0000 L CNN
F 1 "C" H 2425 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2438 2800 50  0001 C CNN
F 3 "" H 2400 2950 50  0000 C CNN
	1    2400 2950
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C4
U 1 1 5A9BC9A5
P 2750 2950
F 0 "C4" H 2775 3050 50  0000 L CNN
F 1 "C" H 2775 2850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2788 2800 50  0001 C CNN
F 3 "" H 2750 2950 50  0000 C CNN
	1    2750 2950
	-1   0    0    1   
$EndComp
Text Notes 1150 4650 0    60   ~ 0
16 kHz
Text Notes 900  -250 0    60   ~ 0
R110 -> Gain Boost/Cut\n39k -> +/-12dB\n10k -> +/-5dB\n
Wire Wire Line
	3700 1700 3700 2150
Wire Wire Line
	4250 2150 4300 2150
Wire Wire Line
	5000 2050 5100 2050
Wire Wire Line
	4250 2750 4250 3000
Wire Wire Line
	5200 3300 5250 3300
Wire Wire Line
	5900 3400 6050 3400
Wire Wire Line
	2950 1600 3000 1600
Wire Wire Line
	2950 800  2950 1600
Wire Wire Line
	2500 1600 2550 1600
Wire Wire Line
	1800 3100 2000 3100
Wire Wire Line
	1600 3100 1800 3100
Wire Wire Line
	1350 2000 1350 2100
Wire Wire Line
	1700 5150 1700 5250
Wire Wire Line
	1700 5150 1850 5150
Wire Wire Line
	1050 6300 1800 6300
Wire Wire Line
	1700 5600 1700 5650
Wire Wire Line
	2250 5150 2250 4900
Wire Wire Line
	1800 6300 1800 6350
Wire Wire Line
	3700 2150 3700 3300
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U2
U 1 1 5CD8F65C
P 2150 1600
F 0 "U2" H 2491 1646 50  0000 L CNN
F 1 "TL072-10band-constantQ-rescue" H 2491 1555 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 2150 1600 50  0001 C CNN
F 3 "" H 2150 1600 50  0001 C CNN
	1    2150 1600
	1    0    0    -1  
$EndComp
Text GLabel 3200 2000 3    60   Input ~ 0
v+
Text GLabel 3200 1400 1    60   Input ~ 0
v-
Wire Wire Line
	2000 3100 2200 3100
Connection ~ 2200 3100
Wire Wire Line
	2200 3100 2400 3100
Connection ~ 2400 3100
Wire Wire Line
	2400 3100 2600 3100
Connection ~ 2600 3100
Wire Wire Line
	2600 3100 2750 3100
Connection ~ 2750 3100
Wire Wire Line
	2750 3100 2850 3100
Wire Wire Line
	2900 2750 2750 2750
Connection ~ 1400 2750
Connection ~ 1600 2750
Wire Wire Line
	1600 2750 1400 2750
Connection ~ 1800 2750
Wire Wire Line
	1800 2750 1600 2750
Connection ~ 2000 2750
Wire Wire Line
	2000 2750 1800 2750
Wire Wire Line
	2200 2800 2200 2750
Connection ~ 2200 2750
Wire Wire Line
	2200 2750 2000 2750
Wire Wire Line
	2400 2800 2400 2750
Connection ~ 2400 2750
Wire Wire Line
	2400 2750 2200 2750
Wire Wire Line
	2600 2800 2600 2750
Connection ~ 2600 2750
Wire Wire Line
	2600 2750 2400 2750
Wire Wire Line
	2750 2800 2750 2750
Connection ~ 2750 2750
Wire Wire Line
	2750 2750 2600 2750
$Comp
L 10band-constantQ-rescue:PWR_FLAG-power-10band-constantQ-rescue #FLG0101
U 1 1 5D07A055
P 1800 2750
F 0 "#FLG0101" H 1800 2845 50  0001 C CNN
F 1 "PWR_FLAG" H 1800 2930 50  0000 C CNN
F 2 "" H 1800 2750 50  0000 C CNN
F 3 "" H 1800 2750 50  0000 C CNN
	1    1800 2750
	1    0    0    -1  
$EndComp
Connection ~ 2850 3100
Wire Wire Line
	2850 3100 2900 3100
Text GLabel 2900 3100 2    60   Input ~ 0
v+
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U1
U 1 1 5CD908B2
P 4600 2050
F 0 "U1" H 4941 2096 50  0000 L CNN
F 1 "TL072-10band-constantQ-rescue" H 4941 2005 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 4600 2050 50  0001 C CNN
F 3 "" H 4600 2050 50  0001 C CNN
	1    4600 2050
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U2
U 2 1 5CD8FA10
P 3300 1700
F 0 "U2" H 3641 1746 50  0000 L CNN
F 1 "TL072-10band-constantQ-rescue" H 3641 1655 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3300 1700 50  0001 C CNN
F 3 "" H 3300 1700 50  0001 C CNN
	2    3300 1700
	1    0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R153
U 1 1 5D29E37E
P 600 3000
F 0 "R153" V 680 3000 50  0000 C CNN
F 1 "2r2" V 600 3000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 530 3000 50  0001 C CNN
F 3 "" H 600 3000 50  0000 C CNN
	1    600  3000
	0    1    1    0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R154
U 1 1 5D29E4BA
P 600 3100
F 0 "R154" V 680 3100 50  0000 C CNN
F 1 "2r2" V 600 3100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 530 3100 50  0001 C CNN
F 3 "" H 600 3100 50  0000 C CNN
	1    600  3100
	0    1    1    0   
$EndComp
Wire Wire Line
	750  3000 1000 3000
Wire Wire Line
	1000 3000 1000 2750
Wire Wire Line
	1000 2750 1400 2750
Wire Wire Line
	750  3100 1400 3100
Connection ~ 1400 3100
Wire Wire Line
	1400 3100 1600 3100
Wire Wire Line
	450  3200 1200 3200
Wire Wire Line
	1350 1500 1550 1500
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R103
U 1 1 5A5317DD
P 1700 1500
F 0 "R103" V 1780 1500 50  0000 C CNN
F 1 "1k" V 1700 1500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1630 1500 50  0001 C CNN
F 3 "" H 1700 1500 50  0000 C CNN
	1    1700 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	16400 6100 16400 6150
Wire Wire Line
	16850 4950 16850 4700
Wire Wire Line
	16300 5400 16300 5450
Wire Wire Line
	15650 6100 16400 6100
Wire Wire Line
	16300 4950 16450 4950
Wire Wire Line
	16300 4950 16300 5050
Wire Wire Line
	14800 6100 14800 6150
Wire Wire Line
	15250 4950 15250 4700
Wire Wire Line
	14700 5400 14700 5450
Wire Wire Line
	14050 6100 14800 6100
Wire Wire Line
	14700 4950 14850 4950
Wire Wire Line
	14700 4950 14700 5050
Wire Wire Line
	13150 6150 13150 6200
Wire Wire Line
	13600 5000 13600 4750
Wire Wire Line
	13050 5450 13050 5500
Wire Wire Line
	12400 6150 13150 6150
Wire Wire Line
	13050 5000 13200 5000
Wire Wire Line
	13050 5000 13050 5100
Wire Wire Line
	11550 6150 11550 6200
Wire Wire Line
	12000 5000 12000 4750
Wire Wire Line
	11450 5450 11450 5500
Wire Wire Line
	10800 6150 11550 6150
Wire Wire Line
	11450 5000 11600 5000
Wire Wire Line
	11450 5000 11450 5100
Wire Wire Line
	9900 6200 9900 6250
Wire Wire Line
	10350 5050 10350 4800
Wire Wire Line
	9800 5500 9800 5550
Wire Wire Line
	9150 6200 9900 6200
Wire Wire Line
	9800 5050 9950 5050
Wire Wire Line
	9800 5050 9800 5150
Wire Wire Line
	8300 6200 8300 6250
Wire Wire Line
	8750 5050 8750 4800
Wire Wire Line
	8200 5500 8200 5550
Wire Wire Line
	7550 6200 8300 6200
Wire Wire Line
	8200 5050 8350 5050
Wire Wire Line
	8200 5050 8200 5150
Wire Wire Line
	6650 6250 6650 6300
Wire Wire Line
	7100 5100 7100 4850
Wire Wire Line
	6550 5550 6550 5600
Wire Wire Line
	5900 6250 6650 6250
Wire Wire Line
	6550 5100 6700 5100
Wire Wire Line
	6550 5100 6550 5200
Wire Wire Line
	5050 6250 5050 6300
Wire Wire Line
	5500 5100 5500 4850
Wire Wire Line
	4950 5550 4950 5600
Wire Wire Line
	4300 6250 5050 6250
Wire Wire Line
	4950 5100 5100 5100
Wire Wire Line
	4950 5100 4950 5200
Wire Wire Line
	3400 6300 3400 6350
Wire Wire Line
	3850 5150 3850 4900
Wire Wire Line
	3300 5600 3300 5650
Wire Wire Line
	2650 6300 3400 6300
Wire Wire Line
	3300 5150 3450 5150
Wire Wire Line
	3300 5150 3300 5250
Text Notes 2800 4650 0    60   ~ 0
10 kHz
Text Notes 4400 4600 0    60   ~ 0
6,3 kHz
Text Notes 5950 4550 0    60   ~ 0
2,5 kHz
Text Notes 7600 4500 0    60   ~ 0
1kHz\n
Text Notes 9100 4550 0    60   ~ 0
630Hz
Text Notes 10800 4500 0    60   ~ 0
260Hz
Text Notes 15650 4550 0    60   ~ 0
36Hz\n
Text Notes 12400 4500 0    60   ~ 0
130Hz
Text Notes 14000 4550 0    60   ~ 0
90Hz\n
Text GLabel 16300 4550 1    60   Input ~ 0
SIG
Text GLabel 16700 5650 2    60   Input ~ 0
v+
Text GLabel 16100 5650 0    60   Input ~ 0
v-
Wire Wire Line
	16100 6950 16100 7200
Wire Wire Line
	16150 6950 16100 6950
Wire Wire Line
	16700 6950 16650 6950
Wire Wire Line
	16700 7200 16700 6950
Text GLabel 16100 7200 3    60   Input ~ 0
CUT
Text GLabel 16700 7200 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV102
U 1 1 5A545E32
P 16400 6950
F 0 "RV102" H 16400 6850 50  0000 C CNN
F 1 "100k" H 16400 6950 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 16400 6950 50  0001 C CNN
F 3 "" H 16400 6950 50  0000 C CNN
	1    16400 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	16400 6500 16400 6450
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C131
U 1 1 5A545E2A
P 16400 6300
F 0 "C131" H 16425 6400 50  0000 L CNN
F 1 "10u" H 16425 6200 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 16438 6150 50  0001 C CNN
F 3 "" H 16400 6300 50  0000 C CNN
	1    16400 6300
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR016
U 1 1 5A545E24
P 16800 4700
F 0 "#PWR016" H 16800 4450 50  0001 C CNN
F 1 "GND" H 16800 4550 50  0000 C CNN
F 2 "" H 16800 4700 50  0000 C CNN
F 3 "" H 16800 4700 50  0000 C CNN
	1    16800 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	16750 4950 16850 4950
Wire Wire Line
	16850 4700 16800 4700
Connection ~ 16850 4950
Wire Wire Line
	16850 5150 16850 4950
Wire Wire Line
	16500 5150 16850 5150
Wire Wire Line
	16500 5450 16500 5150
Wire Wire Line
	15650 5400 15650 5750
Wire Wire Line
	16300 5400 15650 5400
Connection ~ 16300 5400
Wire Wire Line
	16300 5350 16300 5400
Wire Wire Line
	15650 6050 15650 6100
Connection ~ 16400 6100
Wire Wire Line
	16400 6050 16400 6100
Connection ~ 15650 6100
Wire Wire Line
	15450 6100 15650 6100
Wire Wire Line
	15450 6100 15450 5350
Wire Wire Line
	15450 4950 15450 5050
Connection ~ 16300 4950
Wire Wire Line
	15450 4950 16300 4950
Wire Wire Line
	16300 4850 16300 4950
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C112
U 1 1 5A545E0B
P 15450 5200
F 0 "C112" H 15475 5300 50  0000 L CNN
F 1 "330n" H 15475 5100 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 15488 5050 50  0001 C CNN
F 3 "" H 15450 5200 50  0000 C CNN
	1    15450 5200
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C113
U 1 1 5A545E05
P 16300 5200
F 0 "C113" H 16325 5300 50  0000 L CNN
F 1 "330n" H 16325 5100 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 16338 5050 50  0001 C CNN
F 3 "" H 16300 5200 50  0000 C CNN
	1    16300 5200
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R144
U 1 1 5A545DFF
P 16400 6650
F 0 "R144" V 16480 6650 50  0000 C CNN
F 1 "10k" V 16400 6650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 16330 6650 50  0001 C CNN
F 3 "" H 16400 6650 50  0000 C CNN
	1    16400 6650
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R134
U 1 1 5A545DF9
P 15650 5900
F 0 "R134" V 15730 5900 50  0000 C CNN
F 1 "47k" V 15650 5900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 15580 5900 50  0001 C CNN
F 3 "" H 15650 5900 50  0000 C CNN
	1    15650 5900
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R124
U 1 1 5A545DF3
P 16600 4950
F 0 "R124" H 16680 4950 50  0000 C CNN
F 1 "4k7" V 16600 4950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 16530 4950 50  0001 C CNN
F 3 "" H 16600 4950 50  0000 C CNN
	1    16600 4950
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R114
U 1 1 5A545DED
P 16300 4700
F 0 "R114" V 16380 4700 50  0000 C CNN
F 1 "22k" V 16300 4700 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 16230 4700 50  0001 C CNN
F 3 "" H 16300 4700 50  0000 C CNN
	1    16300 4700
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U103
U 2 1 5A545DE7
P 16400 5750
F 0 "U103" H 16350 5950 50  0000 L CNN
F 1 "TL072" H 16350 5500 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 16400 5750 50  0001 C CNN
F 3 "" H 16400 5750 50  0000 C CNN
	2    16400 5750
	0    1    1    0   
$EndComp
Text GLabel 14700 4550 1    60   Input ~ 0
SIG
Text GLabel 15100 5650 2    60   Input ~ 0
v+
Text GLabel 14500 5650 0    60   Input ~ 0
v-
Wire Wire Line
	14500 6950 14500 7200
Wire Wire Line
	14550 6950 14500 6950
Wire Wire Line
	15100 6950 15050 6950
Wire Wire Line
	15100 7200 15100 6950
Text GLabel 14500 7200 3    60   Input ~ 0
CUT
Text GLabel 15100 7200 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV101
U 1 1 5A545DD8
P 14800 6950
F 0 "RV101" H 14800 6850 50  0000 C CNN
F 1 "100k" H 14800 6950 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 14800 6950 50  0001 C CNN
F 3 "" H 14800 6950 50  0000 C CNN
	1    14800 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	14800 6500 14800 6450
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C130
U 1 1 5A545DD0
P 14800 6300
F 0 "C130" H 14825 6400 50  0000 L CNN
F 1 "10u" H 14825 6200 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 14838 6150 50  0001 C CNN
F 3 "" H 14800 6300 50  0000 C CNN
	1    14800 6300
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR015
U 1 1 5A545DCA
P 15200 4700
F 0 "#PWR015" H 15200 4450 50  0001 C CNN
F 1 "GND" H 15200 4550 50  0000 C CNN
F 2 "" H 15200 4700 50  0000 C CNN
F 3 "" H 15200 4700 50  0000 C CNN
	1    15200 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	15150 4950 15250 4950
Wire Wire Line
	15250 4700 15200 4700
Connection ~ 15250 4950
Wire Wire Line
	15250 5150 15250 4950
Wire Wire Line
	14900 5150 15250 5150
Wire Wire Line
	14900 5450 14900 5150
Wire Wire Line
	14050 5400 14050 5750
Wire Wire Line
	14700 5400 14050 5400
Connection ~ 14700 5400
Wire Wire Line
	14700 5350 14700 5400
Wire Wire Line
	14050 6050 14050 6100
Connection ~ 14800 6100
Wire Wire Line
	14800 6050 14800 6100
Connection ~ 14050 6100
Wire Wire Line
	13850 6100 14050 6100
Wire Wire Line
	13850 6100 13850 5350
Wire Wire Line
	13850 4950 13850 5050
Connection ~ 14700 4950
Wire Wire Line
	13850 4950 14700 4950
Wire Wire Line
	14700 4850 14700 4950
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C110
U 1 1 5A545DB1
P 13850 5200
F 0 "C110" H 13875 5300 50  0000 L CNN
F 1 "330n" H 13875 5100 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 13888 5050 50  0001 C CNN
F 3 "" H 13850 5200 50  0000 C CNN
	1    13850 5200
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C111
U 1 1 5A545DAB
P 14700 5200
F 0 "C111" H 14725 5300 50  0000 L CNN
F 1 "330n" H 14725 5100 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 14738 5050 50  0001 C CNN
F 3 "" H 14700 5200 50  0000 C CNN
	1    14700 5200
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R143
U 1 1 5A545DA5
P 14800 6650
F 0 "R143" V 14880 6650 50  0000 C CNN
F 1 "10k" V 14800 6650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 14730 6650 50  0001 C CNN
F 3 "" H 14800 6650 50  0000 C CNN
	1    14800 6650
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R133
U 1 1 5A545D9F
P 14050 5900
F 0 "R133" V 14130 5900 50  0000 C CNN
F 1 "22k" V 14050 5900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 13980 5900 50  0001 C CNN
F 3 "" H 14050 5900 50  0000 C CNN
	1    14050 5900
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R123
U 1 1 5A545D99
P 15000 4950
F 0 "R123" H 15080 4950 50  0000 C CNN
F 1 "1k5" V 15000 4950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 14930 4950 50  0001 C CNN
F 3 "" H 15000 4950 50  0000 C CNN
	1    15000 4950
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R113
U 1 1 5A545D93
P 14700 4700
F 0 "R113" V 14780 4700 50  0000 C CNN
F 1 "11k" V 14700 4700 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 14630 4700 50  0001 C CNN
F 3 "" H 14700 4700 50  0000 C CNN
	1    14700 4700
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U103
U 1 1 5A545D8D
P 14800 5750
F 0 "U103" H 14750 5950 50  0000 L CNN
F 1 "TL072" H 14750 5500 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 14800 5750 50  0001 C CNN
F 3 "" H 14800 5750 50  0000 C CNN
	1    14800 5750
	0    1    1    0   
$EndComp
Text GLabel 13050 4600 1    60   Input ~ 0
SIG
Text GLabel 13450 5700 2    60   Input ~ 0
v+
Text GLabel 12850 5700 0    60   Input ~ 0
v-
Wire Wire Line
	12850 7000 12850 7250
Wire Wire Line
	12900 7000 12850 7000
Wire Wire Line
	13450 7000 13400 7000
Wire Wire Line
	13450 7250 13450 7000
Text GLabel 12850 7250 3    60   Input ~ 0
CUT
Text GLabel 13450 7250 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV104
U 1 1 5A545728
P 13150 7000
F 0 "RV104" H 13150 6900 50  0000 C CNN
F 1 "100k" H 13150 7000 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 13150 7000 50  0001 C CNN
F 3 "" H 13150 7000 50  0000 C CNN
	1    13150 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	13150 6550 13150 6500
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C133
U 1 1 5A545720
P 13150 6350
F 0 "C133" H 13175 6450 50  0000 L CNN
F 1 "10u" H 13175 6250 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 13188 6200 50  0001 C CNN
F 3 "" H 13150 6350 50  0000 C CNN
	1    13150 6350
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR014
U 1 1 5A54571A
P 13550 4750
F 0 "#PWR014" H 13550 4500 50  0001 C CNN
F 1 "GND" H 13550 4600 50  0000 C CNN
F 2 "" H 13550 4750 50  0000 C CNN
F 3 "" H 13550 4750 50  0000 C CNN
	1    13550 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	13500 5000 13600 5000
Wire Wire Line
	13600 4750 13550 4750
Connection ~ 13600 5000
Wire Wire Line
	13600 5200 13600 5000
Wire Wire Line
	13250 5200 13600 5200
Wire Wire Line
	13250 5500 13250 5200
Wire Wire Line
	12400 5450 12400 5800
Wire Wire Line
	13050 5450 12400 5450
Connection ~ 13050 5450
Wire Wire Line
	13050 5400 13050 5450
Wire Wire Line
	12400 6100 12400 6150
Connection ~ 13150 6150
Wire Wire Line
	13150 6100 13150 6150
Connection ~ 12400 6150
Wire Wire Line
	12200 6150 12400 6150
Wire Wire Line
	12200 6150 12200 5400
Wire Wire Line
	12200 5000 12200 5100
Connection ~ 13050 5000
Wire Wire Line
	12200 5000 13050 5000
Wire Wire Line
	13050 4900 13050 5000
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C116
U 1 1 5A545701
P 12200 5250
F 0 "C116" H 12225 5350 50  0000 L CNN
F 1 "330n" H 12225 5150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 12238 5100 50  0001 C CNN
F 3 "" H 12200 5250 50  0000 C CNN
	1    12200 5250
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C117
U 1 1 5A5456FB
P 13050 5250
F 0 "C117" H 13075 5350 50  0000 L CNN
F 1 "330n" H 13075 5150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 13088 5100 50  0001 C CNN
F 3 "" H 13050 5250 50  0000 C CNN
	1    13050 5250
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R146
U 1 1 5A5456F5
P 13150 6700
F 0 "R146" V 13230 6700 50  0000 C CNN
F 1 "10k" V 13150 6700 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 13080 6700 50  0001 C CNN
F 3 "" H 13150 6700 50  0000 C CNN
	1    13150 6700
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R136
U 1 1 5A5456EF
P 12400 5950
F 0 "R136" V 12480 5950 50  0000 C CNN
F 1 "15k" V 12400 5950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 12330 5950 50  0001 C CNN
F 3 "" H 12400 5950 50  0000 C CNN
	1    12400 5950
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R126
U 1 1 5A5456E9
P 13350 5000
F 0 "R126" H 13430 5000 50  0000 C CNN
F 1 "1k" V 13350 5000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 13280 5000 50  0001 C CNN
F 3 "" H 13350 5000 50  0000 C CNN
	1    13350 5000
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R116
U 1 1 5A5456E3
P 13050 4750
F 0 "R116" V 13130 4750 50  0000 C CNN
F 1 "7k5" V 13050 4750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 12980 4750 50  0001 C CNN
F 3 "" H 13050 4750 50  0000 C CNN
	1    13050 4750
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U104
U 2 1 5A5456DD
P 13150 5800
F 0 "U104" H 13100 6000 50  0000 L CNN
F 1 "TL072" H 13100 5550 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 13150 5800 50  0001 C CNN
F 3 "" H 13150 5800 50  0000 C CNN
	2    13150 5800
	0    1    1    0   
$EndComp
Text GLabel 11450 4600 1    60   Input ~ 0
SIG
Text GLabel 11850 5700 2    60   Input ~ 0
v+
Text GLabel 11250 5700 0    60   Input ~ 0
v-
Wire Wire Line
	11250 7000 11250 7250
Wire Wire Line
	11300 7000 11250 7000
Wire Wire Line
	11850 7000 11800 7000
Wire Wire Line
	11850 7250 11850 7000
Text GLabel 11250 7250 3    60   Input ~ 0
CUT
Text GLabel 11850 7250 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV103
U 1 1 5A5456CE
P 11550 7000
F 0 "RV103" H 11550 6900 50  0000 C CNN
F 1 "100k" H 11550 7000 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 11550 7000 50  0001 C CNN
F 3 "" H 11550 7000 50  0000 C CNN
	1    11550 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	11550 6550 11550 6500
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C132
U 1 1 5A5456C6
P 11550 6350
F 0 "C132" H 11575 6450 50  0000 L CNN
F 1 "10u" H 11575 6250 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 11588 6200 50  0001 C CNN
F 3 "" H 11550 6350 50  0000 C CNN
	1    11550 6350
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR013
U 1 1 5A5456C0
P 11950 4750
F 0 "#PWR013" H 11950 4500 50  0001 C CNN
F 1 "GND" H 11950 4600 50  0000 C CNN
F 2 "" H 11950 4750 50  0000 C CNN
F 3 "" H 11950 4750 50  0000 C CNN
	1    11950 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	11900 5000 12000 5000
Wire Wire Line
	12000 4750 11950 4750
Connection ~ 12000 5000
Wire Wire Line
	12000 5200 12000 5000
Wire Wire Line
	11650 5200 12000 5200
Wire Wire Line
	11650 5500 11650 5200
Wire Wire Line
	10800 5450 10800 5800
Wire Wire Line
	11450 5450 10800 5450
Connection ~ 11450 5450
Wire Wire Line
	11450 5400 11450 5450
Wire Wire Line
	10800 6100 10800 6150
Connection ~ 11550 6150
Wire Wire Line
	11550 6100 11550 6150
Connection ~ 10800 6150
Wire Wire Line
	10600 6150 10800 6150
Wire Wire Line
	10600 6150 10600 5400
Wire Wire Line
	10600 5000 10600 5100
Connection ~ 11450 5000
Wire Wire Line
	10600 5000 11450 5000
Wire Wire Line
	11450 4900 11450 5000
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C114
U 1 1 5A5456A7
P 10600 5250
F 0 "C114" H 10625 5350 50  0000 L CNN
F 1 "33n" H 10625 5150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 10638 5100 50  0001 C CNN
F 3 "" H 10600 5250 50  0000 C CNN
	1    10600 5250
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C115
U 1 1 5A5456A1
P 11450 5250
F 0 "C115" H 11475 5350 50  0000 L CNN
F 1 "33n" H 11475 5150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 11488 5100 50  0001 C CNN
F 3 "" H 11450 5250 50  0000 C CNN
	1    11450 5250
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R145
U 1 1 5A54569B
P 11550 6700
F 0 "R145" V 11630 6700 50  0000 C CNN
F 1 "10k" V 11550 6700 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 11480 6700 50  0001 C CNN
F 3 "" H 11550 6700 50  0000 C CNN
	1    11550 6700
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R135
U 1 1 5A545695
P 10800 5950
F 0 "R135" V 10880 5950 50  0000 C CNN
F 1 "75k" V 10800 5950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 10730 5950 50  0001 C CNN
F 3 "" H 10800 5950 50  0000 C CNN
	1    10800 5950
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R125
U 1 1 5A54568F
P 11750 5000
F 0 "R125" H 11830 5000 50  0000 C CNN
F 1 "5k1" V 11750 5000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 11680 5000 50  0001 C CNN
F 3 "" H 11750 5000 50  0000 C CNN
	1    11750 5000
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R115
U 1 1 5A545689
P 11450 4750
F 0 "R115" V 11530 4750 50  0000 C CNN
F 1 "39k" V 11450 4750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 11380 4750 50  0001 C CNN
F 3 "" H 11450 4750 50  0000 C CNN
	1    11450 4750
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U104
U 1 1 5A545683
P 11550 5800
F 0 "U104" H 11500 6000 50  0000 L CNN
F 1 "TL072" H 11500 5550 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 11550 5800 50  0001 C CNN
F 3 "" H 11550 5800 50  0000 C CNN
	1    11550 5800
	0    1    1    0   
$EndComp
Text GLabel 9800 4650 1    60   Input ~ 0
SIG
Text GLabel 10200 5750 2    60   Input ~ 0
v+
Text GLabel 9600 5750 0    60   Input ~ 0
v-
Wire Wire Line
	9600 7050 9600 7300
Wire Wire Line
	9650 7050 9600 7050
Wire Wire Line
	10200 7050 10150 7050
Wire Wire Line
	10200 7300 10200 7050
Text GLabel 9600 7300 3    60   Input ~ 0
CUT
Text GLabel 10200 7300 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV106
U 1 1 5A545674
P 9900 7050
F 0 "RV106" H 9900 6950 50  0000 C CNN
F 1 "100k" H 9900 7050 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 9900 7050 50  0001 C CNN
F 3 "" H 9900 7050 50  0000 C CNN
	1    9900 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 6600 9900 6550
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C135
U 1 1 5A54566C
P 9900 6400
F 0 "C135" H 9925 6500 50  0000 L CNN
F 1 "10u" H 9925 6300 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 9938 6250 50  0001 C CNN
F 3 "" H 9900 6400 50  0000 C CNN
	1    9900 6400
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR012
U 1 1 5A545666
P 10300 4800
F 0 "#PWR012" H 10300 4550 50  0001 C CNN
F 1 "GND" H 10300 4650 50  0000 C CNN
F 2 "" H 10300 4800 50  0000 C CNN
F 3 "" H 10300 4800 50  0000 C CNN
	1    10300 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	10250 5050 10350 5050
Wire Wire Line
	10350 4800 10300 4800
Connection ~ 10350 5050
Wire Wire Line
	10350 5250 10350 5050
Wire Wire Line
	10000 5250 10350 5250
Wire Wire Line
	10000 5550 10000 5250
Wire Wire Line
	9150 5500 9150 5850
Wire Wire Line
	9800 5500 9150 5500
Connection ~ 9800 5500
Wire Wire Line
	9800 5450 9800 5500
Wire Wire Line
	9150 6150 9150 6200
Connection ~ 9900 6200
Wire Wire Line
	9900 6150 9900 6200
Connection ~ 9150 6200
Wire Wire Line
	8950 6200 9150 6200
Wire Wire Line
	8950 6200 8950 5450
Wire Wire Line
	8950 5050 8950 5150
Connection ~ 9800 5050
Wire Wire Line
	8950 5050 9800 5050
Wire Wire Line
	9800 4950 9800 5050
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C120
U 1 1 5A54564D
P 8950 5300
F 0 "C120" H 8975 5400 50  0000 L CNN
F 1 "22n" H 8975 5200 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 8988 5150 50  0001 C CNN
F 3 "" H 8950 5300 50  0000 C CNN
	1    8950 5300
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C121
U 1 1 5A545647
P 9800 5300
F 0 "C121" H 9825 5400 50  0000 L CNN
F 1 "22n" H 9825 5200 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 9838 5150 50  0001 C CNN
F 3 "" H 9800 5300 50  0000 C CNN
	1    9800 5300
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R148
U 1 1 5A545641
P 9900 6750
F 0 "R148" V 9980 6750 50  0000 C CNN
F 1 "10k" V 9900 6750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9830 6750 50  0001 C CNN
F 3 "" H 9900 6750 50  0000 C CNN
	1    9900 6750
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R138
U 1 1 5A54563B
P 9150 6000
F 0 "R138" V 9230 6000 50  0000 C CNN
F 1 "47k" V 9150 6000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9080 6000 50  0001 C CNN
F 3 "" H 9150 6000 50  0000 C CNN
	1    9150 6000
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R128
U 1 1 5A545635
P 10100 5050
F 0 "R128" H 10180 5050 50  0000 C CNN
F 1 "33k" V 10100 5050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 10030 5050 50  0001 C CNN
F 3 "" H 10100 5050 50  0000 C CNN
	1    10100 5050
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R118
U 1 1 5A54562F
P 9800 4800
F 0 "R118" V 9880 4800 50  0000 C CNN
F 1 "22K" V 9800 4800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9730 4800 50  0001 C CNN
F 3 "" H 9800 4800 50  0000 C CNN
	1    9800 4800
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U105
U 2 1 5A545629
P 9900 5850
F 0 "U105" H 9850 6050 50  0000 L CNN
F 1 "TL072" H 9850 5600 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 9900 5850 50  0001 C CNN
F 3 "" H 9900 5850 50  0000 C CNN
	2    9900 5850
	0    1    1    0   
$EndComp
Text GLabel 8200 4650 1    60   Input ~ 0
SIG
Text GLabel 8600 5750 2    60   Input ~ 0
v+
Text GLabel 8000 5750 0    60   Input ~ 0
v-
Wire Wire Line
	8000 7050 8000 7300
Wire Wire Line
	8050 7050 8000 7050
Wire Wire Line
	8600 7050 8550 7050
Wire Wire Line
	8600 7300 8600 7050
Text GLabel 8000 7300 3    60   Input ~ 0
CUT
Text GLabel 8600 7300 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV105
U 1 1 5A54561A
P 8300 7050
F 0 "RV105" H 8300 6950 50  0000 C CNN
F 1 "100k" H 8300 7050 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 8300 7050 50  0001 C CNN
F 3 "" H 8300 7050 50  0000 C CNN
	1    8300 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 6600 8300 6550
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C134
U 1 1 5A545612
P 8300 6400
F 0 "C134" H 8325 6500 50  0000 L CNN
F 1 "10u" H 8325 6300 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 8338 6250 50  0001 C CNN
F 3 "" H 8300 6400 50  0000 C CNN
	1    8300 6400
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR011
U 1 1 5A54560C
P 8700 4800
F 0 "#PWR011" H 8700 4550 50  0001 C CNN
F 1 "GND" H 8700 4650 50  0000 C CNN
F 2 "" H 8700 4800 50  0000 C CNN
F 3 "" H 8700 4800 50  0000 C CNN
	1    8700 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	8650 5050 8750 5050
Wire Wire Line
	8750 4800 8700 4800
Connection ~ 8750 5050
Wire Wire Line
	8750 5250 8750 5050
Wire Wire Line
	8400 5250 8750 5250
Wire Wire Line
	8400 5550 8400 5250
Wire Wire Line
	7550 5500 7550 5850
Wire Wire Line
	8200 5500 7550 5500
Connection ~ 8200 5500
Wire Wire Line
	8200 5450 8200 5500
Wire Wire Line
	7550 6150 7550 6200
Connection ~ 8300 6200
Wire Wire Line
	8300 6150 8300 6200
Connection ~ 7550 6200
Wire Wire Line
	7350 6200 7550 6200
Wire Wire Line
	7350 6200 7350 5450
Wire Wire Line
	7350 5050 7350 5150
Connection ~ 8200 5050
Wire Wire Line
	7350 5050 8200 5050
Wire Wire Line
	8200 4950 8200 5050
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C118
U 1 1 5A5455F3
P 7350 5300
F 0 "C118" H 7375 5400 50  0000 L CNN
F 1 "30n" H 7375 5200 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 7388 5150 50  0001 C CNN
F 3 "" H 7350 5300 50  0000 C CNN
	1    7350 5300
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C119
U 1 1 5A5455ED
P 8200 5300
F 0 "C119" H 8225 5400 50  0000 L CNN
F 1 "30n" H 8225 5200 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 8238 5150 50  0001 C CNN
F 3 "" H 8200 5300 50  0000 C CNN
	1    8200 5300
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R147
U 1 1 5A5455E7
P 8300 6750
F 0 "R147" V 8380 6750 50  0000 C CNN
F 1 "10k" V 8300 6750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 8230 6750 50  0001 C CNN
F 3 "" H 8300 6750 50  0000 C CNN
	1    8300 6750
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R137
U 1 1 5A5455E1
P 7550 6000
F 0 "R137" V 7630 6000 50  0000 C CNN
F 1 "22k" V 7550 6000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7480 6000 50  0001 C CNN
F 3 "" H 7550 6000 50  0000 C CNN
	1    7550 6000
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R127
U 1 1 5A5455DB
P 8500 5050
F 0 "R127" H 8580 5050 50  0000 C CNN
F 1 "1k5" V 8500 5050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 8430 5050 50  0001 C CNN
F 3 "" H 8500 5050 50  0000 C CNN
	1    8500 5050
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R117
U 1 1 5A5455D5
P 8200 4800
F 0 "R117" V 8280 4800 50  0000 C CNN
F 1 "10k" V 8200 4800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 8130 4800 50  0001 C CNN
F 3 "" H 8200 4800 50  0000 C CNN
	1    8200 4800
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U105
U 1 1 5A5455CF
P 8300 5850
F 0 "U105" H 8250 6050 50  0000 L CNN
F 1 "TL072" H 8250 5600 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 8300 5850 50  0001 C CNN
F 3 "" H 8300 5850 50  0000 C CNN
	1    8300 5850
	0    1    1    0   
$EndComp
Text GLabel 6550 4700 1    60   Input ~ 0
SIG
Text GLabel 6950 5800 2    60   Input ~ 0
v+
Text GLabel 6350 5800 0    60   Input ~ 0
v-
Wire Wire Line
	6350 7100 6350 7350
Wire Wire Line
	6400 7100 6350 7100
Wire Wire Line
	6950 7100 6900 7100
Wire Wire Line
	6950 7350 6950 7100
Text GLabel 6350 7350 3    60   Input ~ 0
CUT
Text GLabel 6950 7350 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV108
U 1 1 5A542908
P 6650 7100
F 0 "RV108" H 6650 7000 50  0000 C CNN
F 1 "100k" H 6650 7100 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 6650 7100 50  0001 C CNN
F 3 "" H 6650 7100 50  0000 C CNN
	1    6650 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 6650 6650 6600
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C137
U 1 1 5A542900
P 6650 6450
F 0 "C137" H 6675 6550 50  0000 L CNN
F 1 "10u" H 6675 6350 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 6688 6300 50  0001 C CNN
F 3 "" H 6650 6450 50  0000 C CNN
	1    6650 6450
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR010
U 1 1 5A5428FA
P 7050 4850
F 0 "#PWR010" H 7050 4600 50  0001 C CNN
F 1 "GND" H 7050 4700 50  0000 C CNN
F 2 "" H 7050 4850 50  0000 C CNN
F 3 "" H 7050 4850 50  0000 C CNN
	1    7050 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 5100 7100 5100
Wire Wire Line
	7100 4850 7050 4850
Connection ~ 7100 5100
Wire Wire Line
	7100 5300 7100 5100
Wire Wire Line
	6750 5300 7100 5300
Wire Wire Line
	6750 5600 6750 5300
Wire Wire Line
	5900 5550 5900 5900
Wire Wire Line
	6550 5550 5900 5550
Connection ~ 6550 5550
Wire Wire Line
	6550 5500 6550 5550
Wire Wire Line
	5900 6200 5900 6250
Connection ~ 6650 6250
Wire Wire Line
	6650 6200 6650 6250
Connection ~ 5900 6250
Wire Wire Line
	5700 6250 5900 6250
Wire Wire Line
	5700 6250 5700 5500
Wire Wire Line
	5700 5100 5700 5200
Connection ~ 6550 5100
Wire Wire Line
	5700 5100 6550 5100
Wire Wire Line
	6550 5000 6550 5100
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C124
U 1 1 5A5428E1
P 5700 5350
F 0 "C124" H 5725 5450 50  0000 L CNN
F 1 "3n3" H 5725 5250 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 5738 5200 50  0001 C CNN
F 3 "" H 5700 5350 50  0000 C CNN
	1    5700 5350
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C125
U 1 1 5A5428DB
P 6550 5350
F 0 "C125" H 6575 5450 50  0000 L CNN
F 1 "3n3" H 6575 5250 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6588 5200 50  0001 C CNN
F 3 "" H 6550 5350 50  0000 C CNN
	1    6550 5350
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R150
U 1 1 5A5428D5
P 6650 6800
F 0 "R150" V 6730 6800 50  0000 C CNN
F 1 "10k" V 6650 6800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6580 6800 50  0001 C CNN
F 3 "" H 6650 6800 50  0000 C CNN
	1    6650 6800
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R140
U 1 1 5A5428CF
P 5900 6050
F 0 "R140" V 5980 6050 50  0000 C CNN
F 1 "82k" V 5900 6050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5830 6050 50  0001 C CNN
F 3 "" H 5900 6050 50  0000 C CNN
	1    5900 6050
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R130
U 1 1 5A5428C9
P 6850 5100
F 0 "R130" H 6930 5100 50  0000 C CNN
F 1 "5k6" V 6850 5100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6780 5100 50  0001 C CNN
F 3 "" H 6850 5100 50  0000 C CNN
	1    6850 5100
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R120
U 1 1 5A5428C3
P 6550 4850
F 0 "R120" V 6630 4850 50  0000 C CNN
F 1 "39k" V 6550 4850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6480 4850 50  0001 C CNN
F 3 "" H 6550 4850 50  0000 C CNN
	1    6550 4850
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U106
U 2 1 5A5428BD
P 6650 5900
F 0 "U106" H 6600 6100 50  0000 L CNN
F 1 "TL072" H 6600 5650 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6650 5900 50  0001 C CNN
F 3 "" H 6650 5900 50  0000 C CNN
	2    6650 5900
	0    1    1    0   
$EndComp
Text GLabel 4950 4700 1    60   Input ~ 0
SIG
Text GLabel 5350 5800 2    60   Input ~ 0
v+
Text GLabel 4750 5800 0    60   Input ~ 0
v-
Wire Wire Line
	4750 7100 4750 7350
Wire Wire Line
	4800 7100 4750 7100
Wire Wire Line
	5350 7100 5300 7100
Wire Wire Line
	5350 7350 5350 7100
Text GLabel 4750 7350 3    60   Input ~ 0
CUT
Text GLabel 5350 7350 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV107
U 1 1 5A5428AE
P 5050 7100
F 0 "RV107" H 5050 7000 50  0000 C CNN
F 1 "100k" H 5050 7100 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 5050 7100 50  0001 C CNN
F 3 "" H 5050 7100 50  0000 C CNN
	1    5050 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 6650 5050 6600
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C136
U 1 1 5A5428A6
P 5050 6450
F 0 "C136" H 5075 6550 50  0000 L CNN
F 1 "10u" H 5075 6350 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 5088 6300 50  0001 C CNN
F 3 "" H 5050 6450 50  0000 C CNN
	1    5050 6450
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR09
U 1 1 5A5428A0
P 5450 4850
F 0 "#PWR09" H 5450 4600 50  0001 C CNN
F 1 "GND" H 5450 4700 50  0000 C CNN
F 2 "" H 5450 4850 50  0000 C CNN
F 3 "" H 5450 4850 50  0000 C CNN
	1    5450 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 5100 5500 5100
Wire Wire Line
	5500 4850 5450 4850
Connection ~ 5500 5100
Wire Wire Line
	5500 5300 5500 5100
Wire Wire Line
	5150 5300 5500 5300
Wire Wire Line
	5150 5600 5150 5300
Wire Wire Line
	4300 5550 4300 5900
Wire Wire Line
	4950 5550 4300 5550
Connection ~ 4950 5550
Wire Wire Line
	4950 5500 4950 5550
Wire Wire Line
	4300 6200 4300 6250
Connection ~ 5050 6250
Wire Wire Line
	5050 6200 5050 6250
Connection ~ 4300 6250
Wire Wire Line
	4100 6250 4300 6250
Wire Wire Line
	4100 6250 4100 5500
Wire Wire Line
	4100 5100 4100 5200
Connection ~ 4950 5100
Wire Wire Line
	4100 5100 4950 5100
Wire Wire Line
	4950 5000 4950 5100
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C122
U 1 1 5A542887
P 4100 5350
F 0 "C122" H 4125 5450 50  0000 L CNN
F 1 "3n3" H 4125 5250 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 4138 5200 50  0001 C CNN
F 3 "" H 4100 5350 50  0000 C CNN
	1    4100 5350
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C123
U 1 1 5A542881
P 4950 5350
F 0 "C123" H 4975 5450 50  0000 L CNN
F 1 "3n3" H 4975 5250 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 4988 5200 50  0001 C CNN
F 3 "" H 4950 5350 50  0000 C CNN
	1    4950 5350
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R149
U 1 1 5A54287B
P 5050 6800
F 0 "R149" V 5130 6800 50  0000 C CNN
F 1 "10k" V 5050 6800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4980 6800 50  0001 C CNN
F 3 "" H 5050 6800 50  0000 C CNN
	1    5050 6800
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R139
U 1 1 5A542875
P 4300 6050
F 0 "R139" V 4380 6050 50  0000 C CNN
F 1 "33k" V 4300 6050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4230 6050 50  0001 C CNN
F 3 "" H 4300 6050 50  0000 C CNN
	1    4300 6050
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R129
U 1 1 5A54286F
P 5250 5100
F 0 "R129" H 5330 5100 50  0000 C CNN
F 1 "2k2" V 5250 5100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5180 5100 50  0001 C CNN
F 3 "" H 5250 5100 50  0000 C CNN
	1    5250 5100
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R119
U 1 1 5A542869
P 4950 4850
F 0 "R119" V 5030 4850 50  0000 C CNN
F 1 "15k" V 4950 4850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4880 4850 50  0001 C CNN
F 3 "" H 4950 4850 50  0000 C CNN
	1    4950 4850
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U106
U 1 1 5A542863
P 5050 5900
F 0 "U106" H 5000 6100 50  0000 L CNN
F 1 "TL072" H 5000 5650 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 5050 5900 50  0001 C CNN
F 3 "" H 5050 5900 50  0000 C CNN
	1    5050 5900
	0    1    1    0   
$EndComp
Text GLabel 3300 4750 1    60   Input ~ 0
SIG
Text GLabel 3700 5850 2    60   Input ~ 0
v+
Text GLabel 3100 5850 0    60   Input ~ 0
v-
Wire Wire Line
	3100 7150 3100 7400
Wire Wire Line
	3150 7150 3100 7150
Wire Wire Line
	3700 7150 3650 7150
Wire Wire Line
	3700 7400 3700 7150
Text GLabel 3100 7400 3    60   Input ~ 0
CUT
Text GLabel 3700 7400 3    60   Input ~ 0
BST
$Comp
L 10band-constantQ-rescue:POT-RESCUE-10band-constantQ-10band-constantQ-rescue RV110
U 1 1 5A542516
P 3400 7150
F 0 "RV110" H 3400 7050 50  0000 C CNN
F 1 "100k" H 3400 7150 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 3400 7150 50  0001 C CNN
F 3 "" H 3400 7150 50  0000 C CNN
	1    3400 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6700 3400 6650
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C139
U 1 1 5A54250E
P 3400 6500
F 0 "C139" H 3425 6600 50  0000 L CNN
F 1 "10u" H 3425 6400 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte+" H 3438 6350 50  0001 C CNN
F 3 "" H 3400 6500 50  0000 C CNN
	1    3400 6500
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:GND-power-10band-constantQ-rescue #PWR08
U 1 1 5A542508
P 3800 4900
F 0 "#PWR08" H 3800 4650 50  0001 C CNN
F 1 "GND" H 3800 4750 50  0000 C CNN
F 2 "" H 3800 4900 50  0000 C CNN
F 3 "" H 3800 4900 50  0000 C CNN
	1    3800 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	3750 5150 3850 5150
Wire Wire Line
	3850 4900 3800 4900
Connection ~ 3850 5150
Wire Wire Line
	3850 5350 3850 5150
Wire Wire Line
	3500 5350 3850 5350
Wire Wire Line
	3500 5650 3500 5350
Wire Wire Line
	2650 5600 2650 5950
Wire Wire Line
	3300 5600 2650 5600
Connection ~ 3300 5600
Wire Wire Line
	3300 5550 3300 5600
Wire Wire Line
	2650 6250 2650 6300
Connection ~ 3400 6300
Wire Wire Line
	3400 6250 3400 6300
Connection ~ 2650 6300
Wire Wire Line
	2450 6300 2650 6300
Wire Wire Line
	2450 6300 2450 5550
Wire Wire Line
	2450 5150 2450 5250
Connection ~ 3300 5150
Wire Wire Line
	2450 5150 3300 5150
Wire Wire Line
	3300 5050 3300 5150
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C128
U 1 1 5A5424EF
P 2450 5400
F 0 "C128" H 2475 5500 50  0000 L CNN
F 1 "2n2" H 2475 5300 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 2488 5250 50  0001 C CNN
F 3 "" H 2450 5400 50  0000 C CNN
	1    2450 5400
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:C-Device-10band-constantQ-rescue C129
U 1 1 5A5424E9
P 3300 5400
F 0 "C129" H 3325 5500 50  0000 L CNN
F 1 "2n2" H 3325 5300 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3338 5250 50  0001 C CNN
F 3 "" H 3300 5400 50  0000 C CNN
	1    3300 5400
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R152
U 1 1 5A5424E3
P 3400 6850
F 0 "R152" V 3480 6850 50  0000 C CNN
F 1 "10k" V 3400 6850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3330 6850 50  0001 C CNN
F 3 "" H 3400 6850 50  0000 C CNN
	1    3400 6850
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R142
U 1 1 5A5424DD
P 2650 6100
F 0 "R142" V 2730 6100 50  0000 C CNN
F 1 "33k" V 2650 6100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 2580 6100 50  0001 C CNN
F 3 "" H 2650 6100 50  0000 C CNN
	1    2650 6100
	1    0    0    -1  
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R132
U 1 1 5A5424D7
P 3600 5150
F 0 "R132" H 3680 5150 50  0000 C CNN
F 1 "2k2" V 3600 5150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3530 5150 50  0001 C CNN
F 3 "" H 3600 5150 50  0000 C CNN
	1    3600 5150
	0    -1   -1   0   
$EndComp
$Comp
L 10band-constantQ-rescue:R-Device-10band-constantQ-rescue R122
U 1 1 5A5424D1
P 3300 4900
F 0 "R122" V 3380 4900 50  0000 C CNN
F 1 "15k" V 3300 4900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3230 4900 50  0001 C CNN
F 3 "" H 3300 4900 50  0000 C CNN
	1    3300 4900
	-1   0    0    1   
$EndComp
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U107
U 2 1 5A5424CB
P 3400 5950
F 0 "U107" H 3350 6150 50  0000 L CNN
F 1 "TL072" H 3350 5700 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3400 5950 50  0001 C CNN
F 3 "" H 3400 5950 50  0000 C CNN
	2    3400 5950
	0    1    1    0   
$EndComp
Entry Bus Bus
	2650 6100 2750 6200
Entry Bus Bus
	3350 4850 3450 4950
$Comp
L 10band-constantQ-rescue:TL072-10band-constantQ-rescue U1
U 2 1 5CD90CF4
P 5550 3400
F 0 "U1" H 5891 3446 50  0000 L CNN
F 1 "TL072-10band-constantQ-rescue" H 5891 3355 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 5550 3400 50  0001 C CNN
F 3 "" H 5550 3400 50  0001 C CNN
	2    5550 3400
	1    0    0    1   
$EndComp
$EndSCHEMATC
