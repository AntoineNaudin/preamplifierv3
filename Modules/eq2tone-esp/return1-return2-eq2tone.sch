EESchema Schematic File Version 4
LIBS:return1-return2-eq2tone-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R116
U 1 1 5968A4C6
P 10450 3550
F 0 "R116" V 10530 3550 50  0000 C CNN
F 1 "10k" V 10450 3550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10380 3550 50  0001 C CNN
F 3 "" H 10450 3550 50  0000 C CNN
	1    10450 3550
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R118
U 1 1 5D2FDFFC
P 11050 3650
F 0 "R118" V 11130 3650 50  0000 C CNN
F 1 "75" V 11050 3650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10980 3650 50  0001 C CNN
F 3 "" H 11050 3650 50  0000 C CNN
	1    11050 3650
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D2FDFFD
P 10900 3650
F 0 "#PWR?" H 10900 3400 50  0001 C CNN
F 1 "GND" H 10900 3500 50  0000 C CNN
F 2 "" H 10900 3650 50  0000 C CNN
F 3 "" H 10900 3650 50  0000 C CNN
	1    10900 3650
	0    1    1    0   
$EndComp
Text Label 11450 3550 0    60   ~ 0
MAS
Text Label 11300 3650 0    60   ~ 0
GND-MAS
Wire Wire Line
	11200 3650 11750 3650
Wire Wire Line
	10600 3550 10650 3550
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J103
U 1 1 5C314EF7
P 11950 3550
F 0 "J103" H 11950 3650 50  0000 C CNN
F 1 "MASTER-BUS" H 12300 3500 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 11950 3550 50  0001 C CNN
F 3 "" H 11950 3550 50  0001 C CNN
	1    11950 3550
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV104
U 1 1 5C30E9E2
P 8600 3950
F 0 "RV104" V 8425 3950 50  0000 C CNN
F 1 "MID 10kB" V 8500 3950 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 8600 3950 50  0001 C CNN
F 3 "" H 8600 3950 50  0001 C CNN
	1    8600 3950
	-1   0    0    1   
$EndComp
NoConn ~ 9050 3300
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C108
U 1 1 5D2FDFFA
P 10000 3550
F 0 "C108" H 10025 3650 50  0000 L CNN
F 1 "10u" H 10025 3450 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 10038 3400 50  0001 C CNN
F 3 "" H 10000 3550 50  0000 C CNN
	1    10000 3550
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV103
U 1 1 5CC62F8A
P 7500 3950
F 0 "RV103" V 7325 3950 50  0000 C CNN
F 1 "BASS10k" V 7400 3950 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 7500 3950 50  0001 C CNN
F 3 "" H 7500 3950 50  0001 C CNN
	1    7500 3950
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue U102
U 1 1 596AAB42
P 9350 3550
F 0 "U102" H 9300 3750 50  0000 L CNN
F 1 "ne5532" H 9300 3300 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 9350 3550 50  0001 C CNN
F 3 "" H 9350 3550 50  0000 C CNN
	1    9350 3550
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R112
U 1 1 5CC7B74D
P 7500 4400
F 0 "R112" V 7550 4400 50  0000 C CNN
F 1 "2k2" V 7500 4400 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7430 4400 50  0001 C CNN
F 3 "" H 7500 4400 50  0000 C CNN
	1    7500 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 3550 9650 3550
Connection ~ 9850 3550
NoConn ~ 1250 -500
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C109
U 1 1 5D2FE004
P 15150 4050
F 0 "C109" H 15175 4150 50  0000 L CNN
F 1 "100n" H 15175 3950 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 15188 3900 50  0001 C CNN
F 3 "" H 15150 4050 50  0000 C CNN
	1    15150 4050
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R119
U 1 1 5D2FE005
P 14650 3850
F 0 "R119" V 14730 3850 50  0000 C CNN
F 1 "2R2" V 14650 3850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 14580 3850 50  0001 C CNN
F 3 "" H 14650 3850 50  0000 C CNN
	1    14650 3850
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R120
U 1 1 5D2FE006
P 14650 4650
F 0 "R120" V 14730 4650 50  0000 C CNN
F 1 "2R2" V 14650 4650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 14580 4650 50  0001 C CNN
F 3 "" H 14650 4650 50  0000 C CNN
	1    14650 4650
	0    1    1    0   
$EndComp
Text GLabel 15950 3850 2    60   Input ~ 0
+v
Text GLabel 15950 4650 2    60   Input ~ 0
-v
Text Label 15300 3850 0    60   ~ 0
+16v
Text Label 15300 4650 0    60   ~ 0
-16v
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG?
U 1 1 5AA41643
P 14500 4250
F 0 "#FLG?" H 14500 4345 50  0001 C CNN
F 1 "PWR_FLAG" H 14500 4430 50  0000 C CNN
F 2 "" H 14500 4250 50  0000 C CNN
F 3 "" H 14500 4250 50  0000 C CNN
	1    14500 4250
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG?
U 1 1 5D2FE000
P 15000 3800
F 0 "#FLG?" H 15000 3895 50  0001 C CNN
F 1 "PWR_FLAG" H 15000 3980 50  0000 C CNN
F 2 "" H 15000 3800 50  0000 C CNN
F 3 "" H 15000 3800 50  0000 C CNN
	1    15000 3800
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG?
U 1 1 5D2FE009
P 14950 4650
F 0 "#FLG?" H 14950 4745 50  0001 C CNN
F 1 "PWR_FLAG" H 14950 4830 50  0000 C CNN
F 2 "" H 14950 4650 50  0000 C CNN
F 3 "" H 14950 4650 50  0000 C CNN
	1    14950 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 4650 14500 4650
Wire Wire Line
	14350 3850 14500 3850
Wire Wire Line
	14800 4650 14950 4650
Connection ~ 15150 3850
Wire Wire Line
	15150 3850 15150 3900
Wire Wire Line
	14800 3850 15000 3850
Wire Wire Line
	14350 4050 14350 4650
Wire Wire Line
	15000 3800 15000 3850
Wire Wire Line
	14950 4650 14950 4750
Connection ~ 14950 4650
Wire Wire Line
	14500 3950 14500 4250
Connection ~ 14500 4250
Wire Wire Line
	14150 4050 14350 4050
Wire Wire Line
	14150 3950 14350 3950
Wire Wire Line
	14350 3950 14350 3850
Wire Wire Line
	14150 3850 14150 3800
Wire Wire Line
	14150 3800 14450 3800
Wire Wire Line
	14450 3800 14450 3950
Wire Wire Line
	14450 3950 14500 3950
$Comp
L eq3tone-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue J104
U 1 1 5CCEB1BC
P 13950 3950
F 0 "J104" H 13950 4150 50  0000 C CNN
F 1 "alim" H 13950 3750 50  0000 C CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 13950 3950 50  0001 C CNN
F 3 "" H 13950 3950 50  0001 C CNN
	1    13950 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	15150 4200 15150 4650
Connection ~ 15150 4650
Wire Wire Line
	14950 4650 15150 4650
Connection ~ 15000 3850
Wire Wire Line
	15000 3850 15150 3850
$Comp
L eq3tone-rescue:GNDD-power-riaav3-rescue #PWR?
U 1 1 5CCFA5FC
P 14500 4250
F 0 "#PWR?" H 14500 4000 50  0001 C CNN
F 1 "GNDD-power" H 14504 4095 50  0000 C CNN
F 2 "" H 14500 4250 50  0001 C CNN
F 3 "" H 14500 4250 50  0001 C CNN
	1    14500 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	15150 4650 15950 4650
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D2CB5BC
P 9050 3450
F 0 "#PWR?" H 9050 3200 50  0001 C CNN
F 1 "GND" H 9050 3300 50  0000 C CNN
F 2 "" H 9050 3450 50  0000 C CNN
F 3 "" H 9050 3450 50  0000 C CNN
	1    9050 3450
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R113
U 1 1 5D2CDAF0
P 7850 3950
F 0 "R113" V 7900 3950 50  0000 C CNN
F 1 "2k2" V 7850 3950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7780 3950 50  0001 C CNN
F 3 "" H 7850 3950 50  0000 C CNN
	1    7850 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	7700 3950 7650 3950
Wire Wire Line
	7500 4100 7500 4150
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue C106
U 1 1 5D2D4653
P 8300 3950
F 0 "C106" H 8325 4050 50  0000 L CNN
F 1 "10n" H 8325 3850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.0mm_W2.5mm_P5.00mm-grospad" H 8338 3800 50  0001 C CNN
F 3 "" H 8300 3950 50  0000 C CNN
	1    8300 3950
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R111
U 1 1 5D2D47E0
P 7500 3450
F 0 "R111" V 7550 3450 50  0000 C CNN
F 1 "2k2" V 7500 3450 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7430 3450 50  0001 C CNN
F 3 "" H 7500 3450 50  0000 C CNN
	1    7500 3450
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue C105
U 1 1 5D2D5AAE
P 7150 3900
F 0 "C105" H 7175 4000 50  0000 L CNN
F 1 "220n" H 7175 3800 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.0mm_W2.5mm_P5.00mm-grospad" H 7188 3750 50  0001 C CNN
F 3 "" H 7150 3900 50  0000 C CNN
	1    7150 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7150 3750 7500 3750
Wire Wire Line
	7500 3600 7500 3750
Connection ~ 7500 3750
Wire Wire Line
	7500 3750 7500 3800
Wire Wire Line
	7150 4050 7150 4150
Wire Wire Line
	7150 4150 7500 4150
Connection ~ 7500 4150
Wire Wire Line
	7500 4150 7500 4250
Wire Wire Line
	8000 3950 8100 3950
Wire Wire Line
	9050 3650 8100 3650
Wire Wire Line
	8100 3650 8100 3950
Connection ~ 8100 3950
Wire Wire Line
	8100 3950 8150 3950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R114
U 1 1 5D2DB41B
P 8600 3450
F 0 "R114" V 8650 3450 50  0000 C CNN
F 1 "1k" V 8600 3450 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 8530 3450 50  0001 C CNN
F 3 "" H 8600 3450 50  0000 C CNN
	1    8600 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3600 8600 3800
Wire Wire Line
	8600 3300 8600 3150
Wire Wire Line
	8600 3150 7500 3150
Wire Wire Line
	7500 3300 7500 3150
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R115
U 1 1 5D2ED3D2
P 8600 4350
F 0 "R115" V 8650 4350 50  0000 C CNN
F 1 "1k" V 8600 4350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 8530 4350 50  0001 C CNN
F 3 "" H 8600 4350 50  0000 C CNN
	1    8600 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 4100 8600 4200
Wire Wire Line
	9850 4600 8600 4600
Wire Wire Line
	7500 4600 7500 4550
Wire Wire Line
	9850 3550 9850 4600
Wire Wire Line
	8600 4500 8600 4600
Connection ~ 8600 4600
Wire Wire Line
	8600 4600 7500 4600
Wire Wire Line
	10150 3550 10300 3550
NoConn ~ 15700 850 
NoConn ~ 15700 1000
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R101
U 1 1 5CCEC1EC
P 4100 3100
F 0 "R101" V 4180 3100 50  0000 C CNN
F 1 "100k" V 4100 3100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 4030 3100 50  0001 C CNN
F 3 "" H 4100 3100 50  0000 C CNN
	1    4100 3100
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5CCEC1F2
P 4100 3250
F 0 "#PWR?" H 4100 3000 50  0001 C CNN
F 1 "GND" H 4100 3100 50  0000 C CNN
F 2 "" H 4100 3250 50  0000 C CNN
F 3 "" H 4100 3250 50  0000 C CNN
	1    4100 3250
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV101
U 1 1 5CD2D3A4
P 4850 3050
F 0 "RV101" V 4675 3050 50  0000 C CNN
F 1 "volume" V 4750 3050 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 4850 3050 50  0001 C CNN
F 3 "" H 4850 3050 50  0001 C CNN
	1    4850 3050
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5CD31BB1
P 4850 3200
F 0 "#PWR?" H 4850 2950 50  0001 C CNN
F 1 "GND" H 4850 3050 50  0000 C CNN
F 2 "" H 4850 3200 50  0000 C CNN
F 3 "" H 4850 3200 50  0000 C CNN
	1    4850 3200
	1    0    0    -1  
$EndComp
NoConn ~ -2750 4600
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR?
U 1 1 5AA3E982
P 3650 3000
F 0 "#PWR?" H 3650 2750 50  0001 C CNN
F 1 "GND" H 3650 2850 50  0000 C CNN
F 2 "" H 3650 3000 50  0000 C CNN
F 3 "" H 3650 3000 50  0000 C CNN
	1    3650 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 3500 6350 3500
Wire Wire Line
	6350 2800 6350 2850
Wire Wire Line
	6300 2800 6350 2800
Wire Wire Line
	6350 3500 6350 3450
Text GLabel 9250 3200 0    60   Input ~ 0
+v
Text GLabel 9250 3900 0    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue U101
U 2 1 5CD660AD
P 6450 3150
F 0 "U101" H 6400 3350 50  0000 L CNN
F 1 "ne5532" H 6400 2900 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6450 3150 50  0001 C CNN
F 3 "" H 6450 3150 50  0000 C CNN
	2    6450 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3650 5950 3950
Wire Wire Line
	5950 3250 6150 3250
Wire Wire Line
	6150 3650 5950 3650
Wire Wire Line
	6800 3650 6800 3950
Wire Wire Line
	6450 3650 6800 3650
Wire Wire Line
	6150 3950 5950 3950
Wire Wire Line
	6800 3950 6450 3950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R109
U 1 1 5CD660BA
P 6300 3650
F 0 "R109" V 6350 3650 50  0000 C CNN
F 1 "2k2" V 6300 3650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 6230 3650 50  0001 C CNN
F 3 "" H 6300 3650 50  0000 C CNN
	1    6300 3650
	0    -1   1    0   
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C103
U 1 1 5CD660B4
P 6300 3950
F 0 "C103" H 6325 4050 50  0000 L CNN
F 1 "22p" H 6325 3850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6338 3800 50  0001 C CNN
F 3 "" H 6300 3950 50  0000 C CNN
	1    6300 3950
	0    1    -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R107
U 1 1 5CD379BE
P 5800 3650
F 0 "R107" V 5850 3650 50  0000 C CNN
F 1 "2k2" V 5800 3650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5730 3650 50  0001 C CNN
F 3 "" H 5800 3650 50  0000 C CNN
	1    5800 3650
	0    -1   1    0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D2E8726
P 5650 3650
F 0 "#PWR?" H 5650 3400 50  0001 C CNN
F 1 "GND" H 5650 3500 50  0000 C CNN
F 2 "" H 5650 3650 50  0000 C CNN
F 3 "" H 5650 3650 50  0000 C CNN
	1    5650 3650
	0    1    -1   0   
$EndComp
Wire Wire Line
	3950 2900 4100 2900
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R103
U 1 1 5CD00D3A
P 4350 2900
F 0 "R103" V 4400 2900 50  0000 C CNN
F 1 "220" V 4350 2900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 4280 2900 50  0001 C CNN
F 3 "" H 4350 2900 50  0000 C CNN
	1    4350 2900
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C101
U 1 1 5D335913
P 3800 2900
F 0 "C101" V 4055 2900 50  0000 C CNN
F 1 "10u" V 3650 3250 50  0000 C CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 3838 2750 50  0001 C CNN
F 3 "" H 3800 2900 50  0001 C CNN
	1    3800 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5000 3050 5150 3050
Wire Wire Line
	4100 2950 4100 2900
Wire Wire Line
	4500 2900 4850 2900
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R105
U 1 1 5D38AE5C
P 5150 3200
F 0 "R105" V 5230 3200 50  0000 C CNN
F 1 "15k" V 5150 3200 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5080 3200 50  0001 C CNN
F 3 "" H 5150 3200 50  0000 C CNN
	1    5150 3200
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D38AE62
P 5150 3350
F 0 "#PWR?" H 5150 3100 50  0001 C CNN
F 1 "GND" H 5150 3200 50  0000 C CNN
F 2 "" H 5150 3350 50  0000 C CNN
F 3 "" H 5150 3350 50  0000 C CNN
	1    5150 3350
	1    0    0    -1  
$EndComp
Connection ~ 5150 3050
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV102
U 1 1 5D38EE00
P 4900 4950
F 0 "RV102" V 4725 4950 50  0000 C CNN
F 1 "volume" V 4800 4950 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 4900 4950 50  0001 C CNN
F 3 "" H 4900 4950 50  0001 C CNN
	1    4900 4950
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D38EE06
P 4900 5100
F 0 "#PWR?" H 4900 4850 50  0001 C CNN
F 1 "GND" H 4900 4950 50  0000 C CNN
F 2 "" H 4900 5100 50  0000 C CNN
F 3 "" H 4900 5100 50  0000 C CNN
	1    4900 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4950 5200 4950
Wire Wire Line
	4550 4800 4900 4800
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R106
U 1 1 5D38EE62
P 5200 5100
F 0 "R106" V 5280 5100 50  0000 C CNN
F 1 "15k" V 5200 5100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5130 5100 50  0001 C CNN
F 3 "" H 5200 5100 50  0000 C CNN
	1    5200 5100
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D38EE68
P 5200 5250
F 0 "#PWR?" H 5200 5000 50  0001 C CNN
F 1 "GND" H 5200 5100 50  0000 C CNN
F 2 "" H 5200 5250 50  0000 C CNN
F 3 "" H 5200 5250 50  0000 C CNN
	1    5200 5250
	1    0    0    -1  
$EndComp
Connection ~ 5200 4950
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J101
U 1 1 5D3F56DE
P 3450 3000
F 0 "J101" H 3450 3100 50  0000 C CNN
F 1 "inL" H 3800 2950 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 3450 3000 50  0001 C CNN
F 3 "" H 3450 3000 50  0001 C CNN
	1    3450 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4200 2900 4100 2900
Connection ~ 4100 2900
Connection ~ 5950 3650
Wire Wire Line
	6750 3150 6800 3150
Wire Wire Line
	5950 3250 5950 3650
Wire Wire Line
	6800 3150 6800 3650
Connection ~ 6800 3650
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R102
U 1 1 5D3B8FAA
P 4150 5000
F 0 "R102" V 4230 5000 50  0000 C CNN
F 1 "100k" V 4150 5000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 4080 5000 50  0001 C CNN
F 3 "" H 4150 5000 50  0000 C CNN
	1    4150 5000
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D3B8FB0
P 4150 5150
F 0 "#PWR?" H 4150 4900 50  0001 C CNN
F 1 "GND" H 4150 5000 50  0000 C CNN
F 2 "" H 4150 5150 50  0000 C CNN
F 3 "" H 4150 5150 50  0000 C CNN
	1    4150 5150
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR?
U 1 1 5D3B8FB6
P 3700 4900
F 0 "#PWR?" H 3700 4650 50  0001 C CNN
F 1 "GND" H 3700 4750 50  0000 C CNN
F 2 "" H 3700 4900 50  0000 C CNN
F 3 "" H 3700 4900 50  0000 C CNN
	1    3700 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 5400 6400 5400
Wire Wire Line
	6400 4700 6400 4750
Wire Wire Line
	6350 4700 6400 4700
Wire Wire Line
	6400 5400 6400 5350
Text GLabel 6350 4700 0    60   Input ~ 0
+v
Text GLabel 6350 5400 0    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue U101
U 1 1 5D3B8FC2
P 6500 5050
F 0 "U101" H 6450 5250 50  0000 L CNN
F 1 "ne5532" H 6450 4800 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6500 5050 50  0001 C CNN
F 3 "" H 6500 5050 50  0000 C CNN
	1    6500 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 5550 6000 5850
Wire Wire Line
	6000 5150 6200 5150
Wire Wire Line
	6200 5550 6000 5550
Wire Wire Line
	6850 5550 6850 5850
Wire Wire Line
	6500 5550 6850 5550
Wire Wire Line
	6200 5850 6000 5850
Wire Wire Line
	6850 5850 6500 5850
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R110
U 1 1 5D3B8FCF
P 6350 5550
F 0 "R110" V 6400 5550 50  0000 C CNN
F 1 "2k2" V 6350 5550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 6280 5550 50  0001 C CNN
F 3 "" H 6350 5550 50  0000 C CNN
	1    6350 5550
	0    -1   1    0   
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C104
U 1 1 5D3B8FD5
P 6350 5850
F 0 "C104" H 6375 5950 50  0000 L CNN
F 1 "22p" H 6375 5750 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6388 5700 50  0001 C CNN
F 3 "" H 6350 5850 50  0000 C CNN
	1    6350 5850
	0    1    -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R108
U 1 1 5D3B8FDB
P 5850 5550
F 0 "R108" V 5900 5550 50  0000 C CNN
F 1 "2k2" V 5850 5550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5780 5550 50  0001 C CNN
F 3 "" H 5850 5550 50  0000 C CNN
	1    5850 5550
	0    -1   1    0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5D3B8FE1
P 5700 5550
F 0 "#PWR?" H 5700 5300 50  0001 C CNN
F 1 "GND" H 5700 5400 50  0000 C CNN
F 2 "" H 5700 5550 50  0000 C CNN
F 3 "" H 5700 5550 50  0000 C CNN
	1    5700 5550
	0    1    -1   0   
$EndComp
Wire Wire Line
	4000 4800 4150 4800
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R104
U 1 1 5D3B8FE8
P 4400 4800
F 0 "R104" V 4450 4800 50  0000 C CNN
F 1 "220" V 4400 4800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 4330 4800 50  0001 C CNN
F 3 "" H 4400 4800 50  0000 C CNN
	1    4400 4800
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C102
U 1 1 5D3B8FEE
P 3850 4800
F 0 "C102" V 4105 4800 50  0000 C CNN
F 1 "10u" V 3700 5150 50  0000 C CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 3888 4650 50  0001 C CNN
F 3 "" H 3850 4800 50  0001 C CNN
	1    3850 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 4850 4150 4800
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J102
U 1 1 5D3B8FF5
P 3500 4900
F 0 "J102" H 3500 5000 50  0000 C CNN
F 1 "inL" H 3850 4850 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 3500 4900 50  0001 C CNN
F 3 "" H 3500 4900 50  0001 C CNN
	1    3500 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4250 4800 4150 4800
Connection ~ 4150 4800
Connection ~ 6000 5550
Wire Wire Line
	6000 5150 6000 5550
Wire Wire Line
	6850 5050 6850 5550
Connection ~ 6850 5550
Wire Wire Line
	6800 3150 7500 3150
Connection ~ 6800 3150
Connection ~ 7500 3150
Wire Wire Line
	5150 3050 6150 3050
Wire Wire Line
	5200 4950 6200 4950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R117
U 1 1 5D3E398E
P 10450 5050
F 0 "R117" V 10530 5050 50  0000 C CNN
F 1 "10k" V 10450 5050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10380 5050 50  0001 C CNN
F 3 "" H 10450 5050 50  0000 C CNN
	1    10450 5050
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C107
U 1 1 5D3E3994
P 9950 5050
F 0 "C107" H 9975 5150 50  0000 L CNN
F 1 "10u" H 9975 4950 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 9988 4900 50  0001 C CNN
F 3 "" H 9950 5050 50  0000 C CNN
	1    9950 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10650 3550 10650 5050
Wire Wire Line
	10650 5050 10600 5050
Connection ~ 10650 3550
Wire Wire Line
	10650 3550 11750 3550
Wire Wire Line
	6800 5050 6850 5050
Wire Wire Line
	10100 5050 10300 5050
Wire Wire Line
	6850 5050 9800 5050
Connection ~ 6850 5050
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue U102
U 2 1 5D2B9819
P 9950 2600
F 0 "U102" H 9900 2800 50  0000 L CNN
F 1 "ne5532" H 9900 2350 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 9950 2600 50  0001 C CNN
F 3 "" H 9950 2600 50  0000 C CNN
	2    9950 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 2600 10250 3200
Wire Wire Line
	10250 3200 9650 3200
Wire Wire Line
	9650 3200 9650 2700
NoConn ~ 9650 2500
NoConn ~ 10000 5100
NoConn ~ 10550 5050
Wire Wire Line
	9250 3200 9250 3250
Wire Wire Line
	9250 3850 9250 3900
Wire Wire Line
	15150 3850 15950 3850
$EndSCHEMATC
