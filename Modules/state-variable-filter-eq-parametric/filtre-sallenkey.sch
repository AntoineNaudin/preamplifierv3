EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R3
U 1 1 5C31AE26
P 7250 9750
F 0 "R3" V 7330 9750 50  0000 C CNN
F 1 "3k3" V 7250 9750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM14mm-padcarre2mm" V 7180 9750 50  0001 C CNN
F 3 "" H 7250 9750 50  0001 C CNN
	1    7250 9750
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R4
U 1 1 5C31AFE2
P 6400 10450
F 0 "R4" V 6480 10450 50  0000 C CNN
F 1 "1k" V 6400 10450 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6330 10450 50  0001 C CNN
F 3 "" H 6400 10450 50  0001 C CNN
	1    6400 10450
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0101
U 1 1 5C31B072
P 6150 10500
F 0 "#PWR0101" H 6150 10250 50  0001 C CNN
F 1 "GND" H 6150 10350 50  0000 C CNN
F 2 "" H 6150 10500 50  0001 C CNN
F 3 "" H 6150 10500 50  0001 C CNN
	1    6150 10500
	1    0    0    -1  
$EndComp
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R14
U 1 1 5C31B331
P 7800 8600
F 0 "R14" V 7880 8600 50  0000 C CNN
F 1 "22k" V 7800 8600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM12mm-PAS2MM" V 7730 8600 50  0001 C CNN
F 3 "" H 7800 8600 50  0001 C CNN
	1    7800 8600
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:C-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue C6
U 1 1 5C31B84E
P 9650 9150
F 0 "C6" H 9675 9250 50  0000 L CNN
F 1 "100n" H 9675 9050 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 9688 9000 50  0001 C CNN
F 3 "" H 9650 9150 50  0001 C CNN
	1    9650 9150
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0102
U 1 1 5C31FA06
P 9350 10250
F 0 "#PWR0102" H 9350 10000 50  0001 C CNN
F 1 "GND" H 9350 10100 50  0000 C CNN
F 2 "" H 9350 10250 50  0001 C CNN
F 3 "" H 9350 10250 50  0001 C CNN
	1    9350 10250
	1    0    0    -1  
$EndComp
$Comp
L filtre-sallenkey-rescue:C-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue C2
U 1 1 5C32067D
P 7950 15200
F 0 "C2" H 7975 15300 50  0000 L CNN
F 1 "100n" H 7975 15100 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 7988 15050 50  0001 C CNN
F 3 "" H 7950 15200 50  0001 C CNN
	1    7950 15200
	-1   0    0    1   
$EndComp
Text Notes 10200 9950 0    118  ~ 0
Channel 1 : 70hHz-1KHz
$Comp
L filtre-sallenkey-rescue:Conn_01x03-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue J3
U 1 1 5C32C739
P 7000 15150
F 0 "J3" H 7000 15350 50  0000 C CNN
F 1 "Conn_01x03" H 7000 14950 50  0000 C CNN
F 2 "resistance-grospad:connection-3-padcarre2mm" H 7000 15150 50  0001 C CNN
F 3 "" H 7000 15150 50  0001 C CNN
	1    7000 15150
	-1   0    0    -1  
$EndComp
Text GLabel 7650 14900 1    39   Input ~ 0
+15
Text GLabel 7750 14900 1    39   Input ~ 0
-15
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R23
U 1 1 5C32CB40
P 7350 15050
F 0 "R23" V 7430 15050 50  0000 C CNN
F 1 "20" V 7350 15050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7280 15050 50  0001 C CNN
F 3 "" H 7350 15050 50  0001 C CNN
	1    7350 15050
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R24
U 1 1 5C32CBFF
P 7350 15150
F 0 "R24" V 7430 15150 50  0000 C CNN
F 1 "20" V 7350 15150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7280 15150 50  0001 C CNN
F 3 "" H 7350 15150 50  0001 C CNN
	1    7350 15150
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0103
U 1 1 5C32D106
P 7200 15250
F 0 "#PWR0103" H 7200 15000 50  0001 C CNN
F 1 "GND" H 7200 15100 50  0000 C CNN
F 2 "" H 7200 15250 50  0001 C CNN
F 3 "" H 7200 15250 50  0001 C CNN
	1    7200 15250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9350 10200 9350 10250
Wire Wire Line
	9400 10200 9350 10200
Connection ~ 10100 10100
Connection ~ 7500 9750
Wire Wire Line
	10000 10100 10100 10100
Wire Wire Line
	9800 9150 10100 9150
Wire Wire Line
	9300 9150 9500 9150
Wire Wire Line
	9300 10000 9300 9150
Wire Wire Line
	7600 9950 7450 9950
Wire Wire Line
	6150 10450 6250 10450
Wire Wire Line
	6150 10500 6150 10450
Wire Wire Line
	7400 9750 7500 9750
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R1
U 1 1 5C34C3EE
P 9000 12750
F 0 "R1" V 9080 12750 50  0000 C CNN
F 1 "22k" V 9000 12750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 8930 12750 50  0001 C CNN
F 3 "" H 9000 12750 50  0001 C CNN
	1    9000 12750
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:Conn_01x02-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue J1
U 1 1 5C34C3F4
P 7750 12850
F 0 "J1" H 7750 12950 50  0000 C CNN
F 1 "X" H 7750 12650 50  0000 C CNN
F 2 "resistance-grospad:PinHeader_1x02_P2.54mm_Vertical" H 7750 12850 50  0001 C CNN
F 3 "" H 7750 12850 50  0001 C CNN
	1    7750 12850
	-1   0    0    1   
$EndComp
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0104
U 1 1 5C34C3FA
P 8050 12900
F 0 "#PWR0104" H 8050 12650 50  0001 C CNN
F 1 "GND" H 8050 12750 50  0000 C CNN
F 2 "" H 8050 12900 50  0001 C CNN
F 3 "" H 8050 12900 50  0001 C CNN
	1    8050 12900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 12750 8700 12750
Wire Wire Line
	7950 12850 8050 12850
Wire Wire Line
	8050 12850 8050 12900
Wire Wire Line
	10100 10100 10250 10100
Wire Wire Line
	10100 9150 10100 10100
Wire Wire Line
	7500 8600 7650 8600
Wire Wire Line
	7500 8600 7500 8950
Wire Wire Line
	7500 9750 7600 9750
Wire Wire Line
	9300 10000 9400 10000
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R106
U 1 1 5D247566
P 9550 13150
F 0 "R106" V 9630 13150 50  0000 C CNN
F 1 "22k" V 9550 13150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 9480 13150 50  0001 C CNN
F 3 "" H 9550 13150 50  0001 C CNN
	1    9550 13150
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 13150 10200 13150
Wire Wire Line
	10200 13150 10200 12650
Wire Wire Line
	10200 12650 9900 12650
Wire Wire Line
	9300 12750 9150 12750
Wire Wire Line
	9150 12750 9150 13150
Wire Wire Line
	9150 13150 9400 13150
Wire Wire Line
	8750 12050 8700 12050
Wire Wire Line
	8700 12050 8700 12750
Wire Wire Line
	8350 12750 7950 12750
Wire Wire Line
	9050 12050 10200 12050
Wire Wire Line
	10200 12050 10200 12650
Connection ~ 10200 12650
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R104
U 1 1 5D264786
P 8600 10350
F 0 "R104" V 8680 10350 50  0000 C CNN
F 1 "2k2" V 8600 10350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 8530 10350 50  0001 C CNN
F 3 "" H 8600 10350 50  0001 C CNN
	1    8600 10350
	-1   0    0    1   
$EndComp
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0105
U 1 1 5D2648C5
P 8600 10800
F 0 "#PWR0105" H 8600 10550 50  0001 C CNN
F 1 "GND" H 8600 10650 50  0000 C CNN
F 2 "" H 8600 10800 50  0001 C CNN
F 3 "" H 8600 10800 50  0001 C CNN
	1    8600 10800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C102
U 1 1 5D264B23
P 8600 10650
F 0 "C102" H 8718 10696 50  0000 L CNN
F 1 "100u" H 8718 10605 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-padcarre2mm" H 8638 10500 50  0001 C CNN
F 3 "~" H 8600 10650 50  0001 C CNN
	1    8600 10650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C101
U 1 1 5D383D62
P 8500 12750
F 0 "C101" H 8618 12796 50  0000 L CNN
F 1 "10u" H 8618 12705 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 8538 12600 50  0001 C CNN
F 3 "~" H 8500 12750 50  0001 C CNN
	1    8500 12750
	0    1    1    0   
$EndComp
Wire Wire Line
	8650 12750 8700 12750
Connection ~ 8700 12750
Wire Wire Line
	8200 9850 8400 9850
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R102
U 1 1 5D389F61
P 7800 8950
F 0 "R102" V 7880 8950 50  0000 C CNN
F 1 "22k" V 7800 8950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7730 8950 50  0001 C CNN
F 3 "" H 7800 8950 50  0001 C CNN
	1    7800 8950
	0    1    1    0   
$EndComp
Wire Wire Line
	7650 8950 7500 8950
Connection ~ 7500 8950
Wire Wire Line
	7500 8950 7500 9750
Wire Wire Line
	7950 8950 8400 8950
Wire Wire Line
	8400 8950 8400 9850
Connection ~ 8400 9850
Wire Wire Line
	8400 9850 8600 9850
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R103
U 1 1 5D39D890
P 7800 10600
F 0 "R103" V 7880 10600 50  0000 C CNN
F 1 "22k" V 7800 10600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7730 10600 50  0001 C CNN
F 3 "" H 7800 10600 50  0001 C CNN
	1    7800 10600
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 9950 7450 10600
Wire Wire Line
	7650 10600 7450 10600
Connection ~ 7450 10600
Wire Wire Line
	8600 10150 8600 10200
Wire Wire Line
	7950 10600 8250 10600
Wire Wire Line
	8250 10600 8250 11150
Wire Wire Line
	8250 11150 10250 11150
Wire Wire Line
	10250 11150 10250 10100
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R105
U 1 1 5D3AC955
P 8950 10000
F 0 "R105" V 9030 10000 50  0000 C CNN
F 1 "10k" V 8950 10000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 8880 10000 50  0001 C CNN
F 3 "" H 8950 10000 50  0001 C CNN
	1    8950 10000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8750 10000 8800 10000
Wire Wire Line
	9100 10000 9300 10000
Connection ~ 9300 10000
$Comp
L filtre-sallenkey-rescue:C-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue C106
U 1 1 5D3BB6DE
P 11700 9400
F 0 "C106" H 11725 9500 50  0000 L CNN
F 1 "100n" H 11725 9300 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 11738 9250 50  0001 C CNN
F 3 "" H 11700 9400 50  0001 C CNN
	1    11700 9400
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0106
U 1 1 5D3BB6E6
P 11400 10500
F 0 "#PWR0106" H 11400 10250 50  0001 C CNN
F 1 "GND" H 11400 10350 50  0000 C CNN
F 2 "" H 11400 10500 50  0001 C CNN
F 3 "" H 11400 10500 50  0001 C CNN
	1    11400 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	11400 10450 11400 10500
Wire Wire Line
	11450 10450 11400 10450
Wire Wire Line
	12050 10350 12150 10350
Wire Wire Line
	11850 9400 12150 9400
Wire Wire Line
	11350 9400 11550 9400
Wire Wire Line
	11350 10250 11350 9400
Wire Wire Line
	12150 9400 12150 10350
Wire Wire Line
	11350 10250 11450 10250
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R108
U 1 1 5D3BB705
P 10650 10600
F 0 "R108" V 10730 10600 50  0000 C CNN
F 1 "2k2" V 10650 10600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10580 10600 50  0001 C CNN
F 3 "" H 10650 10600 50  0001 C CNN
	1    10650 10600
	-1   0    0    1   
$EndComp
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0107
U 1 1 5D3BB70B
P 10650 11050
F 0 "#PWR0107" H 10650 10800 50  0001 C CNN
F 1 "GND" H 10650 10900 50  0000 C CNN
F 2 "" H 10650 11050 50  0001 C CNN
F 3 "" H 10650 11050 50  0001 C CNN
	1    10650 11050
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C105
U 1 1 5D3BB711
P 10650 10900
F 0 "C105" H 10768 10946 50  0000 L CNN
F 1 "100u" H 10768 10855 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 10688 10750 50  0001 C CNN
F 3 "~" H 10650 10900 50  0001 C CNN
	1    10650 10900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 10400 10650 10450
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R110
U 1 1 5D3BB721
P 11000 10250
F 0 "R110" V 11080 10250 50  0000 C CNN
F 1 "10k" V 11000 10250 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10930 10250 50  0001 C CNN
F 3 "" H 11000 10250 50  0001 C CNN
	1    11000 10250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10800 10250 10850 10250
Wire Wire Line
	11150 10250 11350 10250
Connection ~ 11350 10250
Connection ~ 10250 10100
Wire Wire Line
	10250 10100 10650 10100
Wire Wire Line
	12150 9400 12150 8600
Connection ~ 12150 9400
Wire Wire Line
	7950 8600 12150 8600
Connection ~ 9150 12750
$Comp
L Device:CP C104
U 1 1 5D3FC241
P 10350 12650
F 0 "C104" H 10468 12696 50  0000 L CNN
F 1 "10u" H 10468 12605 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 10388 12500 50  0001 C CNN
F 3 "~" H 10350 12650 50  0001 C CNN
	1    10350 12650
	0    1    1    0   
$EndComp
$Comp
L filtre-sallenkey-rescue:Conn_01x02-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue J101
U 1 1 5D3FC350
P 11400 12650
F 0 "J101" H 11400 12750 50  0000 C CNN
F 1 "X" H 11400 12450 50  0000 C CNN
F 2 "resistance-grospad:PinHeader_1x02_P2.54mm_Vertical" H 11400 12650 50  0001 C CNN
F 3 "" H 11400 12650 50  0001 C CNN
	1    11400 12650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 11900 8900 11550
Wire Wire Line
	8900 11550 7450 11550
Wire Wire Line
	7450 11550 7450 11400
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R101
U 1 1 5D3FE685
P 7450 11250
F 0 "R101" V 7530 11250 50  0000 C CNN
F 1 "22k" V 7450 11250 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7380 11250 50  0001 C CNN
F 3 "" H 7450 11250 50  0001 C CNN
	1    7450 11250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7450 10600 7450 11100
Wire Wire Line
	6800 10600 7450 10600
Wire Wire Line
	6650 10450 6550 10450
Wire Wire Line
	7100 9750 6800 9750
Wire Wire Line
	6800 9750 6800 10300
Wire Wire Line
	10250 11150 10250 11300
Connection ~ 10250 11150
$Comp
L Device:CP C103
U 1 1 5D418610
P 10250 11450
F 0 "C103" H 10368 11496 50  0000 L CNN
F 1 "10u" H 10368 11405 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 10288 11300 50  0001 C CNN
F 3 "~" H 10250 11450 50  0001 C CNN
	1    10250 11450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 12550 9200 12550
Wire Wire Line
	9200 12550 9200 11900
Wire Wire Line
	10250 11600 10250 11900
Wire Wire Line
	10250 11900 9200 11900
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R109
U 1 1 5D439B17
P 10650 12650
F 0 "R109" V 10730 12650 50  0000 C CNN
F 1 "100" V 10650 12650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10580 12650 50  0001 C CNN
F 3 "" H 10650 12650 50  0001 C CNN
	1    10650 12650
	0    1    1    0   
$EndComp
Wire Wire Line
	10800 12650 11200 12650
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0108
U 1 1 5D43C41F
P 11200 12750
F 0 "#PWR0108" H 11200 12500 50  0001 C CNN
F 1 "GND" H 11200 12600 50  0000 C CNN
F 2 "" H 11200 12750 50  0001 C CNN
F 3 "" H 11200 12750 50  0001 C CNN
	1    11200 12750
	0    1    1    0   
$EndComp
Connection ~ 10250 11900
$Comp
L filtre-sallenkey-rescue:GND-power-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue #PWR0109
U 1 1 5D43C8E4
P 10550 11900
F 0 "#PWR0109" H 10550 11650 50  0001 C CNN
F 1 "GND" H 10550 11750 50  0000 C CNN
F 2 "" H 10550 11900 50  0001 C CNN
F 3 "" H 10550 11900 50  0001 C CNN
	1    10550 11900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_POT RV101
U 1 1 5D43D0B8
P 6800 10450
F 0 "RV101" H 6731 10404 50  0000 R CNN
F 1 "Q 10k" H 6731 10495 50  0000 R CNN
F 2 "resistance-grospad:connection-3-padcarre2mm" H 6800 10450 50  0001 C CNN
F 3 "~" H 6800 10450 50  0001 C CNN
	1    6800 10450
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT RV103
U 1 1 5D43D552
P 8900 12050
F 0 "RV103" V 8786 12050 50  0000 C CNN
F 1 "Boost-Cut 10k" V 8695 12050 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 8900 12050 50  0001 C CNN
F 3 "~" H 8900 12050 50  0001 C CNN
	1    8900 12050
	0    1    -1   0   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV102
U 1 1 5D440061
P 8600 10000
F 0 "RV102" H 8530 9954 50  0000 R CNN
F 1 "10k Freq" H 8500 10200 50  0000 R CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Double_Vertical-padcarre2.5" H 8600 10000 50  0001 C CNN
F 3 "~" H 8600 10000 50  0001 C CNN
	1    8600 10000
	1    0    0    1   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV102
U 2 1 5D4428D9
P 10650 10250
F 0 "RV102" H 10580 10204 50  0000 R CNN
F 1 "10k Freq" H 10580 10295 50  0000 R CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Double_Vertical-padcarre2.5" H 10650 10250 50  0001 C CNN
F 3 "~" H 10650 10250 50  0001 C CNN
	2    10650 10250
	1    0    0    1   
$EndComp
Wire Wire Line
	7500 15150 7650 15150
Wire Wire Line
	7650 15150 7650 15350
Wire Wire Line
	7650 15350 7950 15350
Wire Wire Line
	7650 14900 7650 15150
Wire Wire Line
	7750 14900 7750 15050
Connection ~ 7650 15150
Wire Wire Line
	7500 15050 7750 15050
Wire Wire Line
	7750 15050 7950 15050
Connection ~ 7750 15050
$Comp
L Amplifier_Operational:TL074 U101
U 3 1 5D3CC1C7
P 9700 10100
F 0 "U101" H 9700 9733 50  0000 C CNN
F 1 "TL074" H 9700 9824 50  0000 C CNN
F 2 "resistance-grospad:DIP-14_W7.62mm_LongPads-padcarre2mm" H 9650 10200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 9750 10300 50  0001 C CNN
	3    9700 10100
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL074 U101
U 4 1 5D3D2D75
P 11750 10350
F 0 "U101" H 11750 9983 50  0000 C CNN
F 1 "TL074" H 11750 10074 50  0000 C CNN
F 2 "resistance-grospad:DIP-14_W7.62mm_LongPads-padcarre2mm" H 11700 10450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 11800 10550 50  0001 C CNN
	4    11750 10350
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL074 U101
U 2 1 5D3D30B5
P 7900 9850
F 0 "U101" H 7900 9483 50  0000 C CNN
F 1 "TL074" H 7900 9574 50  0000 C CNN
F 2 "resistance-grospad:DIP-14_W7.62mm_LongPads-padcarre2mm" H 7850 9950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 7950 10050 50  0001 C CNN
	2    7900 9850
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL074 U101
U 1 1 5D3D91D0
P 9600 12650
F 0 "U101" H 9600 13017 50  0000 C CNN
F 1 "TL074" H 9600 12926 50  0000 C CNN
F 2 "resistance-grospad:DIP-14_W7.62mm_LongPads-padcarre2mm" H 9550 12750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 9650 12850 50  0001 C CNN
	1    9600 12650
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL074 U101
U 5 1 5D3DB602
P 8700 15150
F 0 "U101" H 8658 15196 50  0000 L CNN
F 1 "TL074" H 8658 15105 50  0000 L CNN
F 2 "resistance-grospad:DIP-14_W7.62mm_LongPads-padcarre2mm" H 8650 15250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 8750 15350 50  0001 C CNN
	5    8700 15150
	1    0    0    -1  
$EndComp
Text GLabel 8600 14850 1    39   Input ~ 0
+15
Text GLabel 8600 15450 3    39   Input ~ 0
-15
$Comp
L filtre-sallenkey-rescue:R-device-filtre-sallenkey-rescue-filtre-sallenkey-rescue-filtre-sallenkey-rescue R107
U 1 1 5D43C862
P 10400 11900
F 0 "R107" V 10480 11900 50  0000 C CNN
F 1 "100k" V 10400 11900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10330 11900 50  0001 C CNN
F 3 "" H 10400 11900 50  0001 C CNN
	1    10400 11900
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR0110
U 1 1 6070D952
P 6600 12950
F 0 "#PWR0110" H 6600 12700 50  0001 C CNN
F 1 "GNDD" H 6604 12795 50  0000 C CNN
F 2 "" H 6600 12950 50  0001 C CNN
F 3 "" H 6600 12950 50  0001 C CNN
	1    6600 12950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J2
U 1 1 6070EAB1
P 6600 12750
F 0 "J2" V 6538 12662 50  0000 R CNN
F 1 "earth" V 6447 12662 50  0000 R CNN
F 2 "resistance-grospad:PINTST-2mm" H 6600 12750 50  0001 C CNN
F 3 "~" H 6600 12750 50  0001 C CNN
	1    6600 12750
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
