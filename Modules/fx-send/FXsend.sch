EESchema Schematic File Version 4
LIBS:FXsend-cache
EELAYER 29 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 15700 850 
NoConn ~ 15700 1000
$Comp
L FXsend-rescue:TL072-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue U101
U 1 1 5D2AE375
P 7200 4800
F 0 "U101" H 7150 5000 50  0000 L CNN
F 1 "ne5532" H 7150 4550 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 7200 4800 50  0001 C CNN
F 3 "" H 7200 4800 50  0000 C CNN
	1    7200 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5450 6600 4900
Wire Wire Line
	7700 5450 7700 4800
Wire Wire Line
	7700 4800 7500 4800
Wire Wire Line
	6600 5450 6850 5450
NoConn ~ -2750 4600
Text GLabel 6750 8800 2    60   Input ~ 0
+v
Text GLabel 6750 9600 2    60   Input ~ 0
-v
Text Label 6450 8800 0    60   ~ 0
+16v
Text Label 6450 9600 0    60   ~ 0
-16v
$Comp
L FXsend-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #FLG?
U 1 1 5D2AE377
P 5650 9200
F 0 "#FLG?" H 5650 9295 50  0001 C CNN
F 1 "PWR_FLAG" H 5650 9380 50  0000 C CNN
F 2 "" H 5650 9200 50  0000 C CNN
F 3 "" H 5650 9200 50  0000 C CNN
	1    5650 9200
	1    0    0    -1  
$EndComp
$Comp
L FXsend-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #FLG?
U 1 1 5D2AE378
P 6150 8750
F 0 "#FLG?" H 6150 8845 50  0001 C CNN
F 1 "PWR_FLAG" H 6150 8930 50  0000 C CNN
F 2 "" H 6150 8750 50  0000 C CNN
F 3 "" H 6150 8750 50  0000 C CNN
	1    6150 8750
	1    0    0    -1  
$EndComp
$Comp
L FXsend-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #FLG?
U 1 1 5CD30825
P 6100 9600
F 0 "#FLG?" H 6100 9695 50  0001 C CNN
F 1 "PWR_FLAG" H 6100 9780 50  0000 C CNN
F 2 "" H 6100 9600 50  0000 C CNN
F 3 "" H 6100 9600 50  0000 C CNN
	1    6100 9600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 9600 5650 9600
Wire Wire Line
	5500 8800 5650 8800
Wire Wire Line
	5950 9600 6100 9600
Wire Wire Line
	6600 8800 6600 8850
Connection ~ 6300 8800
Wire Wire Line
	6300 8800 6300 8850
Wire Wire Line
	5950 8800 6150 8800
Wire Wire Line
	5500 9000 5500 9600
Wire Wire Line
	6150 8750 6150 8800
Wire Wire Line
	6100 9600 6100 9700
Connection ~ 6100 9600
Wire Wire Line
	5650 8900 5650 9200
Connection ~ 5650 9200
Wire Wire Line
	5300 9000 5500 9000
Wire Wire Line
	5300 8900 5500 8900
Wire Wire Line
	5500 8900 5500 8800
Wire Wire Line
	5300 8800 5300 8750
Wire Wire Line
	5300 8750 5600 8750
Wire Wire Line
	5600 8750 5600 8900
Wire Wire Line
	5600 8900 5650 8900
Wire Wire Line
	6300 9150 6300 9600
Wire Wire Line
	6600 9150 6600 9600
Connection ~ 6300 9600
Wire Wire Line
	6300 9600 6600 9600
Wire Wire Line
	6100 9600 6300 9600
Connection ~ 6150 8800
Wire Wire Line
	6300 8800 6600 8800
Wire Wire Line
	6150 8800 6300 8800
Connection ~ 6600 8800
Connection ~ 6600 9600
$Comp
L FXsend-rescue:GNDD-power-riaav3-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5D2AE381
P 5650 9200
F 0 "#PWR?" H 5650 8950 50  0001 C CNN
F 1 "GNDD-power" H 5654 9045 50  0000 C CNN
F 2 "" H 5650 9200 50  0001 C CNN
F 3 "" H 5650 9200 50  0001 C CNN
	1    5650 9200
	1    0    0    -1  
$EndComp
$Comp
L FXsend-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5D2ED105
P 6900 4700
F 0 "#PWR?" H 6900 4450 50  0001 C CNN
F 1 "GND" H 6900 4550 50  0000 C CNN
F 2 "" H 6900 4700 50  0000 C CNN
F 3 "" H 6900 4700 50  0000 C CNN
	1    6900 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	6600 4900 6900 4900
Text GLabel 7150 4400 2    60   Input ~ 0
+v
Text GLabel 7150 5200 2    60   Input ~ 0
-v
Wire Wire Line
	7150 5200 7100 5200
Wire Wire Line
	7100 5200 7100 5100
$Comp
L FXsend-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue C101
U 1 1 5CCEB1B0
P 6300 9000
F 0 "C101" H 6325 9100 50  0000 L CNN
F 1 "100n" H 6325 8900 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6338 8850 50  0001 C CNN
F 3 "" H 6300 9000 50  0000 C CNN
	1    6300 9000
	1    0    0    -1  
$EndComp
$Comp
L FXsend-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue R102
U 1 1 5D2AE37C
P 5800 8800
F 0 "R102" V 5880 8800 50  0000 C CNN
F 1 "2R2" V 5800 8800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5730 8800 50  0001 C CNN
F 3 "" H 5800 8800 50  0000 C CNN
	1    5800 8800
	0    1    1    0   
$EndComp
$Comp
L FXsend-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue R103
U 1 1 5CCEB1B3
P 5800 9600
F 0 "R103" V 5880 9600 50  0000 C CNN
F 1 "2R2" V 5800 9600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5730 9600 50  0001 C CNN
F 3 "" H 5800 9600 50  0000 C CNN
	1    5800 9600
	0    1    1    0   
$EndComp
Text Label 6450 8800 0    60   ~ 0
+16v
Text Label 6450 9600 0    60   ~ 0
-16v
$Comp
L FXsend-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #FLG?
U 1 1 5AA41643
P 5650 9200
F 0 "#FLG?" H 5650 9295 50  0001 C CNN
F 1 "PWR_FLAG" H 5650 9380 50  0000 C CNN
F 2 "" H 5650 9200 50  0000 C CNN
F 3 "" H 5650 9200 50  0000 C CNN
	1    5650 9200
	1    0    0    -1  
$EndComp
$Comp
L FXsend-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #FLG?
U 1 1 5AA41910
P 6150 8750
F 0 "#FLG?" H 6150 8845 50  0001 C CNN
F 1 "PWR_FLAG" H 6150 8930 50  0000 C CNN
F 2 "" H 6150 8750 50  0000 C CNN
F 3 "" H 6150 8750 50  0000 C CNN
	1    6150 8750
	1    0    0    -1  
$EndComp
$Comp
L FXsend-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #FLG?
U 1 1 5D2AE382
P 6100 9600
F 0 "#FLG?" H 6100 9695 50  0001 C CNN
F 1 "PWR_FLAG" H 6100 9780 50  0000 C CNN
F 2 "" H 6100 9600 50  0000 C CNN
F 3 "" H 6100 9600 50  0000 C CNN
	1    6100 9600
	1    0    0    -1  
$EndComp
$Comp
L FXsend-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue J102
U 1 1 5D2AE37E
P 5100 8900
F 0 "J102" H 5100 9100 50  0000 C CNN
F 1 "alim" H 5100 8700 50  0000 C CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 5100 8900 50  0001 C CNN
F 3 "" H 5100 8900 50  0001 C CNN
	1    5100 8900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6800 8800 6800 8850
Wire Wire Line
	6800 9150 6800 9600
$Comp
L FXsend-rescue:GNDD-power-riaav3-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5CCFA5FC
P 5650 9200
F 0 "#PWR?" H 5650 8950 50  0001 C CNN
F 1 "GNDD-power" H 5654 9045 50  0000 C CNN
F 2 "" H 5650 9200 50  0001 C CNN
F 3 "" H 5650 9200 50  0001 C CNN
	1    5650 9200
	1    0    0    -1  
$EndComp
NoConn ~ 11300 1750
$Comp
L FXsend-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue J106
U 1 1 5D2AE379
P 9700 4900
F 0 "J106" H 9700 5000 50  0000 C CNN
F 1 "FX-out" H 10050 4850 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 9700 4900 50  0001 C CNN
F 3 "" H 9700 4900 50  0001 C CNN
	1    9700 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 5000 9500 5000
$Comp
L FXsend-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5D2AE374
P 9450 5000
F 0 "#PWR?" H 9450 4750 50  0001 C CNN
F 1 "GND" H 9450 4850 50  0000 C CNN
F 2 "" H 9450 5000 50  0000 C CNN
F 3 "" H 9450 5000 50  0000 C CNN
	1    9450 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	7150 4400 7100 4400
Wire Wire Line
	7100 4400 7100 4500
Wire Wire Line
	6600 9600 6800 9600
Wire Wire Line
	6600 8800 6800 8800
Wire Wire Line
	8450 5300 8450 5200
Wire Wire Line
	8500 5300 8450 5300
Wire Wire Line
	8900 4900 8850 4900
$Comp
L FXsend-rescue:TL072-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue U101
U 2 1 5D2B96B3
P 8550 4900
F 0 "U101" H 8500 5100 50  0000 L CNN
F 1 "ne5532" H 8500 4650 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 8550 4900 50  0001 C CNN
F 3 "" H 8550 4900 50  0000 C CNN
	2    8550 4900
	1    0    0    -1  
$EndComp
NoConn ~ 8250 4650
$Comp
L FXsend-rescue:POT-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue RV101
U 1 1 5D2D4EE9
P 7100 5450
F 0 "RV101" V 6925 5450 50  0000 C CNN
F 1 "FX1-20k" V 7000 5450 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 7100 5450 50  0001 C CNN
F 3 "" H 7100 5450 50  0001 C CNN
	1    7100 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	8450 4600 8450 4500
Wire Wire Line
	8450 4500 8500 4500
Wire Wire Line
	8900 4900 8900 5500
Wire Wire Line
	8900 5500 8150 5500
Wire Wire Line
	8150 5500 8150 5000
Wire Wire Line
	8150 5000 8250 5000
$Comp
L FXsend-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue R101
U 1 1 5D33EC22
P 7900 4800
F 0 "R101" V 7980 4800 50  0000 C CNN
F 1 "220" V 7900 4800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7830 4800 50  0001 C CNN
F 3 "" H 7900 4800 50  0000 C CNN
	1    7900 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 4800 8250 4800
Wire Wire Line
	7750 4800 7700 4800
Connection ~ 7700 4800
$Comp
L FXsend-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue R105
U 1 1 5D342230
P 9350 4900
F 0 "R105" V 9400 4900 50  0000 C CNN
F 1 "100" V 9350 4900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9280 4900 50  0001 C CNN
F 3 "" H 9350 4900 50  0000 C CNN
	1    9350 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9200 4900 8900 4900
Connection ~ 8900 4900
$Comp
L FXsend-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue J101
U 1 1 5D346E75
P 4900 5550
F 0 "J101" H 4900 5650 50  0000 C CNN
F 1 "inL" H 5250 5500 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 4900 5550 50  0001 C CNN
F 3 "" H 4900 5550 50  0001 C CNN
	1    4900 5550
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 5450 6150 5450
Connection ~ 6600 5450
$Comp
L FXsend-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5D347F07
P 5100 5550
F 0 "#PWR?" H 5100 5300 50  0001 C CNN
F 1 "GND" H 5100 5150 50  0000 C CNN
F 2 "" H 5100 5550 50  0000 C CNN
F 3 "" H 5100 5550 50  0000 C CNN
	1    5100 5550
	0    -1   -1   0   
$EndComp
$Comp
L FXsend-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue C102
U 1 1 5D348BA5
P 6000 5450
F 0 "C102" H 6025 5550 50  0000 L CNN
F 1 "10u" H 6025 5350 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P100mm-pad2mm" H 6038 5300 50  0001 C CNN
F 3 "" H 6000 5450 50  0000 C CNN
	1    6000 5450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 5450 5100 5450
$Comp
L FXsend-rescue:TL072-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue U102
U 1 1 5D34A7DC
P 7250 3450
F 0 "U102" H 7200 3650 50  0000 L CNN
F 1 "ne5532" H 7200 3200 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 7250 3450 50  0001 C CNN
F 3 "" H 7250 3450 50  0000 C CNN
	1    7250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4100 6650 3550
Wire Wire Line
	7750 4100 7750 3450
Wire Wire Line
	7750 3450 7550 3450
Wire Wire Line
	6650 4100 6850 4100
$Comp
L FXsend-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5D34A7E7
P 6950 3350
F 0 "#PWR?" H 6950 3100 50  0001 C CNN
F 1 "GND" H 6950 3200 50  0000 C CNN
F 2 "" H 6950 3350 50  0000 C CNN
F 3 "" H 6950 3350 50  0000 C CNN
	1    6950 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 3550 6950 3550
Text GLabel 7200 3050 2    60   Input ~ 0
+v
Text GLabel 7200 3850 2    60   Input ~ 0
-v
Wire Wire Line
	7200 3850 7150 3850
Wire Wire Line
	7150 3850 7150 3750
$Comp
L FXsend-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue J104
U 1 1 5D34A7F2
P 9750 3550
F 0 "J104" H 9750 3650 50  0000 C CNN
F 1 "FX-out" H 10100 3500 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 9750 3550 50  0001 C CNN
F 3 "" H 9750 3550 50  0001 C CNN
	1    9750 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 3650 9550 3650
$Comp
L FXsend-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5D34A7F9
P 9500 3650
F 0 "#PWR?" H 9500 3400 50  0001 C CNN
F 1 "GND" H 9500 3500 50  0000 C CNN
F 2 "" H 9500 3650 50  0000 C CNN
F 3 "" H 9500 3650 50  0000 C CNN
	1    9500 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 3050 7150 3050
Wire Wire Line
	7150 3050 7150 3150
Wire Wire Line
	8500 3950 8500 3850
Wire Wire Line
	8550 3950 8500 3950
Text GLabel 8550 3950 2    60   Input ~ 0
-v
Text GLabel 8550 3150 2    60   Input ~ 0
+v
Wire Wire Line
	8950 3550 8900 3550
$Comp
L FXsend-rescue:TL072-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue U102
U 2 1 5D34A806
P 8600 3550
F 0 "U102" H 8550 3750 50  0000 L CNN
F 1 "ne5532" H 8550 3300 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 8600 3550 50  0001 C CNN
F 3 "" H 8600 3550 50  0000 C CNN
	2    8600 3550
	1    0    0    -1  
$EndComp
NoConn ~ 8300 3300
$Comp
L FXsend-rescue:POT-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue RV102
U 1 1 5D34A80D
P 7150 4100
F 0 "RV102" V 6975 4100 50  0000 C CNN
F 1 "FX1-20k" V 7050 4100 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 7150 4100 50  0001 C CNN
F 3 "" H 7150 4100 50  0001 C CNN
	1    7150 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	8500 3250 8500 3150
Wire Wire Line
	8500 3150 8550 3150
Wire Wire Line
	8950 3550 8950 4150
Wire Wire Line
	8950 4150 8200 4150
Wire Wire Line
	8200 4150 8200 3650
Wire Wire Line
	8200 3650 8300 3650
$Comp
L FXsend-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue R104
U 1 1 5D34A81D
P 7950 3450
F 0 "R104" V 8030 3450 50  0000 C CNN
F 1 "220" V 7950 3450 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7880 3450 50  0001 C CNN
F 3 "" H 7950 3450 50  0000 C CNN
	1    7950 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8100 3450 8300 3450
Wire Wire Line
	7800 3450 7750 3450
Connection ~ 7750 3450
$Comp
L FXsend-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue R106
U 1 1 5D34A826
P 9400 3550
F 0 "R106" V 9450 3550 50  0000 C CNN
F 1 "100" V 9400 3550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9330 3550 50  0001 C CNN
F 3 "" H 9400 3550 50  0000 C CNN
	1    9400 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9250 3550 8950 3550
Connection ~ 8950 3550
$Comp
L FXsend-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue J103
U 1 1 5D34A82E
P 4950 4200
F 0 "J103" H 4950 4300 50  0000 C CNN
F 1 "inL" H 5300 4150 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 4950 4200 50  0001 C CNN
F 3 "" H 4950 4200 50  0001 C CNN
	1    4950 4200
	-1   0    0    1   
$EndComp
$Comp
L FXsend-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue #PWR?
U 1 1 5D34A836
P 5150 4200
F 0 "#PWR?" H 5150 3950 50  0001 C CNN
F 1 "GND" H 5150 3800 50  0000 C CNN
F 2 "" H 5150 4200 50  0000 C CNN
F 3 "" H 5150 4200 50  0000 C CNN
	1    5150 4200
	0    -1   -1   0   
$EndComp
$Comp
L FXsend-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue C103
U 1 1 5D34A83C
P 6050 4100
F 0 "C103" H 6075 4200 50  0000 L CNN
F 1 "10u" H 6075 4000 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P100mm-pad2mm" H 6088 3950 50  0001 C CNN
F 3 "" H 6050 4100 50  0000 C CNN
	1    6050 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 4100 5150 4100
Wire Wire Line
	7250 5450 7700 5450
Wire Wire Line
	7150 4250 6850 4250
Wire Wire Line
	6850 4250 6850 4100
Connection ~ 6850 4100
Wire Wire Line
	6850 4100 7000 4100
Wire Wire Line
	7100 5600 6850 5600
Wire Wire Line
	6850 5600 6850 5450
Connection ~ 6850 5450
Wire Wire Line
	6850 5450 6950 5450
$Comp
L FXsend-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue-FXsend-rescue C104
U 1 1 5D3534AC
P 6600 9000
F 0 "C104" H 6625 9100 50  0000 L CNN
F 1 "100n" H 6625 8900 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6638 8850 50  0001 C CNN
F 3 "" H 6600 9000 50  0000 C CNN
	1    6600 9000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4100 7750 4100
Connection ~ 6650 4100
Wire Wire Line
	6650 4100 6200 4100
$EndSCHEMATC
