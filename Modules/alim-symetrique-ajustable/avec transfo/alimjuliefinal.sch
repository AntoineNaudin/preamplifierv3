EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L alimjuliefinal-rescue:CONN_01X03-alimjuliefinal-rescue-alimjuliefinal-rescue P1
U 1 1 5968A90B
P 150 4100
F 0 "P1" H 150 4300 50  0000 C CNN
F 1 "Transfo" V 250 4100 50  0000 C CNN
F 2 "Connectors:AK300-3" H 150 4100 50  0001 C CNN
F 3 "" H 150 4100 50  0000 C CNN
	1    150  4100
	-1   0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR01
U 1 1 5968B288
P 3200 4000
F 0 "#PWR01" H 3200 3750 50  0001 C CNN
F 1 "GND" H 3200 3850 50  0000 C CNN
F 2 "" H 3200 4000 50  0000 C CNN
F 3 "" H 3200 4000 50  0000 C CNN
	1    3200 4000
	0    -1   -1   0   
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C10
U 1 1 5968B788
P 6100 3750
F 0 "C10" H 6125 3850 50  0000 L CNN
F 1 "100n" H 6125 3650 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 6138 3600 50  0001 C CNN
F 3 "" H 6100 3750 50  0000 C CNN
	1    6100 3750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C11
U 1 1 5968B907
P 6100 4250
F 0 "C11" H 6125 4350 50  0000 L CNN
F 1 "100n" H 6125 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 6138 4100 50  0001 C CNN
F 3 "" H 6100 4250 50  0000 C CNN
	1    6100 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C8
U 1 1 5968B9F3
P 5850 3750
F 0 "C8" H 5875 3850 50  0000 L CNN
F 1 "2200" H 5875 3650 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D16.0mm_P7.50mm" H 5888 3600 50  0001 C CNN
F 3 "" H 5850 3750 50  0000 C CNN
	1    5850 3750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C9
U 1 1 5968BA2B
P 5850 4250
F 0 "C9" H 5875 4350 50  0000 L CNN
F 1 "2200" H 5875 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D16.0mm_P7.50mm" H 5888 4100 50  0001 C CNN
F 3 "" H 5850 4250 50  0000 C CNN
	1    5850 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C6
U 1 1 5968BF10
P 5600 3750
F 0 "C6" H 5625 3850 50  0000 L CNN
F 1 "2200" H 5625 3650 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D16.0mm_P7.50mm" H 5638 3600 50  0001 C CNN
F 3 "" H 5600 3750 50  0000 C CNN
	1    5600 3750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C7
U 1 1 5968BF54
P 5600 4250
F 0 "C7" H 5625 4350 50  0000 L CNN
F 1 "2200" H 5625 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D16.0mm_P7.50mm" H 5638 4100 50  0001 C CNN
F 3 "" H 5600 4250 50  0000 C CNN
	1    5600 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C13
U 1 1 5968C679
P 6700 4250
F 0 "C13" H 6725 4350 50  0000 L CNN
F 1 "100n" H 6725 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 6738 4100 50  0001 C CNN
F 3 "" H 6700 4250 50  0000 C CNN
	1    6700 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R8
U 1 1 5968CAA3
P 7650 4750
F 0 "R8" V 7730 4750 50  0000 C CNN
F 1 "220" V 7650 4750 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7580 4750 50  0001 C CNN
F 3 "" H 7650 4750 50  0000 C CNN
	1    7650 4750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D5
U 1 1 5968CBD7
P 7900 4700
F 0 "D5" H 7900 4800 50  0000 C CNN
F 1 "1N4007" H 7900 4600 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P10.16mm_Horizontal" H 7900 4700 50  0001 C CNN
F 3 "" H 7900 4700 50  0000 C CNN
	1    7900 4700
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C17
U 1 1 5968CDB8
P 8200 4550
F 0 "C17" H 8225 4650 50  0000 L CNN
F 1 "47u" H 8225 4450 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 8238 4400 50  0001 C CNN
F 3 "" H 8200 4550 50  0000 C CNN
	1    8200 4550
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C12
U 1 1 5968D436
P 6700 3700
F 0 "C12" H 6725 3800 50  0000 L CNN
F 1 "100n" H 6725 3600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 6738 3550 50  0001 C CNN
F 3 "" H 6700 3700 50  0000 C CNN
	1    6700 3700
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R7
U 1 1 5968D556
P 7650 3250
F 0 "R7" V 7730 3250 50  0000 C CNN
F 1 "220" V 7650 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7580 3250 50  0001 C CNN
F 3 "" H 7650 3250 50  0000 C CNN
	1    7650 3250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D4
U 1 1 5968D55C
P 7900 3250
F 0 "D4" H 7900 3350 50  0000 C CNN
F 1 "1N4007" H 7900 3150 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P10.16mm_Horizontal" H 7900 3250 50  0001 C CNN
F 3 "" H 7900 3250 50  0000 C CNN
	1    7900 3250
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C16
U 1 1 5968D562
P 8200 3350
F 0 "C16" H 8225 3450 50  0000 L CNN
F 1 "47u" H 8225 3250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 8238 3200 50  0001 C CNN
F 3 "" H 8200 3350 50  0000 C CNN
	1    8200 3350
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D3
U 1 1 5968DD68
P 7150 5500
F 0 "D3" H 7150 5600 50  0000 C CNN
F 1 "1N4007" H 7150 5400 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_A-405_P7.62mm_Horizontal" H 7150 5500 50  0001 C CNN
F 3 "" H 7150 5500 50  0000 C CNN
	1    7150 5500
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D2
U 1 1 5968DEBC
P 7100 2400
F 0 "D2" H 7100 2500 50  0000 C CNN
F 1 "1N4007" H 7100 2300 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_A-405_P7.62mm_Horizontal" H 7100 2400 50  0001 C CNN
F 3 "" H 7100 2400 50  0000 C CNN
	1    7100 2400
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR02
U 1 1 59690C0D
P 8650 4000
F 0 "#PWR02" H 8650 3750 50  0001 C CNN
F 1 "GND" H 8650 3850 50  0000 C CNN
F 2 "" H 8650 4000 50  0000 C CNN
F 3 "" H 8650 4000 50  0000 C CNN
	1    8650 4000
	0    -1   -1   0   
$EndComp
Text Notes 8650 3050 0    60   ~ 0
+16\n
Text Notes 8650 4900 0    60   ~ 0
-16\n
$Comp
L alimjuliefinal-rescue:PWR_FLAG-power-alimjuliefinal-rescue #FLG03
U 1 1 59904790
P 3100 4300
F 0 "#FLG03" H 3100 4395 50  0001 C CNN
F 1 "PWR_FLAG" H 3100 4480 50  0000 C CNN
F 2 "" H 3100 4300 50  0000 C CNN
F 3 "" H 3100 4300 50  0000 C CNN
	1    3100 4300
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:PWR_FLAG-power-alimjuliefinal-rescue #FLG04
U 1 1 599049D9
P 3600 4300
F 0 "#FLG04" H 3600 4395 50  0001 C CNN
F 1 "PWR_FLAG" H 3600 4480 50  0000 C CNN
F 2 "" H 3600 4300 50  0000 C CNN
F 3 "" H 3600 4300 50  0000 C CNN
	1    3600 4300
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:PWR_FLAG-power-alimjuliefinal-rescue #FLG05
U 1 1 59904A5F
P 3800 4300
F 0 "#FLG05" H 3800 4395 50  0001 C CNN
F 1 "PWR_FLAG" H 3800 4480 50  0000 C CNN
F 2 "" H 3800 4300 50  0000 C CNN
F 3 "" H 3800 4300 50  0000 C CNN
	1    3800 4300
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:CONN_01X03-alimjuliefinal-rescue-alimjuliefinal-rescue P4
U 1 1 599060A2
P 9450 3600
F 0 "P4" H 9450 3800 50  0000 C CNN
F 1 "filtreparam" V 9550 3600 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 9450 3600 50  0001 C CNN
F 3 "" H 9450 3600 50  0000 C CNN
	1    9450 3600
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CONN_01X03-alimjuliefinal-rescue-alimjuliefinal-rescue P3
U 1 1 5990672B
P 9450 3200
F 0 "P3" H 9450 3400 50  0000 C CNN
F 1 "Master" V 9550 3200 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 9450 3200 50  0001 C CNN
F 3 "" H 9450 3200 50  0000 C CNN
	1    9450 3200
	1    0    0    -1  
$EndComp
Text Notes 10250 4600 0    60   ~ 0
out \n1: -16v\n2:+16v\n3:GND
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR06
U 1 1 59908D8C
P 9050 6150
F 0 "#PWR06" H 9050 5900 50  0001 C CNN
F 1 "GND" H 9050 6000 50  0000 C CNN
F 2 "" H 9050 6150 50  0000 C CNN
F 3 "" H 9050 6150 50  0000 C CNN
	1    9050 6150
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CONN_01X03-alimjuliefinal-rescue-alimjuliefinal-rescue P2
U 1 1 5990B641
P 9450 2800
F 0 "P2" H 9450 3000 50  0000 C CNN
F 1 "AuxInsSend" V 9550 2800 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 9450 2800 50  0001 C CNN
F 3 "" H 9450 2800 50  0000 C CNN
	1    9450 2800
	1    0    0    -1  
$EndComp
Text Notes 4800 1900 0    60   ~ 0
BD245 remplacé par TIP35\nBD246 remplacé par TIP36
$Comp
L alimjuliefinal-rescue:LM317T-alimjuliefinal-rescue-alimjuliefinal-rescue U1
U 1 1 59910CF1
P 7150 3100
F 0 "U1" H 6950 3300 50  0000 C CNN
F 1 "LM317T" H 7150 3300 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 7150 3200 50  0000 C CIN
F 3 "" H 7150 3100 50  0000 C CNN
	1    7150 3100
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D102
U 1 1 59943EA8
P 4050 3800
F 0 "D102" H 4050 3900 50  0000 C CNN
F 1 "1N4936" H 4050 3700 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_A-405_P10.16mm_Horizontal" H 4050 3800 50  0001 C CNN
F 3 "" H 4050 3800 50  0000 C CNN
	1    4050 3800
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D104
U 1 1 59944125
P 4450 3850
F 0 "D104" H 4450 3950 50  0000 C CNN
F 1 "1N4936" H 4450 3750 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_A-405_P10.16mm_Horizontal" H 4450 3850 50  0001 C CNN
F 3 "" H 4450 3850 50  0000 C CNN
	1    4450 3850
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D103
U 1 1 5994446E
P 4300 4150
F 0 "D103" H 4300 4250 50  0000 C CNN
F 1 "1N4936" H 4300 4050 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_A-405_P10.16mm_Horizontal" H 4300 4150 50  0001 C CNN
F 3 "" H 4300 4150 50  0000 C CNN
	1    4300 4150
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D101
U 1 1 599446C5
P 4000 4150
F 0 "D101" H 4000 4250 50  0000 C CNN
F 1 "1N4936" H 4000 4050 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_A-405_P10.16mm_Horizontal" H 4000 4150 50  0001 C CNN
F 3 "" H 4000 4150 50  0000 C CNN
	1    4000 4150
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CONN_01X03-alimjuliefinal-rescue-alimjuliefinal-rescue P101
U 1 1 59944F9C
P 9450 2300
F 0 "P101" H 9450 2500 50  0000 C CNN
F 1 "vumetre" V 9550 2300 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 9450 2300 50  0001 C CNN
F 3 "" H 9450 2300 50  0000 C CNN
	1    9450 2300
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:LM337T-alimjuliefinal-rescue-alimjuliefinal-rescue U101
U 1 1 59945D89
P 7150 4850
F 0 "U101" H 6950 4650 50  0000 C CNN
F 1 "LM337T" H 7150 4650 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 7150 4750 50  0000 C CIN
F 3 "" H 7150 4850 50  0000 C CNN
	1    7150 4850
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR07
U 1 1 5968BD88
P 5600 4000
F 0 "#PWR07" H 5600 3750 50  0001 C CNN
F 1 "GND" H 5600 3850 50  0000 C CNN
F 2 "" H 5600 4000 50  0000 C CNN
F 3 "" H 5600 4000 50  0000 C CNN
	1    5600 4000
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:TRANSFO2-alimjuliefinal-rescue-alimjuliefinal-rescue T101
U 1 1 5A915F80
P 1700 4000
F 0 "T101" H 1700 4500 50  0000 C CNN
F 1 "TRANSFO2" H 1700 3500 50  0000 C CNN
F 2 "Transfo2x15VA:Trafo_CHK-EI54-16VA_2xSec" H 1700 4000 50  0001 C CNN
F 3 "" H 1700 4000 50  0000 C CNN
	1    1700 4000
	1    0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:GNDPWR-alimjuliefinal-rescue-alimjuliefinal-rescue #PWR08
U 1 1 5A917037
P 350 4000
F 0 "#PWR08" H 350 3800 50  0001 C CNN
F 1 "GNDPWR" H 350 3870 50  0000 C CNN
F 2 "" H 350 3950 50  0000 C CNN
F 3 "" H 350 3950 50  0000 C CNN
	1    350  4000
	1    0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:GNDPWR-alimjuliefinal-rescue-alimjuliefinal-rescue #PWR09
U 1 1 5A9171B9
P 2450 5500
F 0 "#PWR09" H 2450 5300 50  0001 C CNN
F 1 "GNDPWR" H 2450 5370 50  0000 C CNN
F 2 "" H 2450 5450 50  0000 C CNN
F 3 "" H 2450 5450 50  0000 C CNN
	1    2450 5500
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR010
U 1 1 5A91724E
P 2900 5500
F 0 "#PWR010" H 2900 5250 50  0001 C CNN
F 1 "GND" H 2900 5350 50  0000 C CNN
F 2 "" H 2900 5500 50  0000 C CNN
F 3 "" H 2900 5500 50  0000 C CNN
	1    2900 5500
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C104
U 1 1 5A9172E3
P 2700 5350
F 0 "C104" H 2725 5450 50  0000 L CNN
F 1 "100n" H 2725 5250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 2738 5200 50  0001 C CNN
F 3 "" H 2700 5350 50  0000 C CNN
	1    2700 5350
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R101
U 1 1 5A917472
P 2700 5150
F 0 "R101" V 2780 5150 50  0000 C CNN
F 1 "3R9" V 2700 5150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2630 5150 50  0001 C CNN
F 3 "" H 2700 5150 50  0000 C CNN
	1    2700 5150
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C103
U 1 1 5A91AA3C
P 2400 3800
F 0 "C103" H 2425 3900 50  0000 L CNN
F 1 "100n" H 2425 3700 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 2438 3650 50  0001 C CNN
F 3 "" H 2400 3800 50  0000 C CNN
	1    2400 3800
	-1   0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C101
U 1 1 5A91AAF7
P 2400 4200
F 0 "C101" H 2425 4300 50  0000 L CNN
F 1 "100n" H 2425 4100 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 2438 4050 50  0001 C CNN
F 3 "" H 2400 4200 50  0000 C CNN
	1    2400 4200
	-1   0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:POT-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue RV101
U 1 1 5A98F362
P 7100 3700
F 0 "RV101" H 7100 3600 50  0000 C CNN
F 1 "10k" H 7100 3700 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296W" H 7100 3700 50  0001 C CNN
F 3 "" H 7100 3700 50  0000 C CNN
	1    7100 3700
	0    1    1    0   
$EndComp
Connection ~ 8950 3100
Wire Wire Line
	9250 2900 9150 2900
Wire Wire Line
	9250 2700 8950 2700
Wire Wire Line
	9250 2800 9050 2800
Connection ~ 9150 3300
Wire Wire Line
	9150 3300 9250 3300
Wire Wire Line
	9150 3700 9250 3700
Wire Wire Line
	9150 2400 9150 2900
Wire Wire Line
	8950 3100 9250 3100
Connection ~ 9050 3200
Wire Wire Line
	9050 3200 9250 3200
Connection ~ 8950 3500
Wire Wire Line
	8950 3500 9250 3500
Wire Wire Line
	9050 3600 9250 3600
Connection ~ 5600 3600
Connection ~ 3100 4000
Wire Wire Line
	3100 4000 3100 4300
Connection ~ 3800 4200
Wire Wire Line
	3800 4300 3800 4200
Connection ~ 3600 3800
Wire Wire Line
	3600 4300 3600 3800
Wire Wire Line
	2250 4000 2400 4000
Connection ~ 8200 4900
Connection ~ 8200 3050
Connection ~ 6050 4400
Connection ~ 6200 4950
Wire Wire Line
	6050 4950 6200 4950
Wire Wire Line
	6050 4400 6050 4950
Connection ~ 6050 3600
Connection ~ 6100 3050
Wire Wire Line
	6050 3600 6050 3050
Wire Wire Line
	6200 4900 6200 4950
Wire Wire Line
	6200 5500 7000 5500
Wire Wire Line
	7650 5500 7300 5500
Connection ~ 7650 4900
Wire Wire Line
	7550 4900 7650 4900
Connection ~ 7900 4900
Wire Wire Line
	7900 4900 7900 4850
Wire Wire Line
	8200 4900 8200 4700
Wire Wire Line
	7900 3400 7900 3550
Wire Wire Line
	7900 4400 7900 4550
Connection ~ 7650 4600
Connection ~ 6700 4000
Wire Wire Line
	6700 3850 6700 4000
Wire Wire Line
	6900 4600 6700 4400
Connection ~ 7150 4600
Wire Wire Line
	6900 4600 7100 4600
Connection ~ 7650 3400
Connection ~ 7150 3400
Wire Wire Line
	6900 3400 6700 3550
Wire Wire Line
	6900 3400 7100 3400
Wire Wire Line
	7150 3350 7150 3400
Wire Wire Line
	7650 4600 7650 4400
Connection ~ 7650 4000
Wire Wire Line
	7650 3850 7650 4000
Wire Wire Line
	7650 3400 7650 3550
Connection ~ 8200 4000
Wire Wire Line
	8200 3500 8200 4000
Connection ~ 7650 3050
Connection ~ 7900 3050
Wire Wire Line
	7900 3050 7900 3100
Wire Wire Line
	8200 3050 8200 3200
Wire Wire Line
	7550 3050 7650 3050
Wire Wire Line
	6050 3050 6100 3050
Wire Wire Line
	6950 2400 6100 2400
Wire Wire Line
	7250 2400 7650 2400
Connection ~ 5600 4400
Connection ~ 5600 4000
Wire Wire Line
	5600 3900 5600 4000
Connection ~ 6100 4000
Connection ~ 5850 4000
Wire Wire Line
	5600 4000 5850 4000
Wire Wire Line
	5850 3900 5850 4000
Wire Wire Line
	6100 3900 6100 4000
Connection ~ 5850 4400
Connection ~ 5850 3600
Wire Wire Line
	4250 3600 4450 3600
Wire Wire Line
	2650 3800 3600 3800
Wire Wire Line
	3650 3800 3650 3500
Wire Wire Line
	3650 3500 4650 3500
Wire Wire Line
	4650 3500 4650 4000
Wire Wire Line
	3850 4200 3850 4150
Wire Wire Line
	2500 4200 3800 4200
Wire Wire Line
	3850 3800 3900 3800
Wire Wire Line
	4250 3600 4200 3800
Wire Wire Line
	4450 3700 4450 3600
Connection ~ 4450 3600
Wire Wire Line
	4650 4000 4450 4000
Wire Wire Line
	4650 4200 4450 4200
Wire Wire Line
	4450 4200 4450 4150
Connection ~ 4650 4000
Wire Wire Line
	4150 4400 4150 4150
Connection ~ 3850 4150
Wire Wire Line
	9150 2400 9250 2400
Connection ~ 9150 2900
Wire Wire Line
	9050 2300 9250 2300
Wire Wire Line
	8950 2200 9250 2200
Connection ~ 8950 2700
Wire Wire Line
	550  4100 550  3800
Wire Wire Line
	550  4100 350  4100
Wire Wire Line
	350  4200 1150 4200
Wire Wire Line
	2250 4100 2100 4100
Wire Wire Line
	2250 3900 2250 4000
Wire Wire Line
	2250 3900 2100 3900
Connection ~ 2250 4000
Wire Wire Line
	2500 4400 2500 4200
Wire Wire Line
	2100 4400 2400 4400
Wire Wire Line
	2650 3600 2650 3800
Wire Wire Line
	2100 3600 2400 3600
Wire Wire Line
	2450 5150 2450 5350
Wire Wire Line
	2450 5150 2550 5150
Wire Wire Line
	2550 5350 2450 5350
Connection ~ 2450 5350
Wire Wire Line
	2900 5150 2900 5350
Wire Wire Line
	2900 5150 2850 5150
Wire Wire Line
	2850 5350 2900 5350
Connection ~ 2900 5350
Wire Wire Line
	1150 4150 1150 4200
Connection ~ 1150 4200
Wire Wire Line
	2400 4350 2400 4400
Connection ~ 2400 4400
Wire Wire Line
	2400 3950 2400 4000
Connection ~ 2400 4000
Wire Wire Line
	2400 3650 2400 3600
Connection ~ 2400 3600
Wire Wire Line
	7100 3450 7100 3400
Connection ~ 7100 3400
$Comp
L alimjuliefinal-rescue:POT-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue RV102
U 1 1 5A997A20
P 7100 4300
F 0 "RV102" H 7100 4200 50  0000 C CNN
F 1 "10k" H 7100 4300 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296W" H 7100 4300 50  0001 C CNN
F 3 "" H 7100 4300 50  0000 C CNN
	1    7100 4300
	0    1    -1   0   
$EndComp
Wire Wire Line
	7100 3950 7100 4000
Connection ~ 7100 4000
Wire Wire Line
	7650 3550 7900 3550
Wire Wire Line
	7650 4400 7900 4400
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C105
U 1 1 5A9990E5
P 7650 3700
F 0 "C105" H 7675 3800 50  0000 L CNN
F 1 "33u" H 7675 3600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 7688 3550 50  0001 C CNN
F 3 "" H 7650 3700 50  0000 C CNN
	1    7650 3700
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C106
U 1 1 5A99918A
P 7650 4250
F 0 "C106" H 7675 4350 50  0000 L CNN
F 1 "33u" H 7675 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 7688 4100 50  0001 C CNN
F 3 "" H 7650 4250 50  0000 C CNN
	1    7650 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4550 7100 4600
Connection ~ 7100 4600
Wire Wire Line
	7250 4300 7250 4600
Connection ~ 7250 4600
$Comp
L alimjuliefinal-rescue:LED-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue D105
U 1 1 5A99B088
P 8500 3300
F 0 "D105" H 8500 3400 50  0000 C CNN
F 1 "LED" H 8500 3200 50  0000 C CNN
F 2 "LEDs:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop" H 8500 3300 50  0001 C CNN
F 3 "" H 8500 3300 50  0000 C CNN
	1    8500 3300
	0    -1   -1   0   
$EndComp
$Comp
L alimjuliefinal-rescue:LED-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue D106
U 1 1 5A99B375
P 8500 4650
F 0 "D106" H 8500 4750 50  0000 C CNN
F 1 "LED" H 8500 4550 50  0000 C CNN
F 2 "LEDs:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop" H 8500 4650 50  0001 C CNN
F 3 "" H 8500 4650 50  0000 C CNN
	1    8500 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8500 4850 8500 4900
Connection ~ 8500 4900
Wire Wire Line
	8500 3100 8500 3050
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R102
U 1 1 5A99B71D
P 8500 3650
F 0 "R102" V 8400 3550 50  0000 C CNN
F 1 "1k5" V 8500 3650 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8430 3650 50  0001 C CNN
F 3 "" H 8500 3650 50  0000 C CNN
	1    8500 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	8500 3800 8500 4000
Connection ~ 8500 4000
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R103
U 1 1 5A99B97D
P 8500 4300
F 0 "R103" V 8400 4200 50  0000 C CNN
F 1 "1k5" V 8500 4300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8430 4300 50  0001 C CNN
F 3 "" H 8500 4300 50  0000 C CNN
	1    8500 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 3700 7250 3400
Connection ~ 7250 3400
$Comp
L alimjuliefinal-rescue:FUSE-alimjuliefinal-rescue-alimjuliefinal-rescue F101
U 1 1 5A99DA4D
P 800 3800
F 0 "F101" H 900 3850 50  0000 C CNN
F 1 "FUSE" H 700 3750 50  0000 C CNN
F 2 "Fuse_Holders_and_Fuses:Fuseholder5x20_horiz_open_inline_Type-I" H 800 3800 50  0001 C CNN
F 3 "" H 800 3800 50  0000 C CNN
	1    800  3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 3100 8950 2700
Wire Wire Line
	9150 3300 9150 3700
Wire Wire Line
	9050 3200 9050 3600
Wire Wire Line
	8950 3500 8950 3100
Wire Wire Line
	5600 3600 5850 3600
Wire Wire Line
	3100 4000 3200 4000
Wire Wire Line
	3800 4200 3850 4200
Wire Wire Line
	3600 3800 3650 3800
Wire Wire Line
	8200 4900 8500 4900
Wire Wire Line
	8200 3050 8500 3050
Wire Wire Line
	6050 4400 6100 4400
Wire Wire Line
	6050 3600 6100 3600
Wire Wire Line
	7650 4900 7900 4900
Wire Wire Line
	7900 4900 8200 4900
Wire Wire Line
	6700 4000 6700 4100
Wire Wire Line
	6700 4000 7100 4000
Wire Wire Line
	7150 4600 7250 4600
Wire Wire Line
	7150 3400 7250 3400
Wire Wire Line
	7650 4000 7650 4100
Wire Wire Line
	8200 4000 8200 4400
Wire Wire Line
	8200 4000 8500 4000
Wire Wire Line
	7650 3050 7900 3050
Wire Wire Line
	7650 3050 7650 3100
Wire Wire Line
	7900 3050 8200 3050
Wire Wire Line
	5600 4400 5850 4400
Wire Wire Line
	5600 4000 5600 4100
Wire Wire Line
	6100 4000 6700 4000
Wire Wire Line
	6100 4000 6100 4100
Wire Wire Line
	5850 4000 6100 4000
Wire Wire Line
	5850 4000 5850 4100
Wire Wire Line
	5850 4400 6050 4400
Wire Wire Line
	5850 3600 6050 3600
Wire Wire Line
	4650 4000 4650 4200
Wire Wire Line
	3850 4150 3850 3800
Wire Wire Line
	8950 2700 8950 2200
Wire Wire Line
	2250 4000 2250 4100
Wire Wire Line
	2450 5350 2450 5500
Wire Wire Line
	2900 5350 2900 5500
Wire Wire Line
	1150 4200 1300 4200
Wire Wire Line
	2400 4400 2500 4400
Wire Wire Line
	2400 4000 3100 4000
Wire Wire Line
	2400 4000 2400 4050
Wire Wire Line
	2400 3600 2650 3600
Wire Wire Line
	7100 3400 7150 3400
Wire Wire Line
	7100 4000 7100 4050
Wire Wire Line
	7100 4600 7150 4600
Wire Wire Line
	7250 4600 7650 4600
Wire Wire Line
	8500 4900 8950 4900
Wire Wire Line
	8500 4000 8500 4150
Wire Wire Line
	8500 4000 8650 4000
Wire Wire Line
	6200 4900 6750 4900
Wire Wire Line
	7650 4900 7650 5500
Wire Wire Line
	7650 2400 7650 3050
Wire Wire Line
	6100 2400 6100 3050
Wire Wire Line
	1050 3800 1300 3800
Wire Wire Line
	6200 4950 6200 5500
Wire Wire Line
	6100 3050 6750 3050
Wire Wire Line
	7250 3400 7650 3400
Wire Wire Line
	7650 4000 8200 4000
Wire Wire Line
	4150 4400 5600 4400
Wire Wire Line
	4450 3600 5600 3600
Wire Wire Line
	7100 4000 7650 4000
Wire Wire Line
	8950 3500 8950 4900
Wire Wire Line
	9050 2300 9050 2800
Connection ~ 9050 2800
Text Label 8500 3050 0    50   ~ 0
+16
Text Label 8500 4900 0    50   ~ 0
-16
Wire Wire Line
	9150 2900 9150 3300
Wire Wire Line
	9050 6150 9050 3700
Wire Wire Line
	9050 3700 9150 3700
Connection ~ 9150 3700
Wire Wire Line
	8500 3050 9050 3050
Wire Wire Line
	9050 2800 9050 3050
Connection ~ 8500 3050
Connection ~ 9050 3050
Wire Wire Line
	9050 3050 9050 3200
$EndSCHEMATC
