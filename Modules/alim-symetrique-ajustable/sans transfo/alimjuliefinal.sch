EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR0101
U 1 1 5968B288
P 1900 3850
F 0 "#PWR0101" H 1900 3600 50  0001 C CNN
F 1 "GND" H 1950 3700 50  0000 C CNN
F 2 "" H 1900 3850 50  0000 C CNN
F 3 "" H 1900 3850 50  0000 C CNN
	1    1900 3850
	0    -1   -1   0   
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C10
U 1 1 5968B788
P 6100 3750
F 0 "C10" H 6125 3850 50  0000 L CNN
F 1 "100n" H 6125 3650 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 6138 3600 50  0001 C CNN
F 3 "" H 6100 3750 50  0000 C CNN
	1    6100 3750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C11
U 1 1 5968B907
P 6100 4250
F 0 "C11" H 6125 4350 50  0000 L CNN
F 1 "100n" H 6125 4150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 6138 4100 50  0001 C CNN
F 3 "" H 6100 4250 50  0000 C CNN
	1    6100 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C8
U 1 1 5968B9F3
P 5850 3750
F 0 "C8" H 5875 3850 50  0000 L CNN
F 1 "2200" H 5875 3650 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P7.50mm-pad2mm" H 5888 3600 50  0001 C CNN
F 3 "" H 5850 3750 50  0000 C CNN
	1    5850 3750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C9
U 1 1 5968BA2B
P 5850 4250
F 0 "C9" H 5875 4350 50  0000 L CNN
F 1 "2200" H 5875 4150 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P7.50mm-pad2mm" H 5888 4100 50  0001 C CNN
F 3 "" H 5850 4250 50  0000 C CNN
	1    5850 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C6
U 1 1 5968BF10
P 5600 3750
F 0 "C6" H 5625 3850 50  0000 L CNN
F 1 "2200" H 5625 3650 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P7.50mm-pad2mm" H 5638 3600 50  0001 C CNN
F 3 "" H 5600 3750 50  0000 C CNN
	1    5600 3750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C7
U 1 1 5968BF54
P 5600 4250
F 0 "C7" H 5625 4350 50  0000 L CNN
F 1 "2200" H 5625 4150 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P7.50mm-pad2mm" H 5638 4100 50  0001 C CNN
F 3 "" H 5600 4250 50  0000 C CNN
	1    5600 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C13
U 1 1 5968C679
P 6700 4250
F 0 "C13" H 6725 4350 50  0000 L CNN
F 1 "100n" H 6725 4150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 6738 4100 50  0001 C CNN
F 3 "" H 6700 4250 50  0000 C CNN
	1    6700 4250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R8
U 1 1 5968CAA3
P 7650 4750
F 0 "R8" V 7730 4750 50  0000 C CNN
F 1 "220" V 7650 4750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7580 4750 50  0001 C CNN
F 3 "" H 7650 4750 50  0000 C CNN
	1    7650 4750
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D5
U 1 1 5968CBD7
P 7900 4700
F 0 "D5" H 7900 4800 50  0000 C CNN
F 1 "1N4007" H 7900 4600 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 7900 4700 50  0001 C CNN
F 3 "" H 7900 4700 50  0000 C CNN
	1    7900 4700
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C17
U 1 1 5968CDB8
P 8200 4550
F 0 "C17" H 8225 4650 50  0000 L CNN
F 1 "100u" H 8225 4450 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 8238 4400 50  0001 C CNN
F 3 "" H 8200 4550 50  0000 C CNN
	1    8200 4550
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C12
U 1 1 5968D436
P 6700 3700
F 0 "C12" H 6725 3800 50  0000 L CNN
F 1 "100n" H 6725 3600 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 6738 3550 50  0001 C CNN
F 3 "" H 6700 3700 50  0000 C CNN
	1    6700 3700
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R7
U 1 1 5968D556
P 7650 3250
F 0 "R7" V 7730 3250 50  0000 C CNN
F 1 "220" V 7650 3250 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7580 3250 50  0001 C CNN
F 3 "" H 7650 3250 50  0000 C CNN
	1    7650 3250
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D4
U 1 1 5968D55C
P 7900 3250
F 0 "D4" H 7900 3350 50  0000 C CNN
F 1 "1N4007" H 7900 3150 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 7900 3250 50  0001 C CNN
F 3 "" H 7900 3250 50  0000 C CNN
	1    7900 3250
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C16
U 1 1 5968D562
P 8200 3350
F 0 "C16" H 8225 3450 50  0000 L CNN
F 1 "100u" H 8225 3250 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 8238 3200 50  0001 C CNN
F 3 "" H 8200 3350 50  0000 C CNN
	1    8200 3350
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D3
U 1 1 5968DD68
P 7150 5500
F 0 "D3" H 7150 5600 50  0000 C CNN
F 1 "1N4007" H 7150 5400 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 7150 5500 50  0001 C CNN
F 3 "" H 7150 5500 50  0000 C CNN
	1    7150 5500
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D2
U 1 1 5968DEBC
P 7100 2400
F 0 "D2" H 7100 2500 50  0000 C CNN
F 1 "1N4007" H 7100 2300 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 7100 2400 50  0001 C CNN
F 3 "" H 7100 2400 50  0000 C CNN
	1    7100 2400
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR0102
U 1 1 59690C0D
P 8650 4000
F 0 "#PWR0102" H 8650 3750 50  0001 C CNN
F 1 "GND" H 8650 3850 50  0000 C CNN
F 2 "" H 8650 4000 50  0000 C CNN
F 3 "" H 8650 4000 50  0000 C CNN
	1    8650 4000
	0    -1   -1   0   
$EndComp
Text Notes 8650 3050 0    60   ~ 0
+16\n
Text Notes 8650 4900 0    60   ~ 0
-16\n
$Comp
L alimjuliefinal-rescue:PWR_FLAG-power-alimjuliefinal-rescue #FLG0101
U 1 1 59904790
P 6350 4000
F 0 "#FLG0101" H 6350 4095 50  0001 C CNN
F 1 "PWR_FLAG" H 6350 4180 50  0000 C CNN
F 2 "" H 6350 4000 50  0000 C CNN
F 3 "" H 6350 4000 50  0000 C CNN
	1    6350 4000
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:PWR_FLAG-power-alimjuliefinal-rescue #FLG0102
U 1 1 599049D9
P 3600 4300
F 0 "#FLG0102" H 3600 4395 50  0001 C CNN
F 1 "PWR_FLAG" H 3600 4480 50  0000 C CNN
F 2 "" H 3600 4300 50  0000 C CNN
F 3 "" H 3600 4300 50  0000 C CNN
	1    3600 4300
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:PWR_FLAG-power-alimjuliefinal-rescue #FLG0103
U 1 1 59904A5F
P 3800 4300
F 0 "#FLG0103" H 3800 4395 50  0001 C CNN
F 1 "PWR_FLAG" H 3800 4480 50  0000 C CNN
F 2 "" H 3800 4300 50  0000 C CNN
F 3 "" H 3800 4300 50  0000 C CNN
	1    3800 4300
	-1   0    0    1   
$EndComp
Text Notes 10100 1850 0    60   ~ 0
out \n1: -16v\n2:+16v\n3:GND
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR0103
U 1 1 59908D8C
P 9150 6150
F 0 "#PWR0103" H 9150 5900 50  0001 C CNN
F 1 "GND" H 9150 6000 50  0000 C CNN
F 2 "" H 9150 6150 50  0000 C CNN
F 3 "" H 9150 6150 50  0000 C CNN
	1    9150 6150
	1    0    0    -1  
$EndComp
Text Notes 4800 1900 0    60   ~ 0
BD245 remplacé par TIP35\nBD246 remplacé par TIP36
$Comp
L alimjuliefinal-rescue:LM317T-alimjuliefinal-rescue-alimjuliefinal-rescue U1
U 1 1 59910CF1
P 7150 3100
F 0 "U1" H 6950 3300 50  0000 C CNN
F 1 "LM317T" H 7150 3300 50  0000 L CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 7150 3200 50  0001 C CIN
F 3 "" H 7150 3100 50  0000 C CNN
	1    7150 3100
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D102
U 1 1 59943EA8
P 4050 3800
F 0 "D102" H 4050 3900 50  0000 C CNN
F 1 "1N4936" H 4050 3700 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 4050 3800 50  0001 C CNN
F 3 "" H 4050 3800 50  0000 C CNN
	1    4050 3800
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D104
U 1 1 59944125
P 4450 3850
F 0 "D104" H 4450 3950 50  0000 C CNN
F 1 "1N4936" H 4450 3750 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 4450 3850 50  0001 C CNN
F 3 "" H 4450 3850 50  0000 C CNN
	1    4450 3850
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D103
U 1 1 5994446E
P 4300 4150
F 0 "D103" H 4300 4250 50  0000 C CNN
F 1 "1N4936" H 4300 4050 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 4300 4150 50  0001 C CNN
F 3 "" H 4300 4150 50  0000 C CNN
	1    4300 4150
	-1   0    0    1   
$EndComp
$Comp
L alimjuliefinal-rescue:D-Device-alimjuliefinal-rescue D101
U 1 1 599446C5
P 4000 4150
F 0 "D101" H 4000 4250 50  0000 C CNN
F 1 "1N4936" H 4000 4050 50  0000 C CNN
F 2 "resistance-grospad:D_DO-35_SOD27_P10.16mm_Horizontal-pad2mm" H 4000 4150 50  0001 C CNN
F 3 "" H 4000 4150 50  0000 C CNN
	1    4000 4150
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:LM337T-alimjuliefinal-rescue-alimjuliefinal-rescue U101
U 1 1 59945D89
P 7150 4850
F 0 "U101" H 6950 4650 50  0000 C CNN
F 1 "LM337T" H 7150 4650 50  0000 L CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 7150 4750 50  0001 C CIN
F 3 "" H 7150 4850 50  0000 C CNN
	1    7150 4850
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR0104
U 1 1 5968BD88
P 5600 4000
F 0 "#PWR0104" H 5600 3750 50  0001 C CNN
F 1 "GND" H 5600 3850 50  0000 C CNN
F 2 "" H 5600 4000 50  0000 C CNN
F 3 "" H 5600 4000 50  0000 C CNN
	1    5600 4000
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:GNDPWR-alimjuliefinal-rescue-alimjuliefinal-rescue #PWR0105
U 1 1 5A9171B9
P 2450 5500
F 0 "#PWR0105" H 2450 5300 50  0001 C CNN
F 1 "GNDPWR" H 2450 5370 50  0000 C CNN
F 2 "" H 2450 5450 50  0000 C CNN
F 3 "" H 2450 5450 50  0000 C CNN
	1    2450 5500
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:GND-power-alimjuliefinal-rescue #PWR0106
U 1 1 5A91724E
P 2900 5500
F 0 "#PWR0106" H 2900 5250 50  0001 C CNN
F 1 "GND" H 2900 5350 50  0000 C CNN
F 2 "" H 2900 5500 50  0000 C CNN
F 3 "" H 2900 5500 50  0000 C CNN
	1    2900 5500
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:C-Device-alimjuliefinal-rescue C104
U 1 1 5A9172E3
P 2700 5350
F 0 "C104" H 2725 5450 50  0000 L CNN
F 1 "100n" H 2725 5250 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 2738 5200 50  0001 C CNN
F 3 "" H 2700 5350 50  0000 C CNN
	1    2700 5350
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R101
U 1 1 5A917472
P 2700 5150
F 0 "R101" V 2780 5150 50  0000 C CNN
F 1 "3R9" V 2700 5150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 2630 5150 50  0001 C CNN
F 3 "" H 2700 5150 50  0000 C CNN
	1    2700 5150
	0    1    1    0   
$EndComp
$Comp
L alimjuliefinal-rescue:POT-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue RV101
U 1 1 5A98F362
P 7100 3650
F 0 "RV101" H 7100 3550 50  0000 C CNN
F 1 "10k" H 7100 3650 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 7100 3650 50  0001 C CNN
F 3 "" H 7100 3650 50  0000 C CNN
	1    7100 3650
	0    1    1    0   
$EndComp
Connection ~ 5600 3600
Wire Wire Line
	3800 4300 3800 4200
Wire Wire Line
	3600 4300 3600 3800
Connection ~ 8200 4900
Connection ~ 8200 3050
Connection ~ 6050 4400
Connection ~ 6200 4950
Wire Wire Line
	6050 4950 6200 4950
Wire Wire Line
	6050 4400 6050 4950
Connection ~ 6050 3600
Connection ~ 6100 3050
Wire Wire Line
	6050 3600 6050 3050
Wire Wire Line
	6200 4900 6200 4950
Wire Wire Line
	6200 5500 7000 5500
Wire Wire Line
	7650 5500 7300 5500
Connection ~ 7650 4900
Wire Wire Line
	7550 4900 7650 4900
Connection ~ 7900 4900
Wire Wire Line
	7900 4900 7900 4850
Wire Wire Line
	8200 4900 8200 4700
Wire Wire Line
	7900 3400 7900 3550
Wire Wire Line
	7900 4400 7900 4550
Connection ~ 7650 4600
Connection ~ 6700 4000
Wire Wire Line
	6700 3850 6700 4000
Wire Wire Line
	6900 4600 6700 4400
Connection ~ 7150 4600
Wire Wire Line
	6900 4600 7100 4600
Connection ~ 7650 3400
Connection ~ 7150 3400
Wire Wire Line
	6900 3400 6700 3550
Wire Wire Line
	6900 3400 7100 3400
Wire Wire Line
	7150 3350 7150 3400
Wire Wire Line
	7650 4600 7650 4400
Connection ~ 7650 4000
Wire Wire Line
	7650 3850 7650 4000
Wire Wire Line
	7650 3400 7650 3550
Connection ~ 8200 4000
Wire Wire Line
	8200 3500 8200 4000
Connection ~ 7650 3050
Connection ~ 7900 3050
Wire Wire Line
	7900 3050 7900 3100
Wire Wire Line
	8200 3050 8200 3200
Wire Wire Line
	7550 3050 7650 3050
Wire Wire Line
	6050 3050 6100 3050
Wire Wire Line
	6950 2400 6100 2400
Wire Wire Line
	7250 2400 7650 2400
Connection ~ 5600 4400
Connection ~ 5600 4000
Wire Wire Line
	5600 3900 5600 4000
Connection ~ 6100 4000
Connection ~ 5850 4000
Wire Wire Line
	5600 4000 5850 4000
Wire Wire Line
	5850 3900 5850 4000
Wire Wire Line
	6100 3900 6100 4000
Connection ~ 5850 4400
Connection ~ 5850 3600
Wire Wire Line
	4250 3600 4450 3600
Wire Wire Line
	3650 3800 3650 3500
Wire Wire Line
	3650 3500 4650 3500
Wire Wire Line
	4650 3500 4650 4000
Wire Wire Line
	3850 4200 3850 4150
Wire Wire Line
	3850 3800 3900 3800
Wire Wire Line
	4250 3600 4200 3800
Wire Wire Line
	4450 3700 4450 3600
Connection ~ 4450 3600
Wire Wire Line
	4650 4000 4450 4000
Wire Wire Line
	4650 4200 4450 4200
Wire Wire Line
	4450 4200 4450 4150
Connection ~ 4650 4000
Wire Wire Line
	4150 4400 4150 4150
Connection ~ 3850 4150
Wire Wire Line
	2450 5150 2450 5350
Wire Wire Line
	2450 5150 2550 5150
Wire Wire Line
	2550 5350 2450 5350
Connection ~ 2450 5350
Wire Wire Line
	2900 5150 2900 5350
Wire Wire Line
	2900 5150 2850 5150
Wire Wire Line
	2850 5350 2900 5350
Connection ~ 2900 5350
Connection ~ 7100 3400
$Comp
L alimjuliefinal-rescue:POT-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue RV102
U 1 1 5A997A20
P 7100 4350
F 0 "RV102" H 7100 4250 50  0000 C CNN
F 1 "10k" H 7100 4350 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 7100 4350 50  0001 C CNN
F 3 "" H 7100 4350 50  0000 C CNN
	1    7100 4350
	0    1    -1   0   
$EndComp
Wire Wire Line
	7650 3550 7900 3550
Wire Wire Line
	7650 4400 7900 4400
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C105
U 1 1 5A9990E5
P 7650 3700
F 0 "C105" H 7675 3800 50  0000 L CNN
F 1 "33u" H 7675 3600 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 7688 3550 50  0001 C CNN
F 3 "" H 7650 3700 50  0000 C CNN
	1    7650 3700
	1    0    0    -1  
$EndComp
$Comp
L alimjuliefinal-rescue:CP-Device-alimjuliefinal-rescue C106
U 1 1 5A99918A
P 7650 4250
F 0 "C106" H 7675 4350 50  0000 L CNN
F 1 "33u" H 7675 4150 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 7688 4100 50  0001 C CNN
F 3 "" H 7650 4250 50  0000 C CNN
	1    7650 4250
	1    0    0    -1  
$EndComp
Connection ~ 7100 4600
Wire Wire Line
	7250 4350 7250 4600
Connection ~ 7250 4600
$Comp
L alimjuliefinal-rescue:LED-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue D105
U 1 1 5A99B088
P 8500 3300
F 0 "D105" H 8500 3400 50  0000 C CNN
F 1 "LED" H 8500 3200 50  0000 C CNN
F 2 "resistance-grospad:diode" H 8500 3300 50  0001 C CNN
F 3 "" H 8500 3300 50  0000 C CNN
	1    8500 3300
	0    -1   -1   0   
$EndComp
$Comp
L alimjuliefinal-rescue:LED-RESCUE-alimjuliefinal-alimjuliefinal-rescue-alimjuliefinal-rescue D106
U 1 1 5A99B375
P 8500 4650
F 0 "D106" H 8500 4750 50  0000 C CNN
F 1 "LED" H 8500 4550 50  0000 C CNN
F 2 "resistance-grospad:diode" H 8500 4650 50  0001 C CNN
F 3 "" H 8500 4650 50  0000 C CNN
	1    8500 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8500 4850 8500 4900
Connection ~ 8500 4900
Wire Wire Line
	8500 3100 8500 3050
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R102
U 1 1 5A99B71D
P 8500 3650
F 0 "R102" V 8400 3550 50  0000 C CNN
F 1 "2k2" V 8500 3650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 8430 3650 50  0001 C CNN
F 3 "" H 8500 3650 50  0000 C CNN
	1    8500 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	8500 3800 8500 4000
Connection ~ 8500 4000
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R103
U 1 1 5A99B97D
P 8500 4300
F 0 "R103" V 8400 4200 50  0000 C CNN
F 1 "2k2" V 8500 4300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 8430 4300 50  0001 C CNN
F 3 "" H 8500 4300 50  0000 C CNN
	1    8500 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 3650 7250 3400
Connection ~ 7250 3400
Wire Wire Line
	5600 3600 5850 3600
Wire Wire Line
	3800 4200 3850 4200
Wire Wire Line
	3600 3800 3650 3800
Wire Wire Line
	8200 4900 8500 4900
Wire Wire Line
	8200 3050 8500 3050
Wire Wire Line
	6050 4400 6100 4400
Wire Wire Line
	6050 3600 6100 3600
Wire Wire Line
	7650 4900 7900 4900
Wire Wire Line
	7900 4900 8200 4900
Wire Wire Line
	6700 4000 6700 4100
Wire Wire Line
	7150 4600 7250 4600
Wire Wire Line
	7150 3400 7250 3400
Wire Wire Line
	7650 4000 7650 4100
Wire Wire Line
	8200 4000 8200 4400
Wire Wire Line
	8200 4000 8500 4000
Wire Wire Line
	7650 3050 7900 3050
Wire Wire Line
	7650 3050 7650 3100
Wire Wire Line
	7900 3050 8200 3050
Wire Wire Line
	5600 4400 5850 4400
Wire Wire Line
	5600 4000 5600 4100
Wire Wire Line
	6100 4000 6350 4000
Wire Wire Line
	6100 4000 6100 4100
Wire Wire Line
	5850 4000 6100 4000
Wire Wire Line
	5850 4000 5850 4100
Wire Wire Line
	5850 4400 6050 4400
Wire Wire Line
	5850 3600 6050 3600
Wire Wire Line
	4650 4000 4650 4200
Wire Wire Line
	3850 4150 3850 3800
Wire Wire Line
	2450 5350 2450 5500
Wire Wire Line
	2900 5350 2900 5500
Wire Wire Line
	7100 3400 7150 3400
Wire Wire Line
	7100 4600 7150 4600
Wire Wire Line
	7250 4600 7650 4600
Wire Wire Line
	8500 4900 8950 4900
Wire Wire Line
	8500 4000 8500 4150
Wire Wire Line
	8500 4000 8650 4000
Wire Wire Line
	6200 4900 6750 4900
Wire Wire Line
	7650 4900 7650 5500
Wire Wire Line
	7650 2400 7650 3050
Wire Wire Line
	6100 2400 6100 3050
Wire Wire Line
	6200 4950 6200 5500
Wire Wire Line
	6100 3050 6750 3050
Wire Wire Line
	7250 3400 7650 3400
Wire Wire Line
	7650 4000 8200 4000
Wire Wire Line
	4150 4400 5600 4400
Wire Wire Line
	4450 3600 5600 3600
Text Label 8500 3050 0    50   ~ 0
+16
Text Label 8500 4900 0    50   ~ 0
-16
Wire Wire Line
	8500 3050 9050 3050
Connection ~ 8500 3050
Connection ~ 9050 3050
Connection ~ 3800 4200
Wire Wire Line
	2500 4200 3800 4200
Connection ~ 3600 3800
Connection ~ 4150 4150
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R1
U 1 1 5F1A2103
P 7300 3900
F 0 "R1" V 7380 3900 50  0000 C CNN
F 1 "220" V 7300 3900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7230 3900 50  0001 C CNN
F 3 "" H 7300 3900 50  0000 C CNN
	1    7300 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 3900 7450 4000
Connection ~ 7450 4000
Wire Wire Line
	7450 4000 7650 4000
$Comp
L alimjuliefinal-rescue:R-Device-alimjuliefinal-rescue R2
U 1 1 5F1CB5BE
P 7300 4100
F 0 "R2" V 7380 4100 50  0000 C CNN
F 1 "220" V 7300 4100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7230 4100 50  0001 C CNN
F 3 "" H 7300 4100 50  0000 C CNN
	1    7300 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 4000 7450 4100
Wire Wire Line
	6700 4000 7450 4000
Wire Wire Line
	7100 4100 7150 4100
$Comp
L Connector:Conn_01x07_Female P101
U 1 1 5F1FA263
P 10700 2450
F 0 "P101" H 10728 2476 50  0000 L CNN
F 1 "vumetre" H 10728 2385 50  0000 L CNN
F 2 "pinheader2:PinHeader_1x07_P2.54mm_Vertical" H 10700 2450 50  0001 C CNN
F 3 "~" H 10700 2450 50  0001 C CNN
	1    10700 2450
	1    0    0    1   
$EndComp
Wire Wire Line
	10500 2750 10350 2750
Wire Wire Line
	10350 2750 10350 2650
Wire Wire Line
	10350 2550 10500 2550
Wire Wire Line
	10500 2650 10350 2650
Connection ~ 10350 2650
Wire Wire Line
	10350 2650 10350 2550
Wire Wire Line
	10500 2350 10350 2350
Wire Wire Line
	10350 2350 10350 2400
Wire Wire Line
	10350 2450 10500 2450
Wire Wire Line
	10500 2150 10350 2150
Wire Wire Line
	10350 2250 10500 2250
Connection ~ 10350 2400
Wire Wire Line
	10350 2400 10350 2450
Wire Wire Line
	8950 2200 10350 2200
Wire Wire Line
	10350 2150 10350 2200
Wire Wire Line
	10350 2200 10350 2250
Connection ~ 10350 2200
$Comp
L Connector:Conn_01x07_Female P2
U 1 1 5F262517
P 10700 3250
F 0 "P2" H 10592 2725 50  0000 C CNN
F 1 "auxiliaire" H 10592 2816 50  0000 C CNN
F 2 "pinheader2:PinHeader_1x07_P2.54mm_Vertical" H 10700 3250 50  0001 C CNN
F 3 "~" H 10700 3250 50  0001 C CNN
	1    10700 3250
	1    0    0    1   
$EndComp
Wire Wire Line
	10500 3550 10350 3550
Wire Wire Line
	10350 3550 10350 3450
Wire Wire Line
	10350 3350 10500 3350
Wire Wire Line
	10500 3450 10350 3450
Connection ~ 10350 3450
Wire Wire Line
	10350 3450 10350 3350
Wire Wire Line
	10500 3150 10350 3150
Wire Wire Line
	10350 3150 10350 3200
Wire Wire Line
	10350 3250 10500 3250
Wire Wire Line
	10500 2950 10350 2950
Wire Wire Line
	10350 3050 10500 3050
Connection ~ 10350 3200
Wire Wire Line
	10350 3200 10350 3250
Wire Wire Line
	8950 3000 10350 3000
Wire Wire Line
	10350 2950 10350 3000
Wire Wire Line
	10350 3000 10350 3050
Connection ~ 10350 3000
Connection ~ 8950 3000
Wire Wire Line
	10500 4400 10350 4400
Wire Wire Line
	10350 4400 10350 4300
Wire Wire Line
	10350 4200 10500 4200
Wire Wire Line
	10500 4300 10350 4300
Connection ~ 10350 4300
Wire Wire Line
	10350 4300 10350 4200
Wire Wire Line
	10500 4000 10350 4000
Wire Wire Line
	10350 4000 10350 4050
Wire Wire Line
	10350 4100 10500 4100
Wire Wire Line
	10500 3800 10350 3800
Wire Wire Line
	10350 3900 10500 3900
Connection ~ 10350 4050
Wire Wire Line
	10350 4050 10350 4100
Wire Wire Line
	8950 3850 10350 3850
Wire Wire Line
	10350 3800 10350 3850
Wire Wire Line
	10350 3850 10350 3900
Connection ~ 10350 3850
$Comp
L Connector:Conn_01x07_Female P4
U 1 1 5F2853AF
P 10700 4900
F 0 "P4" H 10592 4375 50  0000 C CNN
F 1 "arriere" H 10592 4466 50  0000 C CNN
F 2 "pinheader2:PinHeader_1x07_P2.54mm_Vertical" H 10700 4900 50  0001 C CNN
F 3 "~" H 10700 4900 50  0001 C CNN
	1    10700 4900
	1    0    0    1   
$EndComp
Wire Wire Line
	10500 5200 10350 5200
Wire Wire Line
	10350 5200 10350 5100
Wire Wire Line
	10350 5000 10500 5000
Wire Wire Line
	10500 5100 10350 5100
Connection ~ 10350 5100
Wire Wire Line
	10350 5100 10350 5000
Wire Wire Line
	10500 4800 10350 4800
Wire Wire Line
	10350 4800 10350 4850
Wire Wire Line
	10350 4900 10500 4900
Wire Wire Line
	10500 4600 10350 4600
Wire Wire Line
	10350 4700 10500 4700
Connection ~ 10350 4850
Wire Wire Line
	10350 4850 10350 4900
Connection ~ 8950 3850
Connection ~ 9050 4050
Connection ~ 9150 4300
Wire Wire Line
	8950 2200 8950 3000
Wire Wire Line
	9050 2400 9050 3050
Wire Wire Line
	9150 2650 9150 3450
Wire Wire Line
	8950 3000 8950 3850
Wire Wire Line
	9050 3050 9050 3200
Wire Wire Line
	9150 4300 9150 5100
Wire Wire Line
	10350 4600 10350 4650
Wire Wire Line
	8950 3850 8950 4650
Wire Wire Line
	9050 4050 9050 4850
Wire Wire Line
	9050 3200 10350 3200
Connection ~ 9050 3200
Wire Wire Line
	9050 3200 9050 4050
Wire Wire Line
	9150 3450 10350 3450
Connection ~ 9150 3450
Wire Wire Line
	9150 3450 9150 4300
Wire Wire Line
	9150 2650 10350 2650
Wire Wire Line
	9050 2400 10350 2400
Wire Wire Line
	9150 4300 10350 4300
Wire Wire Line
	9050 4050 10350 4050
Wire Wire Line
	10350 4650 8950 4650
Connection ~ 10350 4650
Wire Wire Line
	10350 4650 10350 4700
Connection ~ 8950 4650
Wire Wire Line
	8950 4650 8950 4900
Wire Wire Line
	9050 4850 10350 4850
Wire Wire Line
	9150 5100 10350 5100
Connection ~ 9150 5100
Wire Wire Line
	9150 5100 9150 6000
$Comp
L Connector:Conn_01x07_Female P3
U 1 1 5F28538E
P 10700 4100
F 0 "P3" H 10592 3575 50  0000 C CNN
F 1 "filtre" H 10592 3666 50  0000 C CNN
F 2 "pinheader2:PinHeader_1x07_P2.54mm_Vertical" H 10700 4100 50  0001 C CNN
F 3 "~" H 10700 4100 50  0001 C CNN
	1    10700 4100
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x07_Female P5
U 1 1 5F3413C1
P 10700 5800
F 0 "P5" H 10592 5275 50  0000 C CNN
F 1 "arriere" H 10592 5366 50  0000 C CNN
F 2 "pinheader2:PinHeader_1x07_P2.54mm_Vertical" H 10700 5800 50  0001 C CNN
F 3 "~" H 10700 5800 50  0001 C CNN
	1    10700 5800
	1    0    0    1   
$EndComp
Wire Wire Line
	10500 6100 10350 6100
Wire Wire Line
	10350 6100 10350 6000
Wire Wire Line
	10350 5900 10500 5900
Wire Wire Line
	10500 6000 10350 6000
Connection ~ 10350 6000
Wire Wire Line
	10350 6000 10350 5900
Wire Wire Line
	10500 5700 10350 5700
Wire Wire Line
	10350 5700 10350 5750
Wire Wire Line
	10350 5800 10500 5800
Wire Wire Line
	10500 5500 10350 5500
Wire Wire Line
	10350 5600 10500 5600
Connection ~ 10350 5750
Wire Wire Line
	10350 5750 10350 5800
Wire Wire Line
	10350 5500 10350 5550
Wire Wire Line
	10350 5550 8950 5550
Connection ~ 10350 5550
Wire Wire Line
	10350 5550 10350 5600
Wire Wire Line
	9050 5750 10350 5750
Wire Wire Line
	9150 6000 10350 6000
Connection ~ 9150 6000
Wire Wire Line
	9150 6000 9150 6150
Wire Wire Line
	8950 4900 8950 5550
Connection ~ 8950 4900
Wire Wire Line
	9050 4850 9050 5750
Connection ~ 9050 4850
$Comp
L Connector:Conn_01x07_Female P1
U 1 1 5F3C5395
P 1650 4050
F 0 "P1" H 1542 4535 50  0000 C CNN
F 1 "transfo2x15v" H 1542 4444 50  0000 C CNN
F 2 "pinheader2:PinHeader_1x07_P2.54mm_Vertical" H 1650 4050 50  0001 C CNN
F 3 "~" H 1650 4050 50  0001 C CNN
	1    1650 4050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1850 3750 1900 3750
Wire Wire Line
	1900 3750 1900 3850
Wire Wire Line
	1900 3950 1850 3950
Wire Wire Line
	1850 3850 1900 3850
Connection ~ 1900 3850
Wire Wire Line
	1900 3850 1900 3950
Wire Wire Line
	2250 3850 2250 3800
Wire Wire Line
	2250 3800 3600 3800
Wire Wire Line
	1900 4050 1900 4150
Wire Wire Line
	1900 4150 1850 4150
Wire Wire Line
	1900 4050 1850 4050
Text Label 1900 4100 0    50   ~ 0
+15AC
Wire Wire Line
	1850 4250 1850 4350
Text Label 1850 4300 0    50   ~ 0
-15AC
Text Label 2400 3800 0    50   ~ 0
+15AC
Text Label 2550 4200 0    50   ~ 0
-15AC
Connection ~ 6350 4000
Wire Wire Line
	6350 4000 6700 4000
Wire Wire Line
	7150 3900 7100 3900
$EndSCHEMATC
