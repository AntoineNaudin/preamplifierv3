EESchema Schematic File Version 4
LIBS:highpass-lowpass-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L highpass-lowpass-rescue:C-Device-riaav3-rescue-highpass-lowpass-rescue C102
U 1 1 5CD4D676
P 3150 1950
F 0 "C102" V 2898 1950 50  0000 C CNN
F 1 "150n" V 2989 1950 50  0000 C CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 3188 1800 50  0001 C CNN
F 3 "" H 3150 1950 50  0001 C CNN
	1    3150 1950
	0    1    1    0   
$EndComp
$Comp
L highpass-lowpass-rescue:C-Device-riaav3-rescue-highpass-lowpass-rescue C104
U 1 1 5CD4D6C8
P 3700 1950
F 0 "C104" V 3448 1950 50  0000 C CNN
F 1 "150n" V 3539 1950 50  0000 C CNN
F 2 "resistance-grospad:C_Rect_L11.0mm_W2.7mm_P11mm_MKT-grospad-padcarre2mm" H 3738 1800 50  0001 C CNN
F 3 "" H 3700 1950 50  0001 C CNN
	1    3700 1950
	0    1    1    0   
$EndComp
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R104
U 1 1 5CD4D74F
P 3400 2250
F 0 "R104" H 3470 2296 50  0000 L CNN
F 1 "5k6" H 3470 2205 50  0000 L CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3330 2250 50  0001 C CNN
F 3 "" H 3400 2250 50  0001 C CNN
	1    3400 2250
	1    0    0    -1  
$EndComp
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R105
U 1 1 5CD4D7BF
P 4050 2250
F 0 "R105" H 4120 2296 50  0000 L CNN
F 1 "5k6" H 4120 2205 50  0000 L CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM12mm-PAS2MM" V 3980 2250 50  0001 C CNN
F 3 "" H 4050 2250 50  0001 C CNN
	1    4050 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1950 4050 1950
Wire Wire Line
	4050 2100 4050 1950
Connection ~ 4050 1950
Wire Wire Line
	4050 1950 4350 1950
Wire Wire Line
	3400 2100 3400 1950
Wire Wire Line
	3300 1950 3400 1950
Connection ~ 3400 1950
Wire Wire Line
	3400 1950 3550 1950
$Comp
L Device:R_POT_Dual_Separate RV101
U 1 1 5CD4DD0D
P 3400 2650
F 0 "RV101" H 3330 2604 50  0000 R CNN
F 1 "100k" H 3330 2695 50  0000 R CIN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Double_Vertical-padcarre2.5" H 3400 2650 50  0001 C CNN
F 3 "~" H 3400 2650 50  0001 C CNN
	1    3400 2650
	1    0    0    1   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV101
U 2 1 5CD4DE9B
P 4050 2650
F 0 "RV101" H 3981 2604 50  0000 R CNN
F 1 "100k" H 3981 2695 50  0000 R CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Double_Vertical-padcarre2.5" H 4050 2650 50  0001 C CNN
F 3 "~" H 4050 2650 50  0001 C CNN
	2    4050 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4050 2400 4050 2500
Wire Wire Line
	3400 2400 3400 2500
Wire Wire Line
	3900 2650 3850 2650
Wire Wire Line
	3850 2650 3850 2850
Wire Wire Line
	3850 2850 4050 2850
Wire Wire Line
	4050 2850 4050 2800
Wire Wire Line
	3550 2650 3600 2650
Wire Wire Line
	3600 2650 3600 2850
Wire Wire Line
	3600 2850 3400 2850
Wire Wire Line
	3400 2850 3400 2800
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R107
U 1 1 5CD4E2A5
P 5050 2800
F 0 "R107" H 5120 2846 50  0000 L CNN
F 1 "1k7" H 5120 2755 50  0000 L CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4980 2800 50  0001 C CNN
F 3 "" H 5050 2800 50  0001 C CNN
	1    5050 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2650 5050 2550
Wire Wire Line
	5050 2550 4300 2550
Wire Wire Line
	4300 2550 4300 2150
Wire Wire Line
	4300 2150 4350 2150
Connection ~ 5050 2550
Wire Wire Line
	5050 2550 5050 2500
Wire Wire Line
	4950 2050 5050 2050
Wire Wire Line
	5050 2050 5050 2200
Wire Wire Line
	4050 2850 4050 3050
Wire Wire Line
	4050 3050 5050 3050
Wire Wire Line
	5050 3050 5050 2950
Connection ~ 4050 2850
Wire Wire Line
	5050 2050 5300 2050
Wire Wire Line
	5300 2050 5300 3450
Wire Wire Line
	5300 3450 3400 3450
Wire Wire Line
	3400 3450 3400 2850
Connection ~ 5050 2050
Connection ~ 3400 2850
$Comp
L power:GND #PWR?
U 1 1 5CD4F16F
P 5050 3100
F 0 "#PWR?" H 5050 2850 50  0001 C CNN
F 1 "GND" H 5055 2927 50  0000 C CNN
F 2 "" H 5050 3100 50  0001 C CNN
F 3 "" H 5050 3100 50  0001 C CNN
	1    5050 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3100 5050 3050
Connection ~ 5050 3050
Wire Wire Line
	5050 2550 5550 2550
Wire Wire Line
	5550 2750 5500 2750
Wire Wire Line
	5500 2750 5500 3500
Wire Wire Line
	5500 3500 6300 3500
Wire Wire Line
	6300 3500 6300 2650
Wire Wire Line
	6300 2650 6150 2650
Connection ~ 6300 2650
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R109
U 1 1 5CD510BE
P 7350 2650
F 0 "R109" V 7143 2650 50  0000 C CNN
F 1 "1k" V 7234 2650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7280 2650 50  0001 C CNN
F 3 "" H 7350 2650 50  0001 C CNN
	1    7350 2650
	0    1    1    0   
$EndComp
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R106
U 1 1 5CD50FD5
P 5050 2350
F 0 "R106" H 5120 2396 50  0000 L CNN
F 1 "1k" H 5120 2305 50  0000 L CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 4980 2350 50  0001 C CNN
F 3 "" H 5050 2350 50  0001 C CNN
	1    5050 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV102
U 1 1 5CD52228
P 7050 2650
F 0 "RV102" V 6843 2650 50  0000 C CNN
F 1 "100k" V 6934 2650 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Double_Vertical-padcarre2.5" H 7050 2650 50  0001 C CNN
F 3 "~" H 7050 2650 50  0001 C CNN
	1    7050 2650
	0    -1   1    0   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV102
U 2 1 5CD52385
P 7900 2650
F 0 "RV102" V 7693 2650 50  0000 C CNN
F 1 "100k" V 7784 2650 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Double_Vertical-padcarre2.5" H 7900 2650 50  0001 C CNN
F 3 "~" H 7900 2650 50  0001 C CNN
	2    7900 2650
	0    -1   1    0   
$EndComp
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R110
U 1 1 5CD523DD
P 8250 2650
F 0 "R110" V 8043 2650 50  0000 C CNN
F 1 "1k" V 8134 2650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 8180 2650 50  0001 C CNN
F 3 "" H 8250 2650 50  0001 C CNN
	1    8250 2650
	0    1    1    0   
$EndComp
$Comp
L highpass-lowpass-rescue:C-Device-riaav3-rescue-highpass-lowpass-rescue C105
U 1 1 5CD52541
P 7600 3250
F 0 "C105" H 7485 3204 50  0000 R CNN
F 1 "10n" H 7485 3295 50  0000 R CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 7638 3100 50  0001 C CNN
F 3 "" H 7600 3250 50  0001 C CNN
	1    7600 3250
	-1   0    0    1   
$EndComp
$Comp
L highpass-lowpass-rescue:C-Device-riaav3-rescue-highpass-lowpass-rescue C106
U 1 1 5CD526BF
P 8000 3250
F 0 "C106" H 7885 3204 50  0000 R CNN
F 1 "10n" H 7885 3295 50  0000 R CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 8038 3100 50  0001 C CNN
F 3 "" H 8000 3250 50  0001 C CNN
	1    8000 3250
	-1   0    0    1   
$EndComp
$Comp
L highpass-lowpass-rescue:C-Device-riaav3-rescue-highpass-lowpass-rescue C107
U 1 1 5CD52707
P 8450 3250
F 0 "C107" H 8335 3204 50  0000 R CNN
F 1 "10n" H 8335 3295 50  0000 R CNN
F 2 "resistance-grospad:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2-padcarre2mm" H 8488 3100 50  0001 C CNN
F 3 "" H 8450 3250 50  0001 C CNN
	1    8450 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 2650 7600 2650
Wire Wire Line
	7600 3100 7600 3050
Connection ~ 7600 2650
Wire Wire Line
	7600 2650 7750 2650
Wire Wire Line
	8050 2650 8100 2650
Wire Wire Line
	8000 3100 8000 3050
Wire Wire Line
	8000 3050 7600 3050
Connection ~ 7600 3050
Wire Wire Line
	7600 3050 7600 2850
Wire Wire Line
	8000 3400 8000 3450
Wire Wire Line
	8000 3450 7600 3450
Wire Wire Line
	7600 3450 7600 3400
Wire Wire Line
	9050 2650 8950 2650
Wire Wire Line
	8450 3100 8450 2650
Connection ~ 8450 2650
Wire Wire Line
	8450 2650 8400 2650
Wire Wire Line
	6800 2650 6800 2850
Wire Wire Line
	6800 2850 7050 2850
Wire Wire Line
	7050 2850 7050 2800
Connection ~ 6800 2650
Wire Wire Line
	6800 2650 6900 2650
Wire Wire Line
	9050 2850 9000 2850
Wire Wire Line
	9000 2850 9000 3400
Wire Wire Line
	9000 3400 9700 3400
Wire Wire Line
	9700 3400 9700 2750
Wire Wire Line
	9700 2750 9650 2750
Wire Wire Line
	9700 3400 9700 3900
Wire Wire Line
	9700 3900 7600 3900
Wire Wire Line
	7600 3900 7600 3450
Connection ~ 9700 3400
Connection ~ 7600 3450
$Comp
L power:GND #PWR?
U 1 1 5CD58C09
P 8450 3400
F 0 "#PWR?" H 8450 3150 50  0001 C CNN
F 1 "GND" H 8455 3227 50  0000 C CNN
F 2 "" H 8450 3400 50  0001 C CNN
F 3 "" H 8450 3400 50  0001 C CNN
	1    8450 3400
	1    0    0    -1  
$EndComp
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R112
U 1 1 5CD58F00
P 10000 2750
F 0 "R112" V 9793 2750 50  0000 C CNN
F 1 "100" V 9884 2750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM16mm-padcarre2mm" V 9930 2750 50  0001 C CNN
F 3 "" H 10000 2750 50  0001 C CNN
	1    10000 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 2750 9850 2750
Connection ~ 9700 2750
$Comp
L Connector:Conn_01x02_Male J103
U 1 1 5CD59E23
P 10750 2850
F 0 "J103" H 10723 2730 50  0000 R CNN
F 1 "Conn_01x02_Male" H 11000 2950 50  0000 R CNN
F 2 "resistance-grospad:grospad-connector-2-bis" H 10750 2850 50  0001 C CNN
F 3 "~" H 10750 2850 50  0001 C CNN
	1    10750 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	10550 2750 10150 2750
Text Label 10300 2750 0    50   ~ 0
output
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R113
U 1 1 5CD5ADA0
P 10300 2850
F 0 "R113" V 10093 2850 50  0000 C CNN
F 1 "75" V 10184 2850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 10230 2850 50  0001 C CNN
F 3 "" H 10300 2850 50  0001 C CNN
	1    10300 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	10550 2850 10450 2850
Wire Wire Line
	10150 2850 10100 2850
Wire Wire Line
	10100 2850 10100 2950
$Comp
L power:GND #PWR?
U 1 1 5CD5CCAE
P 10100 2950
F 0 "#PWR?" H 10100 2700 50  0001 C CNN
F 1 "GND" H 10105 2777 50  0000 C CNN
F 2 "" H 10100 2950 50  0001 C CNN
F 3 "" H 10100 2950 50  0001 C CNN
	1    10100 2950
	1    0    0    -1  
$EndComp
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R111
U 1 1 5CD5E07A
P 8750 2650
F 0 "R111" V 8543 2650 50  0000 C CNN
F 1 "1k" V 10600 -3350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 8680 2650 50  0001 C CNN
F 3 "" H 8750 2650 50  0001 C CNN
	1    8750 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	8600 2650 8450 2650
$Comp
L Switch:SW_SPST SW102
U 1 1 5CD5F445
P 8450 2150
F 0 "SW102" H 8450 2385 50  0000 C CNN
F 1 "bypass LP" H 8450 2294 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2-padcaree2mm" H 8450 2150 50  0001 C CNN
F 3 "" H 8450 2150 50  0001 C CNN
	1    8450 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 2650 8950 2150
Wire Wire Line
	8950 2150 8650 2150
Connection ~ 8950 2650
Wire Wire Line
	8950 2650 8900 2650
Wire Wire Line
	6800 2650 6800 2150
Wire Wire Line
	6800 2150 8250 2150
$Comp
L Switch:SW_SPST SW101
U 1 1 5CD61CB5
P 3450 1400
F 0 "SW101" H 3450 1635 50  0000 C CNN
F 1 "bypass HP" H 3450 1544 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" H 3450 1400 50  0001 C CNN
F 3 "" H 3450 1400 50  0001 C CNN
	1    3450 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1950 2950 1950
Wire Wire Line
	2950 1400 3250 1400
Wire Wire Line
	3650 1400 4050 1400
Wire Wire Line
	4050 1400 4050 1950
$Comp
L Connector:Conn_01x02_Male J101
U 1 1 5CD64CBF
P 550 1850
F 0 "J101" H 656 2028 50  0000 C CNN
F 1 "Conn_01x02_Male" H 850 2250 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2-bis" H 550 1850 50  0001 C CNN
F 3 "~" H 550 1850 50  0001 C CNN
	1    550  1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  1950 900  1950
Wire Wire Line
	900  1950 900  2250
$Comp
L power:GND #PWR?
U 1 1 5CD6821A
P 900 2250
F 0 "#PWR?" H 900 2000 50  0001 C CNN
F 1 "GND" H 905 2077 50  0000 C CNN
F 2 "" H 900 2250 50  0001 C CNN
F 3 "" H 900 2250 50  0001 C CNN
	1    900  2250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J102
U 1 1 5CD684E5
P 1150 4300
F 0 "J102" H 1044 3975 50  0000 C CNN
F 1 "alim" H 1044 4066 50  0000 C CNN
F 2 "resistance-grospad:connection-3-padcarre2mm" H 1150 4300 50  0001 C CNN
F 3 "~" H 1150 4300 50  0001 C CNN
	1    1150 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 4400 1650 4700
Wire Wire Line
	3250 4600 3250 4700
Wire Wire Line
	1650 4700 1800 4700
$Comp
L highpass-lowpass-rescue:C-Device-riaav3-rescue-highpass-lowpass-rescue C101
U 1 1 5CD74648
P 2850 4450
F 0 "C101" H 2735 4404 50  0000 R CNN
F 1 "100n" H 2735 4495 50  0000 R CNN
F 2 "resistance-grospad:C_Rect_L7.0mm_W2.5mm_P5.00mm-grospad" H 2888 4300 50  0001 C CNN
F 3 "" H 2850 4450 50  0001 C CNN
	1    2850 4450
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 4600 2850 4700
Connection ~ 2850 4300
Connection ~ 2850 4700
Text Label 2850 4300 0    50   ~ 0
+15
Text Label 2900 4700 0    50   ~ 0
-15
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R102
U 1 1 5CD79058
P 1950 4300
F 0 "R102" V 1743 4300 50  0000 C CNN
F 1 "2R2" V 1834 4300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1880 4300 50  0001 C CNN
F 3 "" H 1950 4300 50  0001 C CNN
	1    1950 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	2100 4300 2850 4300
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R103
U 1 1 5CD7B626
P 1950 4700
F 0 "R103" V 1743 4700 50  0000 C CNN
F 1 "2R2" V 1834 4700 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1880 4700 50  0001 C CNN
F 3 "" H 1950 4700 50  0001 C CNN
	1    1950 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	2100 4700 2850 4700
$Comp
L power:GND #PWR?
U 1 1 5CD870FE
P 1350 4200
F 0 "#PWR?" H 1350 3950 50  0001 C CNN
F 1 "GND" V 1355 4072 50  0000 R CNN
F 2 "" H 1350 4200 50  0001 C CNN
F 3 "" H 1350 4200 50  0001 C CNN
	1    1350 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2050 2050 2000 2050
Wire Wire Line
	2000 2050 2000 2800
Wire Wire Line
	2000 2800 2800 2800
Wire Wire Line
	2800 2800 2800 1950
Wire Wire Line
	2800 1950 2650 1950
Wire Wire Line
	2800 1950 2950 1950
Connection ~ 2800 1950
Connection ~ 2950 1950
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R101
U 1 1 5CDB14C7
P 1150 2150
F 0 "R101" H 1080 2104 50  0000 R CNN
F 1 "22k" H 1080 2195 50  0000 R CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1080 2150 50  0001 C CNN
F 3 "" H 1150 2150 50  0001 C CNN
	1    1150 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1150 1850 1150 2000
$Comp
L power:GND #PWR?
U 1 1 5CDB7548
P 1150 2300
F 0 "#PWR?" H 1150 2050 50  0001 C CNN
F 1 "GND" H 1155 2127 50  0000 C CNN
F 2 "" H 1150 2300 50  0001 C CNN
F 3 "" H 1150 2300 50  0001 C CNN
	1    1150 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  1850 1150 1850
Connection ~ 1150 1850
Wire Wire Line
	1150 1850 2050 1850
Wire Wire Line
	7900 2800 7900 2850
Wire Wire Line
	7900 2850 7600 2850
Connection ~ 7600 2850
Wire Wire Line
	7600 2850 7600 2650
Text Notes 1100 1000 0    157  ~ 0
HighPass\n     f = 1 / ( 2 * π * R * C ) 
Text Notes 7650 1100 0    157  ~ 0
LowPass\n
$Comp
L Amplifier_Operational:TL072 U101
U 2 1 5CE28A9F
P 2350 1950
F 0 "U101" H 2350 2317 50  0000 C CNN
F 1 "TL072" H 2350 2226 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 2350 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 2350 1950 50  0001 C CNN
	2    2350 1950
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U101
U 1 1 5CE28D01
P 4650 2050
F 0 "U101" H 4650 2417 50  0000 C CNN
F 1 "TL072" H 4650 2326 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 4650 2050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 4650 2050 50  0001 C CNN
	1    4650 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1400 2950 1950
Wire Wire Line
	2850 4700 3250 4700
Connection ~ 3250 4700
Wire Wire Line
	3250 4700 3900 4700
$Comp
L Amplifier_Operational:TL072 U101
U 3 1 5CE2F403
P 4300 4500
F 0 "U101" H 4258 4546 50  0000 L CNN
F 1 "TL072" H 4258 4455 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 4300 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 4300 4500 50  0001 C CNN
	3    4300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4200 3900 4200
Wire Wire Line
	3900 4200 3900 4300
Wire Wire Line
	4200 4800 3900 4800
Wire Wire Line
	3900 4800 3900 4700
Wire Wire Line
	4200 4200 4550 4200
Connection ~ 4200 4200
$Comp
L Amplifier_Operational:TL072 U102
U 2 1 5CE47BD9
P 5850 2650
F 0 "U102" H 5850 3017 50  0000 C CNN
F 1 "TL072" H 5850 2926 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 5850 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 5850 2650 50  0001 C CNN
	2    5850 2650
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U102
U 1 1 5CE47D98
P 9350 2750
F 0 "U102" H 9350 3117 50  0000 C CNN
F 1 "TL072" H 9350 3026 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 9350 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 9350 2750 50  0001 C CNN
	1    9350 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4800 4550 4800
Connection ~ 4200 4800
Wire Wire Line
	1350 4400 1650 4400
Wire Wire Line
	1350 4300 1800 4300
Wire Wire Line
	6300 2650 6400 2650
$Comp
L highpass-lowpass-rescue:R-Device-riaav3-rescue-highpass-lowpass-rescue R108
U 1 1 5D23FA72
P 6550 2650
F 0 "R108" V 6343 2650 50  0000 C CNN
F 1 "100" V 6434 2650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM15mm-padcarre2mm" V 6480 2650 50  0001 C CNN
F 3 "" H 6550 2650 50  0001 C CNN
	1    6550 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	6700 2650 6800 2650
Wire Wire Line
	2850 4300 3900 4300
$Comp
L Amplifier_Operational:TL072 U102
U 3 1 5D8BAD3A
P 4650 4500
F 0 "U102" H 4608 4546 50  0000 L CNN
F 1 "TL072" H 4608 4455 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 4650 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 4650 4500 50  0001 C CNN
	3    4650 4500
	1    0    0    -1  
$EndComp
Connection ~ 4550 4800
Wire Wire Line
	4550 4800 4700 4800
Connection ~ 4550 4200
Wire Wire Line
	4550 4200 4700 4200
$EndSCHEMATC
