EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L riaav3-rescue:R-Device R4
U 1 1 5A2162A8
P 2350 4750
F 0 "R4" V 2430 4750 50  0000 C CNN
F 1 "2k2" V 2350 4750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2280 4750 50  0001 C CNN
F 3 "" H 2350 4750 50  0000 C CNN
	1    2350 4750
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:R-Device R1
U 1 1 5A2162C0
P 1850 5850
F 0 "R1" V 1930 5850 50  0000 C CNN
F 1 "47k" V 1850 5850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 1780 5850 50  0001 C CNN
F 3 "" H 1850 5850 50  0000 C CNN
	1    1850 5850
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:R-Device R3
U 1 1 5A216324
P 2300 5750
F 0 "R3" V 2380 5750 50  0000 C CNN
F 1 "4k7" V 2300 5750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2230 5750 50  0001 C CNN
F 3 "" H 2300 5750 50  0000 C CNN
	1    2300 5750
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:C-Device C1
U 1 1 5A21635A
P 1300 5750
F 0 "C1" H 1325 5850 50  0000 L CNN
F 1 "100n" H 1325 5650 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT" H 1338 5600 50  0001 C CNN
F 3 "" H 1300 5750 50  0000 C CNN
	1    1300 5750
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:CP-Device C3
U 1 1 5A216377
P 2250 6350
F 0 "C3" H 2275 6450 50  0000 L CNN
F 1 "100u" H 2275 6250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D6.3_L11.2_P2.5" H 2288 6200 50  0001 C CNN
F 3 "" H 2250 6350 50  0000 C CNN
	1    2250 6350
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:R-Device R7
U 1 1 5A2163B1
P 2900 5550
F 0 "R7" V 2980 5550 50  0000 C CNN
F 1 "180k" V 2900 5550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2830 5550 50  0001 C CNN
F 3 "" H 2900 5550 50  0000 C CNN
	1    2900 5550
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:R-Device R8
U 1 1 5A21641B
P 3100 5950
F 0 "R8" V 3180 5950 50  0000 C CNN
F 1 "10k" V 3100 5950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 3030 5950 50  0001 C CNN
F 3 "" H 3100 5950 50  0000 C CNN
	1    3100 5950
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:R-Device R17
U 1 1 5A216579
P 5350 5500
F 0 "R17" V 5430 5500 50  0000 C CNN
F 1 "100k" V 5350 5500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 5280 5500 50  0001 C CNN
F 3 "" H 5350 5500 50  0000 C CNN
	1    5350 5500
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:C-Device C5
U 1 1 5A2165D6
P 2700 5950
F 0 "C5" H 2725 6050 50  0000 L CNN
F 1 "22n" H 2725 5850 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 2738 5800 50  0001 C CNN
F 3 "" H 2700 5950 50  0000 C CNN
	1    2700 5950
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:C-Device C10
U 1 1 5A216729
P 5150 5050
F 0 "C10" H 5175 5150 50  0000 L CNN
F 1 "1u" H 5175 4950 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W3.5_P5" H 5188 4900 50  0001 C CNN
F 3 "" H 5150 5050 50  0000 C CNN
	1    5150 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1300 4750 1300 5600
Wire Wire Line
	1850 4750 1850 5700
Wire Wire Line
	1150 4750 1300 4750
Connection ~ 1850 4750
Wire Wire Line
	2250 6700 2250 6500
Wire Wire Line
	1850 6700 1850 6000
Wire Wire Line
	2250 6200 2250 5900
Wire Wire Line
	2250 5900 2300 5900
Wire Wire Line
	1300 6700 1300 5900
Connection ~ 1850 6700
Wire Wire Line
	2950 5950 2850 5950
Wire Wire Line
	2550 5950 2550 5550
Wire Wire Line
	2300 5550 2550 5550
Wire Wire Line
	3400 5950 3250 5950
Wire Wire Line
	3400 5550 3050 5550
Connection ~ 3400 5550
Wire Wire Line
	5350 6700 5350 5650
Wire Wire Line
	5300 5050 5350 5050
Wire Wire Line
	5350 5050 5350 5350
Text Label 1150 4750 0    60   ~ 0
inR
Wire Wire Line
	2750 4700 2500 4700
Wire Wire Line
	2500 4700 2500 4750
Wire Wire Line
	2300 4900 2300 5550
Wire Wire Line
	2300 4900 2750 4900
Connection ~ 2300 5550
Connection ~ 2550 5550
Wire Wire Line
	3400 4800 3350 4800
Connection ~ 5350 5050
Text Label 5700 5050 0    60   ~ 0
outR
$Comp
L riaav3-rescue:R-Device R6
U 1 1 5A21936E
P 2800 1550
F 0 "R6" V 2880 1550 50  0000 C CNN
F 1 "2k2" V 2800 1550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2730 1550 50  0001 C CNN
F 3 "" H 2800 1550 50  0000 C CNN
	1    2800 1550
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:R-Device R2
U 1 1 5A219374
P 2300 2650
F 0 "R2" V 2380 2650 50  0000 C CNN
F 1 "47k" V 2300 2650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2230 2650 50  0001 C CNN
F 3 "" H 2300 2650 50  0000 C CNN
	1    2300 2650
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:R-Device R5
U 1 1 5A21937A
P 2750 2550
F 0 "R5" V 2830 2550 50  0000 C CNN
F 1 "4k7" V 2750 2550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 2680 2550 50  0001 C CNN
F 3 "" H 2750 2550 50  0000 C CNN
	1    2750 2550
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:C-Device C2
U 1 1 5A219380
P 1750 2550
F 0 "C2" H 1775 2650 50  0000 L CNN
F 1 "100n" H 1775 2450 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT" H 1788 2400 50  0001 C CNN
F 3 "" H 1750 2550 50  0000 C CNN
	1    1750 2550
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:CP-Device C4
U 1 1 5A219386
P 2700 3150
F 0 "C4" H 2725 3250 50  0000 L CNN
F 1 "100u" H 2725 3050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D6.3_L11.2_P2.5" H 2738 3000 50  0001 C CNN
F 3 "" H 2700 3150 50  0000 C CNN
	1    2700 3150
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:R-Device R9
U 1 1 5A21938C
P 3350 2350
F 0 "R9" V 3430 2350 50  0000 C CNN
F 1 "180k" V 3350 2350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 3280 2350 50  0001 C CNN
F 3 "" H 3350 2350 50  0000 C CNN
	1    3350 2350
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:R-Device R10
U 1 1 5A219392
P 3550 2750
F 0 "R10" V 3630 2750 50  0000 C CNN
F 1 "10k" V 3550 2750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 3480 2750 50  0001 C CNN
F 3 "" H 3550 2750 50  0000 C CNN
	1    3550 2750
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:R-Device R20
U 1 1 5A2193AA
P 6400 2300
F 0 "R20" H 6470 2346 50  0000 L CNN
F 1 "100k" H 6470 2255 50  0000 L CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 6330 2300 50  0001 C CNN
F 3 "" H 6400 2300 50  0000 C CNN
	1    6400 2300
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:C-Device C6
U 1 1 5A2193B0
P 3150 2750
F 0 "C6" H 3175 2850 50  0000 L CNN
F 1 "22n" H 3175 2650 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 3188 2600 50  0001 C CNN
F 3 "" H 3150 2750 50  0000 C CNN
	1    3150 2750
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:C-Device C12
U 1 1 5A2193C2
P 6200 1850
F 0 "C12" H 6225 1950 50  0000 L CNN
F 1 "1u" H 6225 1750 50  0000 L CNN
F 2 "resistance-grospad:condensateur-rect_RM11mm" H 6238 1700 50  0001 C CNN
F 3 "" H 6200 1850 50  0000 C CNN
	1    6200 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 1550 2300 2500
Wire Wire Line
	1400 1550 1750 1550
Connection ~ 2300 1550
Wire Wire Line
	2700 3500 2700 3300
Wire Wire Line
	1050 3500 1200 3500
Wire Wire Line
	2300 3500 2300 2800
Wire Wire Line
	2700 3000 2700 2700
Wire Wire Line
	2700 2700 2750 2700
Wire Wire Line
	1750 3500 1750 2700
Connection ~ 2300 3500
Wire Wire Line
	3400 2750 3300 2750
Wire Wire Line
	3000 2750 3000 2350
Wire Wire Line
	2750 2350 3000 2350
Wire Wire Line
	3850 2750 3700 2750
Wire Wire Line
	3850 2350 3500 2350
Connection ~ 3850 2350
Wire Wire Line
	6400 3500 6400 3150
Wire Wire Line
	6350 1850 6400 1850
Wire Wire Line
	6400 1850 6400 2150
Connection ~ 1750 1550
Connection ~ 1750 3500
Text Label 1600 1550 0    60   ~ 0
inL
Text Label 1600 3500 0    60   ~ 0
gnd
Wire Wire Line
	3200 1500 2950 1500
Wire Wire Line
	2950 1500 2950 1550
Wire Wire Line
	2750 1700 2750 2350
Wire Wire Line
	2750 1700 3200 1700
Connection ~ 2750 2350
Connection ~ 3000 2350
Wire Wire Line
	3850 1600 3800 1600
Connection ~ 6400 1850
Text Label 6750 1850 0    60   ~ 0
outL
Text Label 8800 5950 0    60   ~ 0
+15v
Text Label 9950 6350 0    60   ~ 0
-15
Connection ~ 1650 3500
$Comp
L riaav3-rescue:PWR_FLAG-power #FLG0101
U 1 1 5A219422
P 1750 1550
F 0 "#FLG0101" H 1750 1645 50  0001 C CNN
F 1 "PWR_FLAG" H 1750 1730 50  0000 C CNN
F 2 "" H 1750 1550 50  0000 C CNN
F 3 "" H 1750 1550 50  0000 C CNN
	1    1750 1550
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:PWR_FLAG-power #FLG0102
U 1 1 5A219428
P 1650 3500
F 0 "#FLG0102" H 1650 3595 50  0001 C CNN
F 1 "PWR_FLAG" H 1650 3680 50  0000 C CNN
F 2 "" H 1650 3500 50  0000 C CNN
F 3 "" H 1650 3500 50  0000 C CNN
	1    1650 3500
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:PWR_FLAG-power #FLG0103
U 1 1 5A21942E
P 6800 5850
F 0 "#FLG0103" H 6800 5945 50  0001 C CNN
F 1 "PWR_FLAG" H 6800 6030 50  0000 C CNN
F 2 "" H 6800 5850 50  0000 C CNN
F 3 "" H 6800 5850 50  0000 C CNN
	1    6800 5850
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:R-Device R19
U 1 1 5A21943A
P 9300 5950
F 0 "R19" V 9380 5950 50  0000 C CNN
F 1 "2.2" V 9300 5950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 9230 5950 50  0001 C CNN
F 3 "" H 9300 5950 50  0000 C CNN
	1    9300 5950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 5850 6800 5950
Connection ~ 6800 5950
$Comp
L riaav3-rescue:R-Device R18
U 1 1 5A219443
P 9350 6350
F 0 "R18" V 9430 6350 50  0000 C CNN
F 1 "2.2" V 9350 6350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm" V 9280 6350 50  0001 C CNN
F 3 "" H 9350 6350 50  0000 C CNN
	1    9350 6350
	0    -1   -1   0   
$EndComp
$Comp
L riaav3-rescue:Conn_01x03 P2
U 1 1 5A21944A
P 10250 5950
F 0 "P2" H 10250 6150 50  0000 C CNN
F 1 "CONN_01X03" V 10350 5950 50  0000 C CNN
F 2 "resistance-grospad:Fan_Pin_Header_Straight_1x03-grospad" H 10250 5950 50  0001 C CNN
F 3 "" H 10250 5950 50  0000 C CNN
	1    10250 5950
	1    0    0    1   
$EndComp
Wire Wire Line
	1150 4750 1150 3700
Wire Wire Line
	1150 3700 1050 3700
Connection ~ 1300 4750
Wire Wire Line
	1400 1550 1400 3400
Wire Wire Line
	1400 3400 1050 3400
Wire Wire Line
	1600 3500 1650 3500
NoConn ~ 2950 4500
Wire Wire Line
	7200 2750 7400 2750
Wire Wire Line
	7200 5050 7200 2750
Wire Wire Line
	1300 6700 1850 6700
Text Notes 6450 5550 0    60   ~ 0
R14 et R13 -> 100k -> +36dB\n\n5mv en entrée -> ~500mV en sortie
Text Notes 8450 1200 0    60   ~ 0
Edit:\n1/01/19: R13 et 514 100k -> 47k\n
$Comp
L riaav3-rescue:Conn_01x02 P3
U 1 1 5C2B43BF
P 7550 1850
F 0 "P3" H 7550 1950 50  0000 C CNN
F 1 "Conn_01x02" H 7550 1650 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 7550 1850 50  0001 C CNN
F 3 "" H 7550 1850 50  0001 C CNN
	1    7550 1850
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:Conn_01x02 P5
U 1 1 5C2B4A21
P 7600 2750
F 0 "P5" H 7600 2850 50  0000 C CNN
F 1 "Conn_01x02" H 7600 2550 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 7600 2750 50  0001 C CNN
F 3 "" H 7600 2750 50  0001 C CNN
	1    7600 2750
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:Conn_01x02 P1
U 1 1 5C2B4EAA
P 850 3400
F 0 "P1" H 850 3500 50  0000 C CNN
F 1 "Conn_01x02" H 850 3200 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 850 3400 50  0001 C CNN
F 3 "" H 850 3400 50  0001 C CNN
	1    850  3400
	-1   0    0    -1  
$EndComp
$Comp
L riaav3-rescue:Conn_01x02 P4
U 1 1 5C2B50E5
P 850 3700
F 0 "P4" H 850 3800 50  0000 C CNN
F 1 "Conn_01x02" H 850 3500 50  0000 C CNN
F 2 "resistance-grospad:grospad-connector-2" H 850 3700 50  0001 C CNN
F 3 "" H 850 3700 50  0001 C CNN
	1    850  3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 3600 1200 3600
Wire Wire Line
	1200 3600 1200 3500
Connection ~ 1200 3500
Wire Wire Line
	9900 6150 9950 6150
Wire Wire Line
	8400 6000 8400 5950
Connection ~ 8400 5950
$Comp
L riaav3-rescue:CP-Device C14
U 1 1 5C2DFB69
P 8750 6400
F 0 "C14" H 8775 6500 50  0000 L CNN
F 1 "10u" H 8775 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D8.0mm_P3.50mm" H 8788 6250 50  0001 C CNN
F 3 "" H 8750 6400 50  0000 C CNN
	1    8750 6400
	-1   0    0    1   
$EndComp
$Comp
L riaav3-rescue:C-Device C15
U 1 1 5C2E00AD
P 7350 6100
F 0 "C15" H 7375 6200 50  0000 L CNN
F 1 "100n" H 7375 6000 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 7388 5950 50  0001 C CNN
F 3 "" H 7350 6100 50  0000 C CNN
	1    7350 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 6050 9950 6050
Wire Wire Line
	9950 6050 9950 6150
Connection ~ 9950 6150
Wire Wire Line
	1850 4750 2200 4750
Wire Wire Line
	1850 6700 2250 6700
Wire Wire Line
	3400 5550 3400 5950
Wire Wire Line
	2300 5550 2300 5600
Wire Wire Line
	2550 5550 2750 5550
Wire Wire Line
	5350 5050 7200 5050
Wire Wire Line
	2300 1550 2650 1550
Wire Wire Line
	2300 3500 2700 3500
Wire Wire Line
	3850 2350 3850 2750
Wire Wire Line
	1750 1550 2300 1550
Wire Wire Line
	1750 3500 2300 3500
Wire Wire Line
	6750 5950 6800 5950
Wire Wire Line
	2750 2350 2750 2400
Wire Wire Line
	3000 2350 3200 2350
Wire Wire Line
	6400 1850 7350 1850
Wire Wire Line
	1650 3500 1750 3500
Wire Wire Line
	6800 5950 7350 5950
Wire Wire Line
	1300 4750 1850 4750
Wire Wire Line
	1200 3500 1650 3500
Wire Wire Line
	9950 6150 9950 6350
Wire Wire Line
	1750 1550 1750 2400
$Comp
L riaav3-rescue:GNDA-power #PWR0104
U 1 1 5C56E858
P 7400 2850
F 0 "#PWR0104" H 7400 2600 50  0001 C CNN
F 1 "GNDA" H 7405 2677 50  0000 C CNN
F 2 "" H 7400 2850 50  0001 C CNN
F 3 "" H 7400 2850 50  0001 C CNN
	1    7400 2850
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:GNDA-power #PWR0105
U 1 1 5C5785C9
P 6400 3500
F 0 "#PWR0105" H 6400 3250 50  0001 C CNN
F 1 "GNDA" H 6405 3327 50  0000 C CNN
F 2 "" H 6400 3500 50  0001 C CNN
F 3 "" H 6400 3500 50  0001 C CNN
	1    6400 3500
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:GNDA-power #PWR0106
U 1 1 5C57D2FB
P 7350 1950
F 0 "#PWR0106" H 7350 1700 50  0001 C CNN
F 1 "GNDA" H 7355 1777 50  0000 C CNN
F 2 "" H 7350 1950 50  0001 C CNN
F 3 "" H 7350 1950 50  0001 C CNN
	1    7350 1950
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:TL072-Amplifier_Operational U1
U 3 1 5C582D5B
P 6900 6250
F 0 "U1" H 6858 6296 50  0000 L CNN
F 1 "TL072" H 6858 6205 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6900 6250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 6900 6250 50  0001 C CNN
	3    6900 6250
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:TL072-Amplifier_Operational U1
U 1 1 5C582EDE
P 3050 4800
F 0 "U1" H 3050 5167 50  0000 C CNN
F 1 "TL072" H 3050 5076 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3050 4800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3050 4800 50  0001 C CNN
	1    3050 4800
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:TL072-Amplifier_Operational U1
U 2 1 5C59670A
P 3500 1600
F 0 "U1" H 3500 1967 50  0000 C CNN
F 1 "TL072" H 3500 1876 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3500 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3500 1600 50  0001 C CNN
	2    3500 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 6350 9200 6350
Wire Wire Line
	9450 5950 10050 5950
Wire Wire Line
	9500 6350 9950 6350
Wire Wire Line
	9000 6350 9000 6550
Connection ~ 7350 5950
Wire Wire Line
	8400 5950 9150 5950
Connection ~ 8750 6550
Wire Wire Line
	8750 6550 9000 6550
Wire Wire Line
	6800 6550 7350 6550
Connection ~ 7350 6550
Wire Wire Line
	7350 6250 7350 6550
Wire Wire Line
	6050 1850 3850 1850
Wire Wire Line
	3850 1600 3850 1850
Connection ~ 3850 1850
Wire Wire Line
	3850 1850 3850 2350
Wire Wire Line
	3400 4800 3400 5050
Wire Wire Line
	2250 6700 5250 6700
Connection ~ 2250 6700
Wire Wire Line
	5000 5050 3400 5050
Connection ~ 3400 5050
Wire Wire Line
	3400 5050 3400 5550
Wire Wire Line
	2700 3500 6050 3500
Wire Wire Line
	6050 3500 6050 3150
Wire Wire Line
	6050 3150 6400 3150
Connection ~ 2700 3500
Connection ~ 6400 3150
Wire Wire Line
	6400 3150 6400 2450
$Comp
L riaav3-rescue:GNDA-power #PWR0107
U 1 1 5D241DA0
P 5250 6800
F 0 "#PWR0107" H 5250 6550 50  0001 C CNN
F 1 "GNDA" H 5255 6627 50  0000 C CNN
F 2 "" H 5250 6800 50  0001 C CNN
F 3 "" H 5250 6800 50  0001 C CNN
	1    5250 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 6800 5250 6700
Connection ~ 5250 6700
Wire Wire Line
	5250 6700 5350 6700
$Comp
L riaav3-rescue:CP-Device C13
U 1 1 5C2DE234
P 8400 6150
F 0 "C13" H 8425 6250 50  0000 L CNN
F 1 "10u" H 8425 6050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D8.0mm_P3.50mm" H 8438 6000 50  0001 C CNN
F 3 "" H 8400 6150 50  0000 C CNN
	1    8400 6150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J1
U 1 1 6067DF8D
P 8800 2500
F 0 "J1" V 8738 2412 50  0000 R CNN
F 1 "Conn_01x01_Female" V 8647 2412 50  0000 R CNN
F 2 "resistance-grospad:PINTST-2mm" H 8800 2500 50  0001 C CNN
F 3 "~" H 8800 2500 50  0001 C CNN
	1    8800 2500
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDD #PWR0108
U 1 1 6067EABB
P 8800 2700
F 0 "#PWR0108" H 8800 2450 50  0001 C CNN
F 1 "GNDD" H 8804 2545 50  0000 C CNN
F 2 "" H 8800 2700 50  0001 C CNN
F 3 "" H 8800 2700 50  0001 C CNN
	1    8800 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 6550 8750 6550
Wire Wire Line
	7350 5950 8400 5950
$Comp
L riaav3-rescue:GNDA-power #PWR0101
U 1 1 60687965
P 10050 5850
F 0 "#PWR0101" H 10050 5600 50  0001 C CNN
F 1 "GNDA" H 10055 5677 50  0000 C CNN
F 2 "" H 10050 5850 50  0001 C CNN
F 3 "" H 10050 5850 50  0001 C CNN
	1    10050 5850
	0    1    1    0   
$EndComp
$Comp
L riaav3-rescue:GNDA-power #PWR?
U 1 1 606AC21F
P 8400 6300
F 0 "#PWR?" H 8400 6050 50  0001 C CNN
F 1 "GNDA" H 8405 6127 50  0000 C CNN
F 2 "" H 8400 6300 50  0001 C CNN
F 3 "" H 8400 6300 50  0001 C CNN
	1    8400 6300
	1    0    0    -1  
$EndComp
$Comp
L riaav3-rescue:GNDA-power #PWR?
U 1 1 606AC88E
P 8750 6250
F 0 "#PWR?" H 8750 6000 50  0001 C CNN
F 1 "GNDA" H 8755 6077 50  0000 C CNN
F 2 "" H 8750 6250 50  0001 C CNN
F 3 "" H 8750 6250 50  0001 C CNN
	1    8750 6250
	-1   0    0    1   
$EndComp
$EndSCHEMATC
