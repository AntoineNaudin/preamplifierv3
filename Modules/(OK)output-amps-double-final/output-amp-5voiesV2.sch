EESchema Schematic File Version 4
LIBS:output-amp-5voiesV2-cache
EELAYER 29 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L output-amp-5voies-rescue:C C108
U 1 1 5A89C509
P 2300 6400
F 0 "C108" H 2325 6500 50  0000 L CNN
F 1 "100n" H 2325 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 2338 6250 50  0001 C CNN
F 3 "" H 2300 6400 50  0000 C CNN
	1    2300 6400
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C109
U 1 1 5A89C5A3
P 2650 6400
F 0 "C109" H 2675 6500 50  0000 L CNN
F 1 "100n" H 2675 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 2688 6250 50  0001 C CNN
F 3 "" H 2650 6400 50  0000 C CNN
	1    2650 6400
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C110
U 1 1 5A89C5F6
P 3050 6400
F 0 "C110" H 3075 6500 50  0000 L CNN
F 1 "100n" H 3075 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 3088 6250 50  0001 C CNN
F 3 "" H 3050 6400 50  0000 C CNN
	1    3050 6400
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C111
U 1 1 5A89C64C
P 3400 6400
F 0 "C111" H 3425 6500 50  0000 L CNN
F 1 "100n" H 3425 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 3438 6250 50  0001 C CNN
F 3 "" H 3400 6400 50  0000 C CNN
	1    3400 6400
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C112
U 1 1 5A89C6AC
P 3750 6400
F 0 "C112" H 3775 6500 50  0000 L CNN
F 1 "100n" H 3775 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 3788 6250 50  0001 C CNN
F 3 "" H 3750 6400 50  0000 C CNN
	1    3750 6400
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C114
U 1 1 5A89C90B
P 2300 7150
F 0 "C114" H 2325 7250 50  0000 L CNN
F 1 "100n" H 2325 7050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 2338 7000 50  0001 C CNN
F 3 "" H 2300 7150 50  0000 C CNN
	1    2300 7150
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C115
U 1 1 5A89C911
P 2650 7150
F 0 "C115" H 2675 7250 50  0000 L CNN
F 1 "100n" H 2675 7050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 2688 7000 50  0001 C CNN
F 3 "" H 2650 7150 50  0000 C CNN
	1    2650 7150
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C116
U 1 1 5A89C917
P 3050 7150
F 0 "C116" H 3075 7250 50  0000 L CNN
F 1 "100n" H 3075 7050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 3088 7000 50  0001 C CNN
F 3 "" H 3050 7150 50  0000 C CNN
	1    3050 7150
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C117
U 1 1 5A89C91D
P 3400 7150
F 0 "C117" H 3425 7250 50  0000 L CNN
F 1 "100n" H 3425 7050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 3438 7000 50  0001 C CNN
F 3 "" H 3400 7150 50  0000 C CNN
	1    3400 7150
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:C C118
U 1 1 5A89C923
P 3750 7150
F 0 "C118" H 3775 7250 50  0000 L CNN
F 1 "100n" H 3775 7050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L10.0mm_W2.5mm_P7.50mm_MKS4" H 3788 7000 50  0001 C CNN
F 3 "" H 3750 7150 50  0000 C CNN
	1    3750 7150
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:CONN_01X03 P104
U 1 1 5A89D229
P 850 6750
F 0 "P104" H 850 6950 50  0000 C CNN
F 1 "CONN_01X03" V 950 6750 50  0000 C CNN
F 2 "Connectors:PINHEAD1-3" H 850 6750 50  0001 C CNN
F 3 "" H 850 6750 50  0000 C CNN
	1    850  6750
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89D6F6
P 2300 6550
F 0 "#PWR?" H 2300 6300 50  0001 C CNN
F 1 "GND" H 2300 6400 50  0000 C CNN
F 2 "" H 2300 6550 50  0000 C CNN
F 3 "" H 2300 6550 50  0000 C CNN
	1    2300 6550
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89D7B5
P 2300 7300
F 0 "#PWR?" H 2300 7050 50  0001 C CNN
F 1 "GND" H 2300 7150 50  0000 C CNN
F 2 "" H 2300 7300 50  0000 C CNN
F 3 "" H 2300 7300 50  0000 C CNN
	1    2300 7300
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89D81A
P 2650 7300
F 0 "#PWR?" H 2650 7050 50  0001 C CNN
F 1 "GND" H 2650 7150 50  0000 C CNN
F 2 "" H 2650 7300 50  0000 C CNN
F 3 "" H 2650 7300 50  0000 C CNN
	1    2650 7300
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89D87F
P 3050 7300
F 0 "#PWR?" H 3050 7050 50  0001 C CNN
F 1 "GND" H 3050 7150 50  0000 C CNN
F 2 "" H 3050 7300 50  0000 C CNN
F 3 "" H 3050 7300 50  0000 C CNN
	1    3050 7300
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89D8E4
P 3400 7300
F 0 "#PWR?" H 3400 7050 50  0001 C CNN
F 1 "GND" H 3400 7150 50  0000 C CNN
F 2 "" H 3400 7300 50  0000 C CNN
F 3 "" H 3400 7300 50  0000 C CNN
	1    3400 7300
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89D949
P 3400 6550
F 0 "#PWR?" H 3400 6300 50  0001 C CNN
F 1 "GND" H 3400 6400 50  0000 C CNN
F 2 "" H 3400 6550 50  0000 C CNN
F 3 "" H 3400 6550 50  0000 C CNN
	1    3400 6550
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89D9AE
P 3050 6550
F 0 "#PWR?" H 3050 6300 50  0001 C CNN
F 1 "GND" H 3050 6400 50  0000 C CNN
F 2 "" H 3050 6550 50  0000 C CNN
F 3 "" H 3050 6550 50  0000 C CNN
	1    3050 6550
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89DA13
P 2650 6550
F 0 "#PWR?" H 2650 6300 50  0001 C CNN
F 1 "GND" H 2650 6400 50  0000 C CNN
F 2 "" H 2650 6550 50  0000 C CNN
F 3 "" H 2650 6550 50  0000 C CNN
	1    2650 6550
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89DBE2
P 3750 6550
F 0 "#PWR?" H 3750 6300 50  0001 C CNN
F 1 "GND" H 3750 6400 50  0000 C CNN
F 2 "" H 3750 6550 50  0000 C CNN
F 3 "" H 3750 6550 50  0000 C CNN
	1    3750 6550
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A89DC47
P 3750 7300
F 0 "#PWR?" H 3750 7050 50  0001 C CNN
F 1 "GND" H 3750 7150 50  0000 C CNN
F 2 "" H 3750 7300 50  0000 C CNN
F 3 "" H 3750 7300 50  0000 C CNN
	1    3750 7300
	1    0    0    -1  
$EndComp
Text GLabel 2100 6250 1    60   Input ~ 0
v+
Text GLabel 2100 7000 1    60   Input ~ 0
v-
$Comp
L output-amp-5voies-rescue:R R113
U 1 1 5A89E278
P 1350 6250
F 0 "R113" V 1430 6250 50  0000 C CNN
F 1 "10" V 1350 6250 50  0000 C CNN
F 2 "electronik:Resistor_Horizontal_RM6mm" V 1280 6250 50  0001 C CNN
F 3 "" H 1350 6250 50  0000 C CNN
	1    1350 6250
	0    -1   -1   0   
$EndComp
$Comp
L output-amp-5voies-rescue:R R117
U 1 1 5A89E346
P 1350 7000
F 0 "R117" V 1430 7000 50  0000 C CNN
F 1 "10" V 1350 7000 50  0000 C CNN
F 2 "electronik:Resistor_Horizontal_RM6mm" V 1280 7000 50  0001 C CNN
F 3 "" H 1350 7000 50  0000 C CNN
	1    1350 7000
	0    -1   -1   0   
$EndComp
$Comp
L output-amp-5voies-rescue:PWR_FLAG #FLG?
U 1 1 5A89E629
P 2650 6250
F 0 "#FLG?" H 2650 6345 50  0001 C CNN
F 1 "PWR_FLAG" H 2650 6430 50  0000 C CNN
F 2 "" H 2650 6250 50  0000 C CNN
F 3 "" H 2650 6250 50  0000 C CNN
	1    2650 6250
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:PWR_FLAG #FLG?
U 1 1 5A89E71B
P 2650 7000
F 0 "#FLG?" H 2650 7095 50  0001 C CNN
F 1 "PWR_FLAG" H 2650 7180 50  0000 C CNN
F 2 "" H 2650 7000 50  0000 C CNN
F 3 "" H 2650 7000 50  0000 C CNN
	1    2650 7000
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:JACK_TRS_6PINS J101
U 1 1 5A8AD933
P 14050 3300
F 0 "J101" H 14050 3700 50  0000 C CNN
F 1 "JACK_TRS_6PINS" H 14000 3000 50  0000 C CNN
F 2 "Connect:NMJ6HCD2" H 14150 3150 50  0001 C CNN
F 3 "" H 14150 3150 50  0000 C CNN
	1    14050 3300
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:GNDPWR #PWR?
U 1 1 5A8AD945
P 13400 3750
F 0 "#PWR?" H 13400 3550 50  0001 C CNN
F 1 "GNDPWR" H 13400 3620 50  0000 C CNN
F 2 "" H 13400 3700 50  0000 C CNN
F 3 "" H 13400 3700 50  0000 C CNN
	1    13400 3750
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:CONN_01X01 P101
U 1 1 5A8AD97C
P 5000 3350
F 0 "P101" H 5000 3450 50  0000 C CNN
F 1 "CONN_01X01" V 5100 3350 50  0000 C CNN
F 2 "Connectors:PINTST" H 5000 3350 50  0001 C CNN
F 3 "" H 5000 3350 50  0000 C CNN
	1    5000 3350
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:R R104
U 1 1 5A8B6DB0
P 11200 3400
F 0 "R104" V 11280 3400 50  0000 C CNN
F 1 "220" V 11200 3400 50  0000 C CNN
F 2 "electronik:Resistor_Horizontal_RM6mm" V 11130 3400 50  0001 C CNN
F 3 "" H 11200 3400 50  0000 C CNN
	1    11200 3400
	0    1    1    0   
$EndComp
Text GLabel 9800 3550 3    60   Input ~ 0
v+
Text GLabel 9800 2950 1    60   Input ~ 0
v-
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A8B6DCA
P 8400 3750
F 0 "#PWR?" H 8400 3500 50  0001 C CNN
F 1 "GND" H 8400 3600 50  0000 C CNN
F 2 "" H 8400 3750 50  0000 C CNN
F 3 "" H 8400 3750 50  0000 C CNN
	1    8400 3750
	1    0    0    -1  
$EndComp
$Comp
L output-amp-5voies-rescue:R R106
U 1 1 5A8B6DD0
P 8400 3600
F 0 "R106" V 8480 3600 50  0000 C CNN
F 1 "10k" V 8400 3600 50  0000 C CNN
F 2 "electronik:Resistor_Horizontal_RM6mm" V 8330 3600 50  0001 C CNN
F 3 "" H 8400 3600 50  0000 C CNN
	1    8400 3600
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5A8F3711
P 1050 6650
F 0 "#PWR?" H 1050 6400 50  0001 C CNN
F 1 "GND" H 1050 6500 50  0000 C CNN
F 2 "" H 1050 6650 50  0000 C CNN
F 3 "" H 1050 6650 50  0000 C CNN
	1    1050 6650
	-1   0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:R R118
U 1 1 5A90ADCF
P 12200 3300
F 0 "R118" V 12280 3300 50  0000 C CNN
F 1 "68" V 12200 3300 50  0000 C CNN
F 2 "electronik:Resistor_Horizontal_RM6mm" V 12130 3300 50  0001 C CNN
F 3 "" H 12200 3300 50  0000 C CNN
	1    12200 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	1050 7000 1200 7000
Wire Wire Line
	1150 6250 1200 6250
Connection ~ 2300 7000
Wire Wire Line
	1050 7000 1050 6850
Connection ~ 2300 6250
Wire Wire Line
	1150 6250 1150 6750
Connection ~ 3400 7000
Connection ~ 3050 7000
Connection ~ 2650 7000
Wire Wire Line
	1500 7000 2300 7000
Connection ~ 2650 6250
Connection ~ 3050 6250
Connection ~ 3400 6250
Wire Wire Line
	1500 6250 2300 6250
Connection ~ 13400 3600
Connection ~ 13400 3400
Wire Wire Line
	13650 3400 13400 3400
Connection ~ 13400 3500
Wire Wire Line
	13400 3500 13650 3500
Wire Wire Line
	13400 3600 13650 3600
Wire Wire Line
	13400 3200 13400 3400
Wire Wire Line
	13650 3200 13400 3200
Wire Wire Line
	8400 3350 8400 3450
Wire Wire Line
	9400 3150 9600 3150
Wire Wire Line
	1150 6750 1050 6750
Connection ~ 12000 3300
Wire Wire Line
	12000 2600 12000 3300
Wire Wire Line
	11700 2600 12000 2600
Connection ~ 11150 2600
Wire Wire Line
	11400 2600 11150 2600
Wire Wire Line
	11950 3300 12000 3300
Wire Wire Line
	11150 3200 11350 3200
Wire Wire Line
	11050 2600 11150 2600
$Comp
L output-amp-5voies-rescue:TL072 U101
U 1 1 5A9DA304
P 9900 3250
F 0 "U101" H 9850 3450 50  0000 L CNN
F 1 "TL072" H 9850 3000 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 9900 3250 50  0001 C CNN
F 3 "" H 9900 3250 50  0000 C CNN
	1    9900 3250
	1    0    0    1   
$EndComp
$Comp
L output-amp-5voies-rescue:TL072 U101
U 2 1 5A9DCCDE
P 11650 3300
F 0 "U101" H 11600 3500 50  0000 L CNN
F 1 "TL072" H 11600 3050 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 11650 3300 50  0001 C CNN
F 3 "" H 11650 3300 50  0000 C CNN
	2    11650 3300
	1    0    0    1   
$EndComp
Text GLabel 11550 3600 3    60   Input ~ 0
v+
Text GLabel 11550 3000 1    60   Input ~ 0
v-
$Comp
L output-amp-5voies-rescue:R R103
U 1 1 5A9D7C4B
P 10900 2600
F 0 "R103" V 10980 2600 50  0000 C CNN
F 1 "10k" V 10900 2600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm" V 10830 2600 50  0001 C CNN
F 3 "" H 10900 2600 50  0000 C CNN
	1    10900 2600
	0    1    1    0   
$EndComp
$Comp
L output-amp-5voies-rescue:R R109
U 1 1 5A9D85B4
P 11550 2600
F 0 "R109" V 11650 2600 50  0000 C CNN
F 1 "10k" V 11550 2600 50  0000 C CNN
F 2 "electronik:Resistor_Horizontal_RM6mm" V 11480 2600 50  0001 C CNN
F 3 "" H 11550 2600 50  0000 C CNN
	1    11550 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 7000 2650 7000
Wire Wire Line
	2300 6250 2650 6250
Wire Wire Line
	3400 7000 3750 7000
Wire Wire Line
	3050 7000 3400 7000
Wire Wire Line
	2650 7000 3050 7000
Wire Wire Line
	2650 6250 3050 6250
Wire Wire Line
	3050 6250 3400 6250
Wire Wire Line
	3400 6250 3750 6250
Wire Wire Line
	13400 3600 13400 3750
Wire Wire Line
	13400 3400 13400 3500
Wire Wire Line
	13400 3500 13400 3600
Wire Wire Line
	12000 3300 12050 3300
Wire Wire Line
	11150 2600 11150 3200
$Comp
L output-amp-5voies-rescue:GND #PWR?
U 1 1 5DABDDFC
P 11050 3400
F 0 "#PWR?" H 11050 3150 50  0001 C CNN
F 1 "GND" H 11050 3250 50  0000 C CNN
F 2 "" H 11050 3400 50  0000 C CNN
F 3 "" H 11050 3400 50  0000 C CNN
	1    11050 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	12350 3100 12350 3300
Wire Wire Line
	12350 3100 13650 3100
Wire Wire Line
	12600 3300 12600 4000
Wire Wire Line
	12600 4000 10300 4000
Wire Wire Line
	10300 4000 10300 3250
Wire Wire Line
	12900 3300 13650 3300
Wire Wire Line
	10200 3250 10300 3250
Wire Wire Line
	10300 3250 10300 2600
Wire Wire Line
	10300 2600 9400 2600
Wire Wire Line
	9400 2600 9400 3150
Connection ~ 10300 3250
Connection ~ 8400 3350
Wire Wire Line
	8400 3350 9600 3350
Wire Wire Line
	10300 2600 10750 2600
Connection ~ 10300 2600
$Comp
L output-amp-5voies-rescue:R C102
U 1 1 5D532C41
P 12750 3300
F 0 "C102" V 12830 3300 50  0000 C CNN
F 1 "68" V 12750 3300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 12680 3300 50  0001 C CNN
F 3 "" H 12750 3300 50  0000 C CNN
	1    12750 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 3350 8400 3350
$EndSCHEMATC
