EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 15700 850 
NoConn ~ 15700 1000
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C106
U 1 1 5967EF4B
P 8650 2850
F 0 "C106" H 8675 2950 50  0000 L CNN
F 1 "10u" H 8675 2750 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P100mm-pad2mm" H 8688 2700 50  0001 C CNN
F 3 "" H 8650 2850 50  0000 C CNN
	1    8650 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7250 3500 7250 2950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R107
U 1 1 5D2AE37A
P 7900 3500
F 0 "R107" V 7950 3500 50  0000 C CNN
F 1 "15k" V 7900 3500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7830 3500 50  0001 C CNN
F 3 "" H 7900 3500 50  0000 C CNN
	1    7900 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 3500 8500 3500
Wire Wire Line
	8500 3500 8500 2850
Wire Wire Line
	8500 2850 8300 2850
Wire Wire Line
	7250 3500 7750 3500
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R105
U 1 1 5D2AE37F
P 6800 3150
F 0 "R105" V 6880 3150 50  0000 C CNN
F 1 "100k" V 6800 3150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6730 3150 50  0001 C CNN
F 3 "" H 6800 3150 50  0000 C CNN
	1    6800 3150
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0101
U 1 1 5D2AE380
P 6800 3300
F 0 "#PWR0101" H 6800 3050 50  0001 C CNN
F 1 "GND" H 6800 3150 50  0000 C CNN
F 2 "" H 6800 3300 50  0000 C CNN
F 3 "" H 6800 3300 50  0000 C CNN
	1    6800 3300
	1    0    0    -1  
$EndComp
Connection ~ 8500 2850
NoConn ~ -2750 4600
Text GLabel 6750 8800 2    60   Input ~ 0
+v
Text GLabel 6750 9600 2    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR0102
U 1 1 5AA3E982
P 6350 3050
F 0 "#PWR0102" H 6350 2800 50  0001 C CNN
F 1 "GND" H 6350 2900 50  0000 C CNN
F 2 "" H 6350 3050 50  0000 C CNN
F 3 "" H 6350 3050 50  0000 C CNN
	1    6350 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 9600 5650 9600
Wire Wire Line
	5500 8800 5650 8800
Wire Wire Line
	6100 8800 6100 8850
Wire Wire Line
	5500 9000 5500 9600
Wire Wire Line
	5300 9000 5500 9000
Wire Wire Line
	5300 8900 5500 8900
Wire Wire Line
	5500 8900 5500 8800
Wire Wire Line
	5300 8800 5300 8750
Wire Wire Line
	6100 9150 6100 9600
Wire Wire Line
	6800 3000 6800 2950
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J103
U 1 1 5D3F56DE
P 6150 3050
F 0 "J103" H 6150 3150 50  0000 C CNN
F 1 "inL" H 6500 3000 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 6150 3050 50  0001 C CNN
F 3 "" H 6150 3050 50  0001 C CNN
	1    6150 3050
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0104
U 1 1 5D2ED105
P 7700 2750
F 0 "#PWR0104" H 7700 2500 50  0001 C CNN
F 1 "GND" H 7700 2600 50  0000 C CNN
F 2 "" H 7700 2750 50  0000 C CNN
F 3 "" H 7700 2750 50  0000 C CNN
	1    7700 2750
	0    1    1    0   
$EndComp
Connection ~ 6800 2950
Wire Wire Line
	6800 2950 7250 2950
Connection ~ 7250 2950
Wire Wire Line
	7250 2950 7700 2950
Wire Wire Line
	6350 2950 6450 2950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R111
U 1 1 596B6EA9
P 10350 3400
F 0 "R111" V 10430 3400 50  0000 C CNN
F 1 "10k" V 10350 3400 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 10280 3400 50  0001 C CNN
F 3 "" H 10350 3400 50  0000 C CNN
	1    10350 3400
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R113
U 1 1 596B8C58
P 11250 3600
F 0 "R113" V 11330 3600 50  0000 C CNN
F 1 "75" V 11250 3600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 11180 3600 50  0001 C CNN
F 3 "" H 11250 3600 50  0000 C CNN
	1    11250 3600
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0105
U 1 1 596B8C5E
P 11100 3600
F 0 "#PWR0105" H 11100 3350 50  0001 C CNN
F 1 "GND" H 11100 3450 50  0000 C CNN
F 2 "" H 11100 3600 50  0000 C CNN
F 3 "" H 11100 3600 50  0000 C CNN
	1    11100 3600
	0    1    1    0   
$EndComp
Text Label 11250 3400 0    60   ~ 0
AUX1
Text Label 11350 4000 0    60   ~ 0
AUX2
Text Label 11500 3600 0    60   ~ 0
GND-AUX
Wire Wire Line
	9100 3250 9850 3250
Wire Wire Line
	11400 3600 11950 3600
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0106
U 1 1 5AE70E17
P 9850 3550
F 0 "#PWR0106" H 9850 3300 50  0001 C CNN
F 1 "GND" H 9850 3400 50  0000 C CNN
F 2 "" H 9850 3550 50  0000 C CNN
F 3 "" H 9850 3550 50  0000 C CNN
	1    9850 3550
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV101
U 1 1 5C315B55
P 9850 3400
F 0 "RV101" V 9675 3400 50  0000 C CNN
F 1 "FX1-10K" V 9750 3400 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 9850 3400 50  0001 C CNN
F 3 "" H 9850 3400 50  0001 C CNN
	1    9850 3400
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R112
U 1 1 5C317698
P 10400 4000
F 0 "R112" V 10480 4000 50  0000 C CNN
F 1 "10k" V 10400 4000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 10330 4000 50  0001 C CNN
F 3 "" H 10400 4000 50  0000 C CNN
	1    10400 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9100 3850 9850 3850
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0107
U 1 1 5C3176A4
P 9850 4150
F 0 "#PWR0107" H 9850 3900 50  0001 C CNN
F 1 "GND" H 9850 4000 50  0000 C CNN
F 2 "" H 9850 4150 50  0000 C CNN
F 3 "" H 9850 4150 50  0000 C CNN
	1    9850 4150
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV102
U 1 1 5C3176AA
P 9850 4000
F 0 "RV102" V 9675 4000 50  0000 C CNN
F 1 "FX2-10K" V 9750 4000 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 9850 4000 50  0001 C CNN
F 3 "" H 9850 4000 50  0001 C CNN
	1    9850 4000
	1    0    0    1   
$EndComp
Wire Wire Line
	10000 3400 10100 3400
Wire Wire Line
	10000 4000 10100 4000
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C101
U 1 1 5CCEB1B0
P 6100 9000
F 0 "C101" H 6125 9100 50  0000 L CNN
F 1 "100n" H 6125 8900 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6138 8850 50  0001 C CNN
F 3 "" H 6100 9000 50  0000 C CNN
	1    6100 9000
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R102
U 1 1 5D2AE37C
P 5800 8800
F 0 "R102" V 5880 8800 50  0000 C CNN
F 1 "2R2" V 5800 8800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5730 8800 50  0001 C CNN
F 3 "" H 5800 8800 50  0000 C CNN
	1    5800 8800
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R103
U 1 1 5CCEB1B3
P 5800 9600
F 0 "R103" V 5880 9600 50  0000 C CNN
F 1 "2R2" V 5800 9600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5730 9600 50  0001 C CNN
F 3 "" H 5800 9600 50  0000 C CNN
	1    5800 9600
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue J102
U 1 1 5D2AE37E
P 5100 8900
F 0 "J102" H 5100 9100 50  0000 C CNN
F 1 "alim" H 5100 8700 50  0000 C CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 5100 8900 50  0001 C CNN
F 3 "" H 5100 8900 50  0001 C CNN
	1    5100 8900
	-1   0    0    1   
$EndComp
NoConn ~ 11300 1750
Wire Wire Line
	8800 2850 9100 2850
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J106
U 1 1 5D2AE379
P 10250 2850
F 0 "J106" H 10250 2950 50  0000 C CNN
F 1 "MASTER-BUS" H 10600 2800 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 10250 2850 50  0001 C CNN
F 3 "" H 10250 2850 50  0001 C CNN
	1    10250 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 2950 10050 2950
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0109
U 1 1 5D2AE374
P 9700 2950
F 0 "#PWR0109" H 9700 2700 50  0001 C CNN
F 1 "GND" H 9700 2800 50  0000 C CNN
F 2 "" H 9700 2950 50  0000 C CNN
F 3 "" H 9700 2950 50  0000 C CNN
	1    9700 2950
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R109
U 1 1 5D2AE373
P 9850 2950
F 0 "R109" V 9930 2950 50  0000 C CNN
F 1 "75" V 9850 2950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9780 2950 50  0001 C CNN
F 3 "" H 9850 2950 50  0000 C CNN
	1    9850 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9100 2850 9100 3250
Connection ~ 9100 2850
Wire Wire Line
	9100 2850 10050 2850
Connection ~ 9100 3250
Wire Wire Line
	9100 3250 9100 3850
NoConn ~ 7700 4300
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C107
U 1 1 5D2B96AD
P 8650 4550
F 0 "C107" H 8675 4650 50  0000 L CNN
F 1 "10u" H 8675 4450 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P100mm-pad2mm" H 8688 4400 50  0001 C CNN
F 3 "" H 8650 4550 50  0000 C CNN
	1    8650 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7250 5200 7250 4650
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R108
U 1 1 5D2B96BA
P 7900 5200
F 0 "R108" V 7950 5200 50  0000 C CNN
F 1 "15k" V 7900 5200 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7830 5200 50  0001 C CNN
F 3 "" H 7900 5200 50  0000 C CNN
	1    7900 5200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 5200 8500 5200
Wire Wire Line
	8500 5200 8500 4550
Wire Wire Line
	8500 4550 8300 4550
Wire Wire Line
	7250 5200 7750 5200
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R106
U 1 1 5D2B96C4
P 6800 4850
F 0 "R106" V 6880 4850 50  0000 C CNN
F 1 "100k" V 6800 4850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6730 4850 50  0001 C CNN
F 3 "" H 6800 4850 50  0000 C CNN
	1    6800 4850
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0110
U 1 1 5D2B96CA
P 6800 5000
F 0 "#PWR0110" H 6800 4750 50  0001 C CNN
F 1 "GND" H 6800 4850 50  0000 C CNN
F 2 "" H 6800 5000 50  0000 C CNN
F 3 "" H 6800 5000 50  0000 C CNN
	1    6800 5000
	1    0    0    -1  
$EndComp
Connection ~ 8500 4550
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR0111
U 1 1 5D2B96D1
P 6350 4750
F 0 "#PWR0111" H 6350 4500 50  0001 C CNN
F 1 "GND" H 6350 4600 50  0000 C CNN
F 2 "" H 6350 4750 50  0000 C CNN
F 3 "" H 6350 4750 50  0000 C CNN
	1    6350 4750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 4700 6800 4650
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J105
U 1 1 5D2B96D8
P 6150 4750
F 0 "J105" H 6150 4850 50  0000 C CNN
F 1 "inR" H 6500 4700 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 6150 4750 50  0001 C CNN
F 3 "" H 6150 4750 50  0001 C CNN
	1    6150 4750
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0112
U 1 1 5D2B96DE
P 7700 4450
F 0 "#PWR0112" H 7700 4200 50  0001 C CNN
F 1 "GND" H 7700 4300 50  0000 C CNN
F 2 "" H 7700 4450 50  0000 C CNN
F 3 "" H 7700 4450 50  0000 C CNN
	1    7700 4450
	0    1    1    0   
$EndComp
Connection ~ 6800 4650
Wire Wire Line
	6800 4650 7250 4650
Connection ~ 7250 4650
Wire Wire Line
	7250 4650 7700 4650
Wire Wire Line
	6350 4650 6450 4650
NoConn ~ 11300 3450
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J107
U 1 1 5D2B9743
P 10250 4550
F 0 "J107" H 10250 4650 50  0000 C CNN
F 1 "EFFECT BUS" H 10600 4500 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 10250 4550 50  0001 C CNN
F 3 "" H 10250 4550 50  0001 C CNN
	1    10250 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4650 10050 4650
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0113
U 1 1 5D2B974A
P 9700 4650
F 0 "#PWR0113" H 9700 4400 50  0001 C CNN
F 1 "GND" H 9700 4500 50  0000 C CNN
F 2 "" H 9700 4650 50  0000 C CNN
F 3 "" H 9700 4650 50  0000 C CNN
	1    9700 4650
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R110
U 1 1 5D2B9750
P 9850 4650
F 0 "R110" V 9930 4650 50  0000 C CNN
F 1 "75" V 9850 4650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9780 4650 50  0001 C CNN
F 3 "" H 9850 4650 50  0000 C CNN
	1    9850 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8800 4550 10050 4550
$Comp
L Device:Jumper_NC_Small JP101
U 1 1 5D2AFEBF
P 10100 3550
F 0 "JP101" V 10054 3624 50  0000 L CNN
F 1 "Jumper_NC_Small" V 10145 3624 50  0000 L CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 10100 3550 50  0001 C CNN
F 3 "~" H 10100 3550 50  0001 C CNN
	1    10100 3550
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP102
U 1 1 5D2AFFD1
P 10100 4150
F 0 "JP102" V 10054 4224 50  0000 L CNN
F 1 "Jumper_NC_Small" V 10145 4224 50  0000 L CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 10100 4150 50  0001 C CNN
F 3 "~" H 10100 4150 50  0001 C CNN
	1    10100 4150
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0114
U 1 1 5D2B0039
P 10100 4250
F 0 "#PWR0114" H 10100 4000 50  0001 C CNN
F 1 "GND" H 10100 4100 50  0000 C CNN
F 2 "" H 10100 4250 50  0000 C CNN
F 3 "" H 10100 4250 50  0000 C CNN
	1    10100 4250
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0115
U 1 1 5D2B00D0
P 10100 3650
F 0 "#PWR0115" H 10100 3400 50  0001 C CNN
F 1 "GND" H 10100 3500 50  0000 C CNN
F 2 "" H 10100 3650 50  0000 C CNN
F 3 "" H 10100 3650 50  0000 C CNN
	1    10100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 3450 10100 3400
Connection ~ 10100 3400
Wire Wire Line
	10100 3400 10200 3400
Wire Wire Line
	10100 4050 10100 4000
Connection ~ 10100 4000
Wire Wire Line
	10100 4000 10250 4000
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C102
U 1 1 5D2B21AB
P 6600 2950
F 0 "C102" H 6625 3050 50  0000 L CNN
F 1 "10u" H 6625 2850 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P100mm-pad2mm" H 6638 2800 50  0001 C CNN
F 3 "" H 6600 2950 50  0000 C CNN
	1    6600 2950
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C103
U 1 1 5D2B21B1
P 6600 4650
F 0 "C103" H 6625 4750 50  0000 L CNN
F 1 "10u" H 6625 4550 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P100mm-pad2mm" H 6638 4500 50  0001 C CNN
F 3 "" H 6600 4650 50  0000 C CNN
	1    6600 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6750 2950 6800 2950
Wire Wire Line
	6750 4650 6800 4650
$Comp
L eq3tone-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue J1
U 1 1 606862F0
P 12150 3500
F 0 "J1" H 12150 3700 50  0000 C CNN
F 1 "fx-outputs" H 12150 3300 50  0000 C CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 12150 3500 50  0001 C CNN
F 3 "" H 12150 3500 50  0001 C CNN
	1    12150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 3400 11950 3400
Wire Wire Line
	11950 3500 11850 3500
Wire Wire Line
	11850 3500 11850 4000
Wire Wire Line
	10550 4000 11850 4000
Wire Wire Line
	6750 9600 6600 9600
$Comp
L Amplifier_Operational:TL072 U1
U 1 1 606E728A
P 8000 2850
F 0 "U1" H 8000 3217 50  0000 C CNN
F 1 "TL072" H 8000 3126 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 8000 2850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 8000 2850 50  0001 C CNN
	1    8000 2850
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U1
U 2 1 606EB5A5
P 8000 4550
F 0 "U1" H 8000 4917 50  0000 C CNN
F 1 "TL072" H 8000 4826 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 8000 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 8000 4550 50  0001 C CNN
	2    8000 4550
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U1
U 3 1 606ED8C1
P 6700 9150
F 0 "U1" H 6512 9104 50  0000 R CNN
F 1 "TL072" H 6512 9195 50  0000 R CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6700 9150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 6700 9150 50  0001 C CNN
	3    6700 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 8850 6600 8800
Wire Wire Line
	6600 9450 6600 9600
Connection ~ 6600 9600
Wire Wire Line
	5950 9600 6100 9600
Wire Wire Line
	5950 8800 6100 8800
Connection ~ 6100 8800
Connection ~ 6100 9600
Wire Wire Line
	6100 8800 6600 8800
Wire Wire Line
	6100 9600 6600 9600
Wire Wire Line
	6750 8800 6600 8800
Connection ~ 6600 8800
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0103
U 1 1 60701C85
P 5300 8750
F 0 "#PWR0103" H 5300 8500 50  0001 C CNN
F 1 "GND" H 5300 8600 50  0000 C CNN
F 2 "" H 5300 8750 50  0000 C CNN
F 3 "" H 5300 8750 50  0000 C CNN
	1    5300 8750
	-1   0    0    1   
$EndComp
$EndSCHEMATC
