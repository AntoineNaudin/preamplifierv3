EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R12
U 1 1 5968A4C6
P 7350 3150
F 0 "R12" V 7430 3150 50  0000 C CNN
F 1 "10k" V 7350 3150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7280 3150 50  0001 C CNN
F 3 "" H 7350 3150 50  0000 C CNN
	1    7350 3150
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R15
U 1 1 5968AD4B
P 8050 5250
F 0 "R15" V 8130 5250 50  0000 C CNN
F 1 "75" V 8050 5250 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7980 5250 50  0001 C CNN
F 3 "" H 8050 5250 50  0000 C CNN
	1    8050 5250
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0101
U 1 1 5968AF92
P 7900 5250
F 0 "#PWR0101" H 7900 5000 50  0001 C CNN
F 1 "GND" H 7900 5100 50  0000 C CNN
F 2 "" H 7900 5250 50  0000 C CNN
F 3 "" H 7900 5250 50  0000 C CNN
	1    7900 5250
	0    1    1    0   
$EndComp
Text Label 8450 5150 0    60   ~ 0
MAS
Text Label 8300 5250 0    60   ~ 0
GND-MAS
NoConn ~ 15700 850 
NoConn ~ 15700 1000
Wire Wire Line
	8200 5250 8750 5250
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J3
U 1 1 5C314EF7
P 8950 5150
F 0 "J3" H 8950 5250 50  0000 C CNN
F 1 "MASTER-BUS" H 9300 5100 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 8950 5150 50  0001 C CNN
F 3 "" H 8950 5150 50  0001 C CNN
	1    8950 5150
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R5
U 1 1 5CCEC1EC
P 4200 2050
F 0 "R5" V 4280 2050 50  0000 C CNN
F 1 "100k" V 4200 2050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4130 2050 50  0001 C CNN
F 3 "" H 4200 2050 50  0000 C CNN
	1    4200 2050
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0102
U 1 1 5CCEC1F2
P 4200 2200
F 0 "#PWR0102" H 4200 1950 50  0001 C CNN
F 1 "GND" H 4200 2050 50  0000 C CNN
F 2 "" H 4200 2200 50  0000 C CNN
F 3 "" H 4200 2200 50  0000 C CNN
	1    4200 2200
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV5
U 1 1 5CD2D3A4
P 4550 2000
F 0 "RV5" V 4375 2000 50  0000 C CNN
F 1 "volume" V 4450 2000 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 4550 2000 50  0001 C CNN
F 3 "" H 4550 2000 50  0001 C CNN
	1    4550 2000
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0103
U 1 1 5CD31BB1
P 4550 2150
F 0 "#PWR0103" H 4550 1900 50  0001 C CNN
F 1 "GND" H 4550 2000 50  0000 C CNN
F 2 "" H 4550 2150 50  0000 C CNN
F 3 "" H 4550 2150 50  0000 C CNN
	1    4550 2150
	1    0    0    -1  
$EndComp
NoConn ~ -2750 4600
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C15
U 1 1 5CCEB1B0
P 6300 9000
F 0 "C15" H 6325 9100 50  0000 L CNN
F 1 "100n" H 6325 8900 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6338 8850 50  0001 C CNN
F 3 "" H 6300 9000 50  0000 C CNN
	1    6300 9000
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R1
U 1 1 5CCEB1B2
P 5800 8800
F 0 "R1" V 5880 8800 50  0000 C CNN
F 1 "2R2" V 5800 8800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5730 8800 50  0001 C CNN
F 3 "" H 5800 8800 50  0000 C CNN
	1    5800 8800
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R2
U 1 1 5CCEB1B3
P 5800 9600
F 0 "R2" V 5880 9600 50  0000 C CNN
F 1 "2R2" V 5800 9600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5730 9600 50  0001 C CNN
F 3 "" H 5800 9600 50  0000 C CNN
	1    5800 9600
	0    1    1    0   
$EndComp
Text GLabel 6600 8800 2    60   Input ~ 0
+v
Text GLabel 6600 9600 2    60   Input ~ 0
-v
Text Label 6450 8800 0    60   ~ 0
+16v
Text Label 6450 9600 0    60   ~ 0
-16v
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR0104
U 1 1 5AA3E982
P 3750 1950
F 0 "#PWR0104" H 3750 1700 50  0001 C CNN
F 1 "GND" H 3750 1800 50  0000 C CNN
F 2 "" H 3750 1950 50  0000 C CNN
F 3 "" H 3750 1950 50  0000 C CNN
	1    3750 1950
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG0101
U 1 1 5AA41643
P 5650 9200
F 0 "#FLG0101" H 5650 9295 50  0001 C CNN
F 1 "PWR_FLAG" H 5650 9380 50  0000 C CNN
F 2 "" H 5650 9200 50  0000 C CNN
F 3 "" H 5650 9200 50  0000 C CNN
	1    5650 9200
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG0102
U 1 1 5AA41910
P 6150 8750
F 0 "#FLG0102" H 6150 8845 50  0001 C CNN
F 1 "PWR_FLAG" H 6150 8930 50  0000 C CNN
F 2 "" H 6150 8750 50  0000 C CNN
F 3 "" H 6150 8750 50  0000 C CNN
	1    6150 8750
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG0103
U 1 1 5CD30825
P 6100 9600
F 0 "#FLG0103" H 6100 9695 50  0001 C CNN
F 1 "PWR_FLAG" H 6100 9780 50  0000 C CNN
F 2 "" H 6100 9600 50  0000 C CNN
F 3 "" H 6100 9600 50  0000 C CNN
	1    6100 9600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 9600 5650 9600
Wire Wire Line
	5500 8800 5650 8800
Wire Wire Line
	6300 8800 6300 8850
Wire Wire Line
	5950 8800 6150 8800
Wire Wire Line
	5500 9000 5500 9600
Wire Wire Line
	6150 8750 6150 8800
Wire Wire Line
	5650 8900 5650 9200
Connection ~ 5650 9200
Wire Wire Line
	5300 9000 5500 9000
Wire Wire Line
	5300 8900 5500 8900
Wire Wire Line
	5500 8900 5500 8800
Wire Wire Line
	5300 8800 5300 8750
Wire Wire Line
	5300 8750 5600 8750
Wire Wire Line
	5600 8750 5600 8900
Wire Wire Line
	5600 8900 5650 8900
Wire Wire Line
	6300 9150 6300 9600
Wire Wire Line
	6300 3500 6350 3500
Wire Wire Line
	6350 2800 6350 2850
Wire Wire Line
	6300 2800 6350 2800
Wire Wire Line
	6350 3500 6350 3450
Text GLabel 6300 2800 0    60   Input ~ 0
+v
Text GLabel 6300 3500 0    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue U1
U 1 1 5CD660AD
P 6450 3150
F 0 "U1" H 6400 3350 50  0000 L CNN
F 1 "ne5532" H 6400 2900 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6450 3150 50  0001 C CNN
F 3 "" H 6450 3150 50  0000 C CNN
	1    6450 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 9600 6300 9600
Connection ~ 6150 8800
Wire Wire Line
	6150 8800 6300 8800
$Comp
L eq3tone-rescue:GNDD-power-riaav3-rescue #PWR0105
U 1 1 5CCFA5FC
P 5650 9200
F 0 "#PWR0105" H 5650 8950 50  0001 C CNN
F 1 "GNDD-power" H 5654 9045 50  0000 C CNN
F 2 "" H 5650 9200 50  0001 C CNN
F 3 "" H 5650 9200 50  0001 C CNN
	1    5650 9200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3050 6150 3050
Wire Wire Line
	5950 3500 5950 3250
Wire Wire Line
	5400 3500 5950 3500
Wire Wire Line
	5950 3250 6150 3250
Connection ~ 5950 3500
Wire Wire Line
	6150 4000 5950 4000
Wire Wire Line
	6800 4000 6800 3700
Wire Wire Line
	6450 4000 6800 4000
Wire Wire Line
	5950 4000 5950 3700
Wire Wire Line
	5950 3700 5950 3500
Connection ~ 5950 3700
Wire Wire Line
	6150 3700 5950 3700
Wire Wire Line
	6800 3700 6450 3700
Connection ~ 6800 3700
Wire Wire Line
	6800 3150 6750 3150
Connection ~ 6800 3150
Wire Wire Line
	6800 3150 6800 3700
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R28
U 1 1 5CD660BA
P 6300 4000
F 0 "R28" V 6350 4000 50  0000 C CNN
F 1 "10k" V 6300 4000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6230 4000 50  0001 C CNN
F 3 "" H 6300 4000 50  0000 C CNN
	1    6300 4000
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C13
U 1 1 5CD660B4
P 6300 3700
F 0 "C13" H 6325 3800 50  0000 L CNN
F 1 "22p" H 6325 3600 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6338 3550 50  0001 C CNN
F 3 "" H 6300 3700 50  0000 C CNN
	1    6300 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 2000 5500 3050
Wire Wire Line
	5100 3500 5050 3500
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R26
U 1 1 5CD379BE
P 5250 3500
F 0 "R26" V 5300 3500 50  0000 C CNN
F 1 "10k" V 5250 3500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5180 3500 50  0001 C CNN
F 3 "" H 5250 3500 50  0000 C CNN
	1    5250 3500
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0106
U 1 1 5D2E8726
P 5050 3500
F 0 "#PWR0106" H 5050 3250 50  0001 C CNN
F 1 "GND" H 5050 3350 50  0000 C CNN
F 2 "" H 5050 3500 50  0000 C CNN
F 3 "" H 5050 3500 50  0000 C CNN
	1    5050 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 1850 4200 1850
Wire Wire Line
	5350 2000 5500 2000
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R25
U 1 1 5CD00D3A
P 5200 2000
F 0 "R25" V 5250 2000 50  0000 C CNN
F 1 "220" V 5200 2000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM12mm-PAS2MM" V 5130 2000 50  0001 C CNN
F 3 "" H 5200 2000 50  0000 C CNN
	1    5200 2000
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C101
U 1 1 5D335913
P 3900 1850
F 0 "C101" V 4155 1850 50  0000 C CNN
F 1 "10u" V 3750 2200 50  0000 C CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 3938 1700 50  0001 C CNN
F 3 "" H 3900 1850 50  0001 C CNN
	1    3900 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 2000 4850 2000
Wire Wire Line
	4200 1900 4200 1850
Connection ~ 4200 1850
Wire Wire Line
	4200 1850 4550 1850
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R103
U 1 1 5D38AE5C
P 4850 2150
F 0 "R103" V 4930 2150 50  0000 C CNN
F 1 "15k" V 4850 2150 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4780 2150 50  0001 C CNN
F 3 "" H 4850 2150 50  0000 C CNN
	1    4850 2150
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0107
U 1 1 5D38AE62
P 4850 2300
F 0 "#PWR0107" H 4850 2050 50  0001 C CNN
F 1 "GND" H 4850 2150 50  0000 C CNN
F 2 "" H 4850 2300 50  0000 C CNN
F 3 "" H 4850 2300 50  0000 C CNN
	1    4850 2300
	1    0    0    -1  
$EndComp
Connection ~ 4850 2000
Wire Wire Line
	4850 2000 5050 2000
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R101
U 1 1 5D38EDF4
P 4050 6000
F 0 "R101" V 4130 6000 50  0000 C CNN
F 1 "100k" V 4050 6000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3980 6000 50  0001 C CNN
F 3 "" H 4050 6000 50  0000 C CNN
	1    4050 6000
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0108
U 1 1 5D38EDFA
P 4050 6150
F 0 "#PWR0108" H 4050 5900 50  0001 C CNN
F 1 "GND" H 4050 6000 50  0000 C CNN
F 2 "" H 4050 6150 50  0000 C CNN
F 3 "" H 4050 6150 50  0000 C CNN
	1    4050 6150
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV101
U 1 1 5D38EE00
P 4400 5950
F 0 "RV101" V 4225 5950 50  0000 C CNN
F 1 "volume" V 4300 5950 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 4400 5950 50  0001 C CNN
F 3 "" H 4400 5950 50  0001 C CNN
	1    4400 5950
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0109
U 1 1 5D38EE06
P 4400 6100
F 0 "#PWR0109" H 4400 5850 50  0001 C CNN
F 1 "GND" H 4400 5950 50  0000 C CNN
F 2 "" H 4400 6100 50  0000 C CNN
F 3 "" H 4400 6100 50  0000 C CNN
	1    4400 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 7450 6200 7450
Wire Wire Line
	6200 6750 6200 6800
Wire Wire Line
	6150 6750 6200 6750
Wire Wire Line
	6200 7450 6200 7400
Text GLabel 6150 6750 0    60   Input ~ 0
+v
Text GLabel 6150 7450 0    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue U1
U 2 1 5D38EE1E
P 6300 7100
F 0 "U1" H 6250 7300 50  0000 L CNN
F 1 "ne5532" H 6250 6850 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6300 7100 50  0001 C CNN
F 3 "" H 6300 7100 50  0000 C CNN
	2    6300 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 7000 6000 7000
Wire Wire Line
	5800 7450 5800 7200
Wire Wire Line
	5250 7450 5800 7450
Wire Wire Line
	5800 7200 6000 7200
Connection ~ 5800 7450
Wire Wire Line
	6000 7950 5800 7950
Wire Wire Line
	6650 7950 6650 7650
Wire Wire Line
	6300 7950 6650 7950
Wire Wire Line
	5800 7950 5800 7650
Wire Wire Line
	5800 7650 5800 7450
Connection ~ 5800 7650
Wire Wire Line
	6000 7650 5800 7650
Wire Wire Line
	6650 7650 6300 7650
Connection ~ 6650 7650
Wire Wire Line
	6650 7100 6600 7100
Connection ~ 6650 7100
Wire Wire Line
	6650 7100 6650 7650
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R106
U 1 1 5D38EE35
P 6150 7950
F 0 "R106" V 6200 7950 50  0000 C CNN
F 1 "10k" V 6150 7950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6080 7950 50  0001 C CNN
F 3 "" H 6150 7950 50  0000 C CNN
	1    6150 7950
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C103
U 1 1 5D38EE3B
P 6150 7650
F 0 "C103" H 6175 7750 50  0000 L CNN
F 1 "22p" H 6175 7550 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 6188 7500 50  0001 C CNN
F 3 "" H 6150 7650 50  0000 C CNN
	1    6150 7650
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 5950 5350 7000
Wire Wire Line
	4950 7450 4900 7450
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R105
U 1 1 5D38EE43
P 5100 7450
F 0 "R105" V 5150 7450 50  0000 C CNN
F 1 "10k" V 5100 7450 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5030 7450 50  0001 C CNN
F 3 "" H 5100 7450 50  0000 C CNN
	1    5100 7450
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0110
U 1 1 5D38EE49
P 4900 7450
F 0 "#PWR0110" H 4900 7200 50  0001 C CNN
F 1 "GND" H 4900 7300 50  0000 C CNN
F 2 "" H 4900 7450 50  0000 C CNN
F 3 "" H 4900 7450 50  0000 C CNN
	1    4900 7450
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 5800 4050 5800
Wire Wire Line
	5200 5950 5350 5950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R104
U 1 1 5D38EE51
P 5050 5950
F 0 "R104" V 5100 5950 50  0000 C CNN
F 1 "220" V 5050 5950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM12mm-PAS2MM" V 4980 5950 50  0001 C CNN
F 3 "" H 5050 5950 50  0000 C CNN
	1    5050 5950
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C102
U 1 1 5D38EE57
P 3750 5800
F 0 "C102" V 4005 5800 50  0000 C CNN
F 1 "10u" V 3600 6150 50  0000 C CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 3788 5650 50  0001 C CNN
F 3 "" H 3750 5800 50  0001 C CNN
	1    3750 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 5950 4700 5950
Wire Wire Line
	4050 5850 4050 5800
Connection ~ 4050 5800
Wire Wire Line
	4050 5800 4400 5800
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R102
U 1 1 5D38EE62
P 4700 6100
F 0 "R102" V 4780 6100 50  0000 C CNN
F 1 "15k" V 4700 6100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4630 6100 50  0001 C CNN
F 3 "" H 4700 6100 50  0000 C CNN
	1    4700 6100
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0111
U 1 1 5D38EE68
P 4700 6250
F 0 "#PWR0111" H 4700 6000 50  0001 C CNN
F 1 "GND" H 4700 6100 50  0000 C CNN
F 2 "" H 4700 6250 50  0000 C CNN
F 3 "" H 4700 6250 50  0000 C CNN
	1    4700 6250
	1    0    0    -1  
$EndComp
Connection ~ 4700 5950
Wire Wire Line
	4700 5950 4900 5950
Wire Wire Line
	3300 5800 3600 5800
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R107
U 1 1 5D3B8331
P 7400 7100
F 0 "R107" V 7480 7100 50  0000 C CNN
F 1 "10k" V 7400 7100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 7330 7100 50  0001 C CNN
F 3 "" H 7400 7100 50  0000 C CNN
	1    7400 7100
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J6
U 1 1 5D3F56DE
P 3550 1950
F 0 "J6" H 3550 2050 50  0000 C CNN
F 1 "inL" H 3900 1900 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 3550 1950 50  0001 C CNN
F 3 "" H 3550 1950 50  0001 C CNN
	1    3550 1950
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J101
U 1 1 5D3FF933
P 3100 5900
F 0 "J101" H 3100 6000 50  0000 C CNN
F 1 "inR" H 3450 5850 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 3100 5900 50  0001 C CNN
F 3 "" H 3100 5900 50  0001 C CNN
	1    3100 5900
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR0112
U 1 1 5D3FFF0D
P 3300 5900
F 0 "#PWR0112" H 3300 5650 50  0001 C CNN
F 1 "GND" H 3300 5750 50  0000 C CNN
F 2 "" H 3300 5900 50  0000 C CNN
F 3 "" H 3300 5900 50  0000 C CNN
	1    3300 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 7100 7600 7100
Wire Wire Line
	7500 3150 7600 3150
Wire Wire Line
	7600 3150 7600 5150
Connection ~ 7600 5150
Wire Wire Line
	7600 5150 7600 7100
Wire Wire Line
	7600 5150 8750 5150
Wire Wire Line
	5950 9600 6100 9600
Connection ~ 6100 9600
Connection ~ 6300 9600
Wire Wire Line
	6300 9600 6600 9600
Wire Wire Line
	6600 8800 6300 8800
Connection ~ 6300 8800
Wire Wire Line
	6800 3150 7200 3150
Wire Wire Line
	6650 7100 7250 7100
$Comp
L eq3tone-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue J1
U 1 1 5CCEB1BC
P 5100 8900
F 0 "J1" H 5100 9100 50  0000 C CNN
F 1 "alim" H 5100 8700 50  0000 C CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 5100 8900 50  0001 C CNN
F 3 "" H 5100 8900 50  0001 C CNN
	1    5100 8900
	-1   0    0    1   
$EndComp
$EndSCHEMATC
