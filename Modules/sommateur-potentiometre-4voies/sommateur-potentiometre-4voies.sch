EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 15700 850 
NoConn ~ 15700 1000
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C3
U 1 1 5967EF4B
P 1400 2100
F 0 "C3" H 1425 2200 50  0000 L CNN
F 1 "10u" H 1425 2000 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 1438 1950 50  0001 C CNN
F 3 "" H 1400 2100 50  0000 C CNN
	1    1400 2100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 2950 6050 2400
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R12
U 1 1 5D2AE37A
P 6700 2950
F 0 "R12" V 6750 2950 50  0000 C CNN
F 1 "20k" V 6700 2950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6630 2950 50  0001 C CNN
F 3 "" H 6700 2950 50  0000 C CNN
	1    6700 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 2950 7300 2950
Wire Wire Line
	7300 2950 7300 2300
Wire Wire Line
	7300 2300 7100 2300
Wire Wire Line
	6050 2950 6550 2950
NoConn ~ 1550 4550
Text GLabel 6800 8750 1    60   Input ~ 0
+v
Text GLabel 6800 9600 3    60   Input ~ 0
-v
Text Label 6450 8800 0    60   ~ 0
+16v
Text Label 6450 9600 0    60   ~ 0
-16v
Wire Wire Line
	5500 9600 5650 9600
Wire Wire Line
	5950 9600 6100 9600
Wire Wire Line
	6600 8800 6600 8850
Wire Wire Line
	5500 8900 5500 9600
Wire Wire Line
	6100 9600 6100 9700
Connection ~ 6100 9600
Wire Wire Line
	5300 8900 5500 8900
Wire Wire Line
	5300 8700 5300 8650
Wire Wire Line
	5300 8650 5350 8650
Wire Wire Line
	6300 9150 6300 9600
Wire Wire Line
	6600 9150 6600 9600
Connection ~ 6300 9600
Wire Wire Line
	6300 9600 6600 9600
Wire Wire Line
	6100 9600 6300 9600
Connection ~ 6600 9600
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0101
U 1 1 5D2ED105
P 6500 2200
F 0 "#PWR0101" H 6500 1950 50  0001 C CNN
F 1 "GND" H 6500 2050 50  0000 C CNN
F 2 "" H 6500 2200 50  0000 C CNN
F 3 "" H 6500 2200 50  0000 C CNN
	1    6500 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 2400 6500 2400
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R3
U 1 1 596B6EA9
P 3200 2350
F 0 "R3" V 3280 2350 50  0000 C CNN
F 1 "10k" V 3200 2350 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3130 2350 50  0001 C CNN
F 3 "" H 3200 2350 50  0000 C CNN
	1    3200 2350
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0102
U 1 1 5AE70E17
P 2700 2500
F 0 "#PWR0102" H 2700 2250 50  0001 C CNN
F 1 "GND" H 2700 2350 50  0000 C CNN
F 2 "" H 2700 2500 50  0000 C CNN
F 3 "" H 2700 2500 50  0000 C CNN
	1    2700 2500
	1    0    0    -1  
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:POT-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue RV1
U 1 1 5C315B55
P 2700 2350
F 0 "RV1" V 2525 2350 50  0000 C CNN
F 1 "SUB-20K" V 2600 2350 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 2700 2350 50  0001 C CNN
F 3 "" H 2700 2350 50  0001 C CNN
	1    2700 2350
	1    0    0    1   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R7
U 1 1 5C317698
P 3250 2950
F 0 "R7" V 3330 2950 50  0000 C CNN
F 1 "10k" V 3250 2950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3180 2950 50  0001 C CNN
F 3 "" H 3250 2950 50  0000 C CNN
	1    3250 2950
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0103
U 1 1 5C3176A4
P 2700 3100
F 0 "#PWR0103" H 2700 2850 50  0001 C CNN
F 1 "GND" H 2700 2950 50  0000 C CNN
F 2 "" H 2700 3100 50  0000 C CNN
F 3 "" H 2700 3100 50  0000 C CNN
	1    2700 3100
	1    0    0    -1  
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:POT-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue RV2
U 1 1 5C3176AA
P 2700 2950
F 0 "RV2" V 2525 2950 50  0000 C CNN
F 1 "LOW-20K" V 2600 2950 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 2700 2950 50  0001 C CNN
F 3 "" H 2700 2950 50  0001 C CNN
	1    2700 2950
	1    0    0    1   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue C1
U 1 1 5CCEB1B0
P 6300 9000
F 0 "C1" H 6325 9100 50  0000 L CNN
F 1 "100n" H 6325 8900 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 6338 8850 50  0001 C CNN
F 3 "" H 6300 9000 50  0000 C CNN
	1    6300 9000
	1    0    0    -1  
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue R9
U 1 1 5D2AE37C
P 5800 8800
F 0 "R9" V 5880 8800 50  0000 C CNN
F 1 "2R2" V 5800 8800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5730 8800 50  0001 C CNN
F 3 "" H 5800 8800 50  0000 C CNN
	1    5800 8800
	0    1    1    0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue R10
U 1 1 5CCEB1B3
P 5800 9600
F 0 "R10" V 5880 9600 50  0000 C CNN
F 1 "2R2" V 5800 9600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5730 9600 50  0001 C CNN
F 3 "" H 5800 9600 50  0000 C CNN
	1    5800 9600
	0    1    1    0   
$EndComp
Text Label 6450 8800 0    60   ~ 0
+16v
Text Label 6450 9600 0    60   ~ 0
-16v
$Comp
L sommateur-potentiometre-4voies-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue J9
U 1 1 5D2AE37E
P 5100 8800
F 0 "J9" H 5100 9000 50  0000 C CNN
F 1 "alim" H 5100 8600 50  0000 C CNN
F 2 "resistance-grospad:CONN-3pin-2.54-TO-220-padcarre2mm" H 5100 8800 50  0001 C CNN
F 3 "" H 5100 8800 50  0001 C CNN
	1    5100 8800
	-1   0    0    1   
$EndComp
NoConn ~ 11300 1750
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C4
U 1 1 5D2B96AD
P 1550 2750
F 0 "C4" H 1575 2850 50  0000 L CNN
F 1 "10u" H 1575 2650 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 1588 2600 50  0001 C CNN
F 3 "" H 1550 2750 50  0000 C CNN
	1    1550 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 4650 6050 4100
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R13
U 1 1 5D2B96BA
P 6700 4650
F 0 "R13" V 6750 4650 50  0000 C CNN
F 1 "20k" V 6700 4650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 6630 4650 50  0001 C CNN
F 3 "" H 6700 4650 50  0000 C CNN
	1    6700 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 4650 7300 4650
Wire Wire Line
	7300 4650 7300 4000
Wire Wire Line
	7300 4000 7100 4000
Wire Wire Line
	6050 4650 6550 4650
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0104
U 1 1 5D2B96DE
P 6500 3900
F 0 "#PWR0104" H 6500 3650 50  0001 C CNN
F 1 "GND" H 6500 3750 50  0000 C CNN
F 2 "" H 6500 3900 50  0000 C CNN
F 3 "" H 6500 3900 50  0000 C CNN
	1    6500 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 2200 2700 2100
Wire Wire Line
	2700 2800 2700 2750
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R4
U 1 1 5D664531
P 3200 3800
F 0 "R4" V 3280 3800 50  0000 C CNN
F 1 "10k" V 3200 3800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3130 3800 50  0001 C CNN
F 3 "" H 3200 3800 50  0000 C CNN
	1    3200 3800
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0105
U 1 1 5D66453C
P 2700 3950
F 0 "#PWR0105" H 2700 3700 50  0001 C CNN
F 1 "GND" H 2700 3800 50  0000 C CNN
F 2 "" H 2700 3950 50  0000 C CNN
F 3 "" H 2700 3950 50  0000 C CNN
	1    2700 3950
	1    0    0    -1  
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:POT-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue RV3
U 1 1 5D664546
P 2700 3800
F 0 "RV3" V 2525 3800 50  0000 C CNN
F 1 "MID-20K" V 2600 3800 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 2700 3800 50  0001 C CNN
F 3 "" H 2700 3800 50  0001 C CNN
	1    2700 3800
	1    0    0    1   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R8
U 1 1 5D664550
P 3250 4400
F 0 "R8" V 3330 4400 50  0000 C CNN
F 1 "10k" V 3250 4400 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3180 4400 50  0001 C CNN
F 3 "" H 3250 4400 50  0000 C CNN
	1    3250 4400
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0106
U 1 1 5D66455A
P 2700 4550
F 0 "#PWR0106" H 2700 4300 50  0001 C CNN
F 1 "GND" H 2700 4400 50  0000 C CNN
F 2 "" H 2700 4550 50  0000 C CNN
F 3 "" H 2700 4550 50  0000 C CNN
	1    2700 4550
	1    0    0    -1  
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:POT-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue RV4
U 1 1 5D664564
P 2700 4400
F 0 "RV4" V 2525 4400 50  0000 C CNN
F 1 "HIGH-20K" V 2600 4400 50  0000 C CNN
F 2 "resistance-grospad:Potentiometer_Alps_RK163_Single_Vertical-padcarre" H 2700 4400 50  0001 C CNN
F 3 "" H 2700 4400 50  0001 C CNN
	1    2700 4400
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 3650 2700 3550
Wire Wire Line
	2700 4250 2700 4200
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R1
U 1 1 5D6724F1
P 3150 5650
F 0 "R1" V 3230 5650 50  0000 C CNN
F 1 "10k" V 3150 5650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 3080 5650 50  0001 C CNN
F 3 "" H 3150 5650 50  0000 C CNN
	1    3150 5650
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R5
U 1 1 5D6724FC
P 3200 6300
F 0 "R5" V 3280 6300 50  0000 C CNN
F 1 "10k" V 3200 6300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 3130 6300 50  0001 C CNN
F 3 "" H 3200 6300 50  0000 C CNN
	1    3200 6300
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R2
U 1 1 5D672537
P 3150 7100
F 0 "R2" V 3230 7100 50  0000 C CNN
F 1 "10k" V 3150 7100 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 3080 7100 50  0001 C CNN
F 3 "" H 3150 7100 50  0000 C CNN
	1    3150 7100
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R6
U 1 1 5D672542
P 3200 7750
F 0 "R6" V 3280 7750 50  0000 C CNN
F 1 "10k" V 3200 7750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 3130 7750 50  0001 C CNN
F 3 "" H 3200 7750 50  0000 C CNN
	1    3200 7750
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C2
U 1 1 5D6A5EB3
P 1600 3400
F 0 "C2" H 1625 3500 50  0000 L CNN
F 1 "10u" H 1625 3300 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 1638 3250 50  0001 C CNN
F 3 "" H 1600 3400 50  0000 C CNN
	1    1600 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 6700 6000 6150
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R11
U 1 1 5D6A5EC8
P 6650 6700
F 0 "R11" V 6700 6700 50  0000 C CNN
F 1 "20k" V 6650 6700 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 6580 6700 50  0001 C CNN
F 3 "" H 6650 6700 50  0000 C CNN
	1    6650 6700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 6700 7250 6700
Wire Wire Line
	7250 6700 7250 6050
Wire Wire Line
	7250 6050 7050 6050
Wire Wire Line
	6000 6700 6500 6700
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0107
U 1 1 5D6A5ED7
P 6450 5950
F 0 "#PWR0107" H 6450 5700 50  0001 C CNN
F 1 "GND" H 6450 5800 50  0000 C CNN
F 2 "" H 6450 5950 50  0000 C CNN
F 3 "" H 6450 5950 50  0000 C CNN
	1    6450 5950
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 6150 6450 6150
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C5
U 1 1 5D6A5F06
P 1600 3750
F 0 "C5" H 1625 3850 50  0000 L CNN
F 1 "10u" H 1625 3650 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 1638 3600 50  0001 C CNN
F 3 "" H 1600 3750 50  0000 C CNN
	1    1600 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 7950 6100 7400
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R14
U 1 1 5D6A5F1B
P 6750 7950
F 0 "R14" V 6800 7950 50  0000 C CNN
F 1 "20k" V 6750 7950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 6680 7950 50  0001 C CNN
F 3 "" H 6750 7950 50  0000 C CNN
	1    6750 7950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6900 7950 7350 7950
Wire Wire Line
	7350 7950 7350 7300
Wire Wire Line
	7350 7300 7150 7300
Wire Wire Line
	6100 7950 6600 7950
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0108
U 1 1 5D6A5F2A
P 6550 7200
F 0 "#PWR0108" H 6550 6950 50  0001 C CNN
F 1 "GND" H 6550 7050 50  0000 C CNN
F 2 "" H 6550 7200 50  0000 C CNN
F 3 "" H 6550 7200 50  0000 C CNN
	1    6550 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 5650 5400 5650
Wire Wire Line
	6050 2400 5400 2400
Connection ~ 6050 2400
Wire Wire Line
	3350 2350 5400 2350
Wire Wire Line
	5400 2350 5400 2400
Connection ~ 5400 2400
Wire Wire Line
	5400 2400 5400 5650
Wire Wire Line
	5200 2950 5200 4100
Wire Wire Line
	3350 6300 5200 6300
Wire Wire Line
	3400 2950 5200 2950
Wire Wire Line
	4950 3800 4950 6150
Wire Wire Line
	3350 3800 4950 3800
Wire Wire Line
	3300 7100 4950 7100
Wire Wire Line
	4700 4400 4700 7400
Wire Wire Line
	3400 4400 4700 4400
Wire Wire Line
	3350 7750 4700 7750
Wire Wire Line
	4700 7400 6100 7400
Connection ~ 4700 7400
Wire Wire Line
	4700 7400 4700 7750
Connection ~ 6100 7400
Wire Wire Line
	6100 7400 6550 7400
Wire Wire Line
	4950 6150 6000 6150
Connection ~ 4950 6150
Wire Wire Line
	4950 6150 4950 7100
Connection ~ 6000 6150
Wire Wire Line
	5200 4100 6050 4100
Connection ~ 5200 4100
Wire Wire Line
	5200 4100 5200 6300
Connection ~ 6050 4100
Wire Wire Line
	6050 4100 6500 4100
Text Label 5100 2350 0    50   ~ 0
sub
Text Label 5050 2950 0    50   ~ 0
low
Text Label 4800 3800 0    50   ~ 0
mid
Text Label 4550 4400 0    50   ~ 0
high
Text Label 2250 2100 0    50   ~ 0
sub-phono
Text Label 2250 2750 0    50   ~ 0
low-phono
Text Label 2250 3550 0    50   ~ 0
Mid-phono
Text Label 2250 4200 0    50   ~ 0
high-phono
Text Label 1650 5650 0    50   ~ 0
sub-effect
Text Label 1700 6300 0    50   ~ 0
low-effect
Text Label 1750 7100 0    50   ~ 0
mid-effect
Text Label 1700 7750 0    50   ~ 0
high-effect
$Comp
L sommateur-potentiometre-4voies-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue-eq3tone-rescue C6
U 1 1 5D707AD8
P 6600 9000
F 0 "C6" H 6625 9100 50  0000 L CNN
F 1 "100n" H 6625 8900 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 6638 8850 50  0001 C CNN
F 3 "" H 6600 9000 50  0000 C CNN
	1    6600 9000
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:NE5532 U1
U 2 1 5D5C6894
P 6800 4000
F 0 "U1" H 6800 4367 50  0000 C CNN
F 1 "NE5532" H 6800 4276 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6800 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne5532.pdf" H 6800 4000 50  0001 C CNN
	2    6800 4000
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:NE5532 U1
U 1 1 5D5D1204
P 6800 2300
F 0 "U1" H 6800 2667 50  0000 C CNN
F 1 "NE5532" H 6800 2576 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6800 2300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne5532.pdf" H 6800 2300 50  0001 C CNN
	1    6800 2300
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:NE5532 U2
U 1 1 5D5F9E9E
P 6750 6050
F 0 "U2" H 6750 6417 50  0000 C CNN
F 1 "NE5532" H 6750 6326 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6750 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne5532.pdf" H 6750 6050 50  0001 C CNN
	1    6750 6050
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:NE5532 U2
U 2 1 5D5FCE00
P 6850 7300
F 0 "U2" H 6850 7667 50  0000 C CNN
F 1 "NE5532" H 6850 7576 50  0000 C CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6850 7300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne5532.pdf" H 6850 7300 50  0001 C CNN
	2    6850 7300
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:NE5532 U2
U 3 1 5D6031B6
P 6900 9150
F 0 "U2" H 6858 9196 50  0000 L CNN
F 1 "NE5532" H 6858 9105 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 6900 9150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne5532.pdf" H 6900 9150 50  0001 C CNN
	3    6900 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 9450 6800 9600
$Comp
L Amplifier_Operational:NE5532 U1
U 3 1 5D60E884
P 7350 9150
F 0 "U1" H 7308 9196 50  0000 L CNN
F 1 "NE5532" H 7308 9105 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 7350 9150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne5532.pdf" H 7350 9150 50  0001 C CNN
	3    7350 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 8800 7250 8850
Wire Wire Line
	7250 9600 7250 9450
Wire Wire Line
	6600 9600 6800 9600
Connection ~ 6800 9600
Wire Wire Line
	6800 9600 7250 9600
Wire Wire Line
	6800 8800 7250 8800
Wire Wire Line
	6600 8800 6800 8800
Connection ~ 6800 8800
Wire Wire Line
	6800 8800 6800 8850
Wire Wire Line
	6800 8750 6800 8800
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0109
U 1 1 5D6ED57F
P 1000 5900
F 0 "#PWR0109" H 1000 5650 50  0001 C CNN
F 1 "GND" H 1000 5750 50  0000 C CNN
F 2 "" H 1000 5900 50  0000 C CNN
F 3 "" H 1000 5900 50  0000 C CNN
	1    1000 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 6300 2350 6300
Wire Wire Line
	1550 5650 2400 5650
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0110
U 1 1 5D850126
P 9700 4850
F 0 "#PWR0110" H 9700 4600 50  0001 C CNN
F 1 "GND" H 9700 4700 50  0000 C CNN
F 2 "" H 9700 4850 50  0000 C CNN
F 3 "" H 9700 4850 50  0000 C CNN
	1    9700 4850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x05_Female J1
U 1 1 606893A3
P 550 3300
F 0 "J1" H 442 2875 50  0000 C CNN
F 1 "inputs-MAIN" H 442 2966 50  0000 C CNN
F 2 "resistance-grospad:PinSocket_1x05_P2.54mm_Vertical" H 550 3300 50  0001 C CNN
F 3 "~" H 550 3300 50  0001 C CNN
	1    550  3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	950  2100 950  3200
Wire Wire Line
	950  3200 750  3200
Wire Wire Line
	1050 2750 1050 3300
Wire Wire Line
	1050 3300 750  3300
Wire Wire Line
	2150 3400 2150 3550
Wire Wire Line
	2150 3550 2700 3550
Wire Wire Line
	2700 2100 1550 2100
Wire Wire Line
	2700 2750 1700 2750
Wire Wire Line
	750  3400 1450 3400
Wire Wire Line
	2050 4200 2700 4200
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0111
U 1 1 606C0A59
P 750 3100
F 0 "#PWR0111" H 750 2850 50  0001 C CNN
F 1 "GND" H 750 2950 50  0000 C CNN
F 2 "" H 750 3100 50  0000 C CNN
F 3 "" H 750 3100 50  0000 C CNN
	1    750  3100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 606C42D5
P 10200 5050
F 0 "J2" H 10092 4625 50  0000 C CNN
F 1 "OUTPUTS" H 10092 4716 50  0000 C CNN
F 2 "resistance-grospad:PinSocket_1x05_P2.54mm_Vertical" H 10200 5050 50  0001 C CNN
F 3 "~" H 10200 5050 50  0001 C CNN
	1    10200 5050
	1    0    0    1   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R15
U 1 1 606C5BCB
P 9850 4850
F 0 "R15" V 9900 4850 50  0000 C CNN
F 1 "75" V 9850 4850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 9780 4850 50  0001 C CNN
F 3 "" H 9850 4850 50  0000 C CNN
	1    9850 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10000 5250 8950 5250
Wire Wire Line
	8950 5250 8950 7300
Wire Wire Line
	10000 5150 8700 5150
Wire Wire Line
	10000 4950 8250 4950
Wire Wire Line
	8250 4950 8250 2300
Connection ~ 6600 8800
Wire Wire Line
	6150 8750 6150 8800
Connection ~ 6150 8800
Wire Wire Line
	5950 8800 6150 8800
Wire Wire Line
	6300 8800 6300 8850
Wire Wire Line
	6150 8800 6300 8800
Wire Wire Line
	6300 8800 6600 8800
Connection ~ 6300 8800
$Comp
L sommateur-potentiometre-4voies-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue #PWR0112
U 1 1 6073CC81
P 5350 8650
F 0 "#PWR0112" H 5350 8400 50  0001 C CNN
F 1 "GND" H 5350 8500 50  0000 C CNN
F 2 "" H 5350 8650 50  0000 C CNN
F 3 "" H 5350 8650 50  0000 C CNN
	1    5350 8650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 8800 5650 8800
Wire Wire Line
	1250 2100 950  2100
Wire Wire Line
	1400 2750 1050 2750
Wire Wire Line
	1750 3400 2150 3400
Wire Wire Line
	1750 3750 2050 3750
Wire Wire Line
	2050 3750 2050 4200
Wire Wire Line
	750  3500 1300 3500
Wire Wire Line
	1300 3500 1300 3750
Wire Wire Line
	1300 3750 1450 3750
Wire Wire Line
	2850 2350 3050 2350
Wire Wire Line
	2850 2950 3100 2950
Wire Wire Line
	2850 3800 3050 3800
Wire Wire Line
	2850 4400 3100 4400
$Comp
L Connector:Conn_01x05_Female J3
U 1 1 607FCC0A
P 800 6100
F 0 "J3" H 692 5675 50  0000 C CNN
F 1 "inputs-AUX" H 692 5766 50  0000 C CNN
F 2 "resistance-grospad:PinSocket_1x05_P2.54mm_Vertical" H 800 6100 50  0001 C CNN
F 3 "~" H 800 6100 50  0001 C CNN
	1    800  6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	1000 6000 1550 6000
Wire Wire Line
	1550 6000 1550 5650
Wire Wire Line
	1000 6100 1550 6100
Wire Wire Line
	1550 6100 1550 6300
Wire Wire Line
	1000 6200 1450 6200
Wire Wire Line
	1450 6200 1450 7100
Wire Wire Line
	1450 7100 2350 7100
Wire Wire Line
	1000 6300 1350 6300
Wire Wire Line
	1350 6300 1350 7750
Wire Wire Line
	1350 7750 2300 7750
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C10
U 1 1 60851CC4
P 2550 5650
F 0 "C10" H 2575 5750 50  0000 L CNN
F 1 "10u" H 2575 5550 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 2588 5500 50  0001 C CNN
F 3 "" H 2550 5650 50  0000 C CNN
	1    2550 5650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 5650 3000 5650
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C8
U 1 1 608555E9
P 2500 6300
F 0 "C8" H 2525 6400 50  0000 L CNN
F 1 "10u" H 2525 6200 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 2538 6150 50  0001 C CNN
F 3 "" H 2500 6300 50  0000 C CNN
	1    2500 6300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2650 6300 3050 6300
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C9
U 1 1 60855F07
P 2500 7100
F 0 "C9" H 2525 7200 50  0000 L CNN
F 1 "10u" H 2525 7000 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 2538 6950 50  0001 C CNN
F 3 "" H 2500 7100 50  0000 C CNN
	1    2500 7100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2650 7100 3000 7100
$Comp
L sommateur-potentiometre-4voies-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue C7
U 1 1 6085AC8A
P 2450 7750
F 0 "C7" H 2475 7850 50  0000 L CNN
F 1 "10u" H 2475 7650 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD" H 2488 7600 50  0001 C CNN
F 3 "" H 2450 7750 50  0000 C CNN
	1    2450 7750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2600 7750 3050 7750
Wire Wire Line
	7300 2300 7550 2300
Connection ~ 7300 2300
Wire Wire Line
	7800 5050 7800 4000
Wire Wire Line
	7800 4000 7700 4000
Connection ~ 7300 4000
Wire Wire Line
	7250 6050 7700 6050
Wire Wire Line
	8700 5150 8700 6050
Connection ~ 7250 6050
Wire Wire Line
	7350 7300 7700 7300
Connection ~ 7350 7300
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R17
U 1 1 60884354
P 7700 2300
F 0 "R17" V 7750 2300 50  0000 C CNN
F 1 "100" V 7700 2300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7630 2300 50  0001 C CNN
F 3 "" H 7700 2300 50  0000 C CNN
	1    7700 2300
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R16
U 1 1 60884B17
P 7550 4000
F 0 "R16" V 7600 4000 50  0000 C CNN
F 1 "100" V 7550 4000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7480 4000 50  0001 C CNN
F 3 "" H 7550 4000 50  0000 C CNN
	1    7550 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7400 4000 7300 4000
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R18
U 1 1 60885216
P 7850 6050
F 0 "R18" V 7900 6050 50  0000 C CNN
F 1 "100" V 7850 6050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7780 6050 50  0001 C CNN
F 3 "" H 7850 6050 50  0000 C CNN
	1    7850 6050
	0    -1   -1   0   
$EndComp
$Comp
L sommateur-potentiometre-4voies-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-eq3tone-rescue R19
U 1 1 60886958
P 7850 7300
F 0 "R19" V 7900 7300 50  0000 C CNN
F 1 "100" V 7850 7300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7780 7300 50  0001 C CNN
F 3 "" H 7850 7300 50  0000 C CNN
	1    7850 7300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8000 7300 8950 7300
Wire Wire Line
	8000 6050 8700 6050
Wire Wire Line
	10000 5050 7800 5050
Wire Wire Line
	7850 2300 8250 2300
$EndSCHEMATC
