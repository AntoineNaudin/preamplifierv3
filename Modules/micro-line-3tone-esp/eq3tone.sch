EESchema Schematic File Version 4
LIBS:eq3tone-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R12
U 1 1 5968A4C6
P 9900 7400
F 0 "R12" V 9980 7400 50  0000 C CNN
F 1 "10k" V 9900 7400 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9830 7400 50  0001 C CNN
F 3 "" H 9900 7400 50  0000 C CNN
	1    9900 7400
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R15
U 1 1 5968AD4B
P 10500 7500
F 0 "R15" V 10580 7500 50  0000 C CNN
F 1 "75" V 10500 7500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 10430 7500 50  0001 C CNN
F 3 "" H 10500 7500 50  0000 C CNN
	1    10500 7500
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5968AF92
P 10350 7500
F 0 "#PWR?" H 10350 7250 50  0001 C CNN
F 1 "GND" H 10350 7350 50  0000 C CNN
F 2 "" H 10350 7500 50  0000 C CNN
F 3 "" H 10350 7500 50  0000 C CNN
	1    10350 7500
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R13
U 1 1 596B6EA9
P 9900 7900
F 0 "R13" V 9980 7900 50  0000 C CNN
F 1 "10k" V 9900 7900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9830 7900 50  0001 C CNN
F 3 "" H 9900 7900 50  0000 C CNN
	1    9900 7900
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R16
U 1 1 596B8C58
P 10500 8000
F 0 "R16" V 10580 8000 50  0000 C CNN
F 1 "75" V 10500 8000 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 10430 8000 50  0001 C CNN
F 3 "" H 10500 8000 50  0000 C CNN
	1    10500 8000
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 596B8C5E
P 10350 8000
F 0 "#PWR?" H 10350 7750 50  0001 C CNN
F 1 "GND" H 10350 7850 50  0000 C CNN
F 2 "" H 10350 8000 50  0000 C CNN
F 3 "" H 10350 8000 50  0000 C CNN
	1    10350 8000
	0    1    1    0   
$EndComp
Text Label 10900 7400 0    60   ~ 0
MAS
Text Label 10800 7900 0    60   ~ 0
AUX1
Text Label 10900 8500 0    60   ~ 0
AUX2
Text Label 10750 7500 0    60   ~ 0
GND-MAS
Text Label 10750 8000 0    60   ~ 0
GND-AUX
NoConn ~ 12550 5200
NoConn ~ 12550 5350
Wire Wire Line
	10650 7500 11200 7500
Wire Wire Line
	8650 7400 9750 7400
Wire Wire Line
	8650 7750 8650 8350
Wire Wire Line
	8650 7750 9400 7750
Connection ~ 8650 7750
Wire Wire Line
	10650 8000 11200 8000
Wire Wire Line
	10050 7400 11200 7400
Wire Wire Line
	10050 7900 11200 7900
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5AE70E17
P 9400 8050
F 0 "#PWR?" H 9400 7800 50  0001 C CNN
F 1 "GND" H 9400 7900 50  0000 C CNN
F 2 "" H 9400 8050 50  0000 C CNN
F 3 "" H 9400 8050 50  0000 C CNN
	1    9400 8050
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J3
U 1 1 5C314EF7
P 11400 7400
F 0 "J3" H 11400 7500 50  0000 C CNN
F 1 "MASTER-BUS" H 11750 7350 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 11400 7400 50  0001 C CNN
F 3 "" H 11400 7400 50  0001 C CNN
	1    11400 7400
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV6
U 1 1 5C315B55
P 9400 7900
F 0 "RV6" V 9225 7900 50  0000 C CNN
F 1 "FX1-10K" V 9300 7900 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 9400 7900 50  0001 C CNN
F 3 "" H 9400 7900 50  0001 C CNN
	1    9400 7900
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R14
U 1 1 5C317698
P 9950 8500
F 0 "R14" V 10030 8500 50  0000 C CNN
F 1 "10k" V 9950 8500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 9880 8500 50  0001 C CNN
F 3 "" H 9950 8500 50  0000 C CNN
	1    9950 8500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 8350 9400 8350
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5C3176A4
P 9400 8650
F 0 "#PWR?" H 9400 8400 50  0001 C CNN
F 1 "GND" H 9400 8500 50  0000 C CNN
F 2 "" H 9400 8650 50  0000 C CNN
F 3 "" H 9400 8650 50  0000 C CNN
	1    9400 8650
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV7
U 1 1 5C3176AA
P 9400 8500
F 0 "RV7" V 9225 8500 50  0000 C CNN
F 1 "FX2-10K" V 9300 8500 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 9400 8500 50  0001 C CNN
F 3 "" H 9400 8500 50  0001 C CNN
	1    9400 8500
	1    0    0    1   
$EndComp
Wire Wire Line
	9550 7900 9750 7900
Wire Wire Line
	9550 8500 9800 8500
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV3
U 1 1 5C30E9E2
P 6050 8050
F 0 "RV3" V 5875 8050 50  0000 C CNN
F 1 "MID 10kB" V 5950 8050 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 6050 8050 50  0001 C CNN
F 3 "" H 6050 8050 50  0001 C CNN
	1    6050 8050
	0    -1   1    0   
$EndComp
Wire Wire Line
	3800 7500 3950 7500
Connection ~ 3950 7500
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C5
U 1 1 5AA40EBA
P 4150 8450
F 0 "C5" H 4175 8550 50  0000 L CNN
F 1 "22u" H 4175 8350 50  0000 L CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 4188 8300 50  0001 C CNN
F 3 "" H 4150 8450 50  0000 C CNN
	1    4150 8450
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5AA40AE0
P 6050 9550
F 0 "#PWR?" H 6050 9300 50  0001 C CNN
F 1 "GND" H 6050 9400 50  0000 C CNN
F 2 "" H 6050 9550 50  0000 C CNN
F 3 "" H 6050 9550 50  0000 C CNN
	1    6050 9550
	1    0    0    -1  
$EndComp
NoConn ~ 6900 7150
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue U2
U 2 1 596AAFA6
P 5250 9300
F 0 "U2" H 5200 9500 50  0000 L CNN
F 1 "tl072" H 5200 9050 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 5250 9300 50  0001 C CNN
F 3 "" H 5250 9300 50  0000 C CNN
	2    5250 9300
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue C9
U 1 1 5967EF4B
P 7850 7400
F 0 "C9" H 7875 7500 50  0000 L CNN
F 1 "10u" H 7875 7300 50  0000 L CNN
F 2 "resistance-grospad:CP_Radial_D13.0mm_P100mm-pad2mm" H 7888 7250 50  0001 C CNN
F 3 "" H 7850 7400 50  0000 C CNN
	1    7850 7400
	0    -1   -1   0   
$EndComp
Text Notes 5000 9850 0    60   ~ 0
Freq HI MID\n
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV4
U 1 1 5CC62F24
P 6050 8550
F 0 "RV4" V 5875 8550 50  0000 C CNN
F 1 "HIGH10k" V 5950 8550 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 6050 8550 50  0001 C CNN
F 3 "" H 6050 8550 50  0001 C CNN
	1    6050 8550
	0    -1   1    0   
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV2
U 1 1 5CC62F8A
P 6050 7500
F 0 "RV2" V 5875 7500 50  0000 C CNN
F 1 "BASS10k" V 5950 7500 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 6050 7500 50  0001 C CNN
F 3 "" H 6050 7500 50  0001 C CNN
	1    6050 7500
	0    -1   1    0   
$EndComp
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue U1
U 2 1 596AAB42
P 7200 7400
F 0 "U1" H 7150 7600 50  0000 L CNN
F 1 "ne5532" H 7150 7150 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 7200 7400 50  0001 C CNN
F 3 "" H 7200 7400 50  0000 C CNN
	2    7200 7400
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R8
U 1 1 5CC6B835
P 4300 7500
F 0 "R8" V 4350 7500 50  0000 C CNN
F 1 "2k7" V 4300 7500 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 4230 7500 50  0001 C CNN
F 3 "" H 4300 7500 50  0000 C CNN
	1    4300 7500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4450 7500 5800 7500
Wire Wire Line
	5800 8050 5900 8050
Connection ~ 5800 7500
Wire Wire Line
	5800 7500 5900 7500
Wire Wire Line
	5800 8550 5900 8550
Wire Wire Line
	5800 7500 5800 8050
Connection ~ 5800 8050
Wire Wire Line
	5800 8050 5800 8550
Wire Wire Line
	6200 7500 6450 7500
Wire Wire Line
	6200 8050 6450 8050
Wire Wire Line
	6450 8050 6450 7500
Connection ~ 6450 7500
Wire Wire Line
	6450 7500 6900 7500
Wire Wire Line
	6200 8550 6450 8550
Wire Wire Line
	6450 8550 6450 8050
Connection ~ 6450 8050
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R11
U 1 1 5CC7B74D
P 7100 8050
F 0 "R11" V 7150 8050 50  0000 C CNN
F 1 "2k7" V 7100 8050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 7030 8050 50  0001 C CNN
F 3 "" H 7100 8050 50  0000 C CNN
	1    7100 8050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7250 8050 7700 8050
Wire Wire Line
	7700 8050 7700 7400
Wire Wire Line
	7700 7400 7500 7400
Wire Wire Line
	6450 8050 6950 8050
Wire Wire Line
	6900 7300 5800 7300
Wire Wire Line
	5800 7300 5800 7500
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue C8
U 1 1 5CC86B37
P 6050 9000
F 0 "C8" H 6075 9100 50  0000 L CNN
F 1 "22n" H 6075 8900 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L11.0mm_W2.7mm_P11mm_MKT-grospad-padcarre2mm" H 6088 8850 50  0001 C CNN
F 3 "" H 6050 9000 50  0000 C CNN
	1    6050 9000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 8700 6050 8850
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R10
U 1 1 5CC89A2A
P 6050 9400
F 0 "R10" V 6100 9400 50  0000 C CNN
F 1 "470" V 6050 9400 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 5980 9400 50  0001 C CNN
F 3 "" H 6050 9400 50  0000 C CNN
	1    6050 9400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 9150 6050 9250
Wire Wire Line
	4950 9400 4850 9400
Wire Wire Line
	4850 9400 4850 9950
Wire Wire Line
	4850 9950 5550 9950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R9
U 1 1 5CC990C4
P 5550 8850
F 0 "R9" V 5600 8850 50  0000 C CNN
F 1 "470" V 5550 8850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 5480 8850 50  0001 C CNN
F 3 "" H 5550 8850 50  0000 C CNN
	1    5550 8850
	1    0    0    -1  
$EndComp
Connection ~ 5550 9300
Wire Wire Line
	6050 8200 5550 8200
Wire Wire Line
	5550 8200 5550 8250
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue C7
U 1 1 5CCA34DB
P 5550 8400
F 0 "C7" H 5575 8500 50  0000 L CNN
F 1 "22n" H 5575 8300 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L7.0mm_W2.5mm_P5.00mm-grospad" H 5588 8250 50  0001 C CNN
F 3 "" H 5550 8400 50  0000 C CNN
	1    5550 8400
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue C6
U 1 1 5CCA37A8
P 4500 9200
F 0 "C6" H 4525 9300 50  0000 L CNN
F 1 "10n" H 4525 9100 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 4538 9050 50  0001 C CNN
F 3 "" H 4500 9200 50  0000 C CNN
	1    4500 9200
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 8600 4350 9200
Wire Wire Line
	5550 8550 5550 8600
Wire Wire Line
	4350 8600 5550 8600
Connection ~ 5550 8600
Wire Wire Line
	5550 8600 5550 8700
Wire Wire Line
	4650 9200 4700 9200
Wire Wire Line
	5550 9300 5550 9950
Wire Wire Line
	5550 9000 5550 9300
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R7
U 1 1 5CCC49BA
P 4700 9550
F 0 "R7" V 4750 9550 50  0000 C CNN
F 1 "56k" V 4700 9550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM15mm-padcarre2mm" V 4630 9550 50  0001 C CNN
F 3 "" H 4700 9550 50  0000 C CNN
	1    4700 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 9400 4700 9200
Connection ~ 4700 9200
Wire Wire Line
	4700 9200 4950 9200
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV1
U 1 1 5CCC8A03
P 4700 9850
F 0 "RV1" V 4525 9850 50  0000 C CNN
F 1 "MidFreq 1M" V 4600 9850 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 4700 9850 50  0001 C CNN
F 3 "" H 4700 9850 50  0001 C CNN
	1    4700 9850
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 9850 4350 9850
Wire Wire Line
	4350 9850 4350 10150
Wire Wire Line
	4350 10150 4700 10150
Wire Wire Line
	4700 10150 4700 10000
Wire Wire Line
	4700 10150 4700 10200
Connection ~ 4700 10150
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5CCD0FBF
P 4700 10200
F 0 "#PWR?" H 4700 9950 50  0001 C CNN
F 1 "GND" H 4700 10050 50  0000 C CNN
F 2 "" H 4700 10200 50  0000 C CNN
F 3 "" H 4700 10200 50  0000 C CNN
	1    4700 10200
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue U2
U 1 1 5CCD1D0E
P 3850 9350
F 0 "U2" H 3800 9550 50  0000 L CNN
F 1 "tl072" H 3800 9100 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3850 9350 50  0001 C CNN
F 3 "" H 3850 9350 50  0000 C CNN
	1    3850 9350
	1    0    0    -1  
$EndComp
Text Notes 6300 8050 0    60   ~ 0
Mid
Wire Wire Line
	3550 9450 3450 9450
Wire Wire Line
	3450 9450 3450 10000
Wire Wire Line
	3450 10000 4150 10000
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R6
U 1 1 5CCD1D19
P 4150 8900
F 0 "R6" V 4200 8900 50  0000 C CNN
F 1 "470" V 4150 8900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4080 8900 50  0001 C CNN
F 3 "" H 4150 8900 50  0000 C CNN
	1    4150 8900
	1    0    0    -1  
$EndComp
Connection ~ 4150 9350
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue C4
U 1 1 5CCD1D26
P 3100 9250
F 0 "C4" H 3125 9350 50  0000 L CNN
F 1 "68n" H 3125 9150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3138 9100 50  0001 C CNN
F 3 "" H 3100 9250 50  0000 C CNN
	1    3100 9250
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 8650 2950 9250
Wire Wire Line
	4150 8600 4150 8650
Wire Wire Line
	2950 8650 4150 8650
Connection ~ 4150 8650
Wire Wire Line
	4150 8650 4150 8750
Wire Wire Line
	3250 9250 3300 9250
Wire Wire Line
	4150 9350 4150 10000
Wire Wire Line
	4150 9050 4150 9350
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R4
U 1 1 5CCD1D34
P 3300 9600
F 0 "R4" V 3350 9600 50  0000 C CNN
F 1 "56k" V 3300 9600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3230 9600 50  0001 C CNN
F 3 "" H 3300 9600 50  0000 C CNN
	1    3300 9600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 9450 3300 9250
Connection ~ 3300 9250
Wire Wire Line
	3300 9250 3550 9250
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5CCD1D49
P 3300 9950
F 0 "#PWR?" H 3300 9700 50  0001 C CNN
F 1 "GND" H 3300 9800 50  0000 C CNN
F 2 "" H 3300 9950 50  0000 C CNN
F 3 "" H 3300 9950 50  0000 C CNN
	1    3300 9950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 9750 3300 9950
Wire Wire Line
	4150 8300 4150 7700
Wire Wire Line
	4150 7700 6050 7700
Wire Wire Line
	6050 7700 6050 7650
Wire Wire Line
	3950 7500 4150 7500
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R5
U 1 1 5CCEC1EC
P 3950 7650
F 0 "R5" V 4030 7650 50  0000 C CNN
F 1 "100k" V 3950 7650 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3880 7650 50  0001 C CNN
F 3 "" H 3950 7650 50  0000 C CNN
	1    3950 7650
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5CCEC1F2
P 3950 7800
F 0 "#PWR?" H 3950 7550 50  0001 C CNN
F 1 "GND" H 3950 7650 50  0000 C CNN
F 2 "" H 3950 7800 50  0000 C CNN
F 3 "" H 3950 7800 50  0000 C CNN
	1    3950 7800
	1    0    0    -1  
$EndComp
Connection ~ 7700 7400
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue RV5
U 1 1 5CD2D3A4
P 3650 7500
F 0 "RV5" V 3475 7500 50  0000 C CNN
F 1 "volume" V 3550 7500 50  0000 C CNN
F 2 "resistance-grospad:pot-RK163-grospad" H 3650 7500 50  0001 C CNN
F 3 "" H 3650 7500 50  0001 C CNN
	1    3650 7500
	1    0    0    1   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5CD31BB1
P 3650 7650
F 0 "#PWR?" H 3650 7400 50  0001 C CNN
F 1 "GND" H 3650 7500 50  0000 C CNN
F 2 "" H 3650 7650 50  0000 C CNN
F 3 "" H 3650 7650 50  0000 C CNN
	1    3650 7650
	1    0    0    -1  
$EndComp
Text GLabel 3750 9050 1    60   Input ~ 0
+v
Text GLabel 3750 9650 3    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J4
U 1 1 5CCB4263
P 11400 7900
F 0 "J4" H 11400 8000 50  0000 C CNN
F 1 "AUX1" H 11750 7850 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 11400 7900 50  0001 C CNN
F 3 "" H 11400 7900 50  0001 C CNN
	1    11400 7900
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue R17
U 1 1 5CCBE6D8
P 10550 8600
F 0 "R17" V 10630 8600 50  0000 C CNN
F 1 "75" V 10550 8600 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 10480 8600 50  0001 C CNN
F 3 "" H 10550 8600 50  0000 C CNN
	1    10550 8600
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue #PWR?
U 1 1 5CCBE6DE
P 10400 8600
F 0 "#PWR?" H 10400 8350 50  0001 C CNN
F 1 "GND" H 10400 8450 50  0000 C CNN
F 2 "" H 10400 8600 50  0000 C CNN
F 3 "" H 10400 8600 50  0000 C CNN
	1    10400 8600
	0    1    1    0   
$EndComp
Text Label 10800 8600 0    60   ~ 0
GND-AUX2
Wire Wire Line
	10700 8600 11250 8600
Wire Wire Line
	10100 8500 11250 8500
$Comp
L eq3tone-rescue:Conn_01x02-eq3tone-rescue-eq3tone-rescue J5
U 1 1 5CCBE6E8
P 11450 8500
F 0 "J5" H 11450 8600 50  0000 C CNN
F 1 "AUX2" H 11800 8450 50  0000 C CNN
F 2 "resistance-grospad:LED_D2.0mm_W4.0mm_H2.8mm_FlatTop-pad2mm" H 11450 8500 50  0001 C CNN
F 3 "" H 11450 8500 50  0001 C CNN
	1    11450 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 7400 8650 7750
Wire Wire Line
	8000 7400 8650 7400
Connection ~ 8650 7400
Text Label 550  1450 0    60   ~ 0
IN+
NoConn ~ 1250 -500
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C15
U 1 1 5CCEB1B0
P 9600 13950
F 0 "C15" H 9625 14050 50  0000 L CNN
F 1 "100n" H 9625 13850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 9638 13800 50  0001 C CNN
F 3 "" H 9600 13950 50  0000 C CNN
	1    9600 13950
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C17
U 1 1 59666BE9
P 9900 13950
F 0 "C17" H 9925 14050 50  0000 L CNN
F 1 "100n" H 9925 13850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 9938 13800 50  0001 C CNN
F 3 "" H 9900 13950 50  0000 C CNN
	1    9900 13950
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R1
U 1 1 5CCEB1B2
P 9100 13750
F 0 "R1" V 9180 13750 50  0000 C CNN
F 1 "2R2" V 9100 13750 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 9030 13750 50  0001 C CNN
F 3 "" H 9100 13750 50  0000 C CNN
	1    9100 13750
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R2
U 1 1 5CCEB1B3
P 9100 14550
F 0 "R2" V 9180 14550 50  0000 C CNN
F 1 "2R2" V 9100 14550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 9030 14550 50  0001 C CNN
F 3 "" H 9100 14550 50  0000 C CNN
	1    9100 14550
	0    1    1    0   
$EndComp
Text GLabel 10200 13750 2    60   Input ~ 0
+v
Text GLabel 10200 14550 2    60   Input ~ 0
-v
Text Label 9750 13750 0    60   ~ 0
+16v
Text Label 9750 14550 0    60   ~ 0
-16v
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR?
U 1 1 5AA3E982
P 300 1550
F 0 "#PWR?" H 300 1300 50  0001 C CNN
F 1 "GND" H 300 1400 50  0000 C CNN
F 2 "" H 300 1550 50  0000 C CNN
F 3 "" H 300 1550 50  0000 C CNN
	1    300  1550
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG?
U 1 1 5AA41643
P 8950 14150
F 0 "#FLG?" H 8950 14245 50  0001 C CNN
F 1 "PWR_FLAG" H 8950 14330 50  0000 C CNN
F 2 "" H 8950 14150 50  0000 C CNN
F 3 "" H 8950 14150 50  0000 C CNN
	1    8950 14150
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG?
U 1 1 5AA41910
P 9450 13700
F 0 "#FLG?" H 9450 13795 50  0001 C CNN
F 1 "PWR_FLAG" H 9450 13880 50  0000 C CNN
F 2 "" H 9450 13700 50  0000 C CNN
F 3 "" H 9450 13700 50  0000 C CNN
	1    9450 13700
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:PWR_FLAG-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #FLG?
U 1 1 5CD30825
P 9400 14550
F 0 "#FLG?" H 9400 14645 50  0001 C CNN
F 1 "PWR_FLAG" H 9400 14730 50  0000 C CNN
F 2 "" H 9400 14550 50  0000 C CNN
F 3 "" H 9400 14550 50  0000 C CNN
	1    9400 14550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 14550 8950 14550
Wire Wire Line
	8800 13750 8950 13750
Wire Wire Line
	9250 14550 9400 14550
Wire Wire Line
	9900 14550 10100 14550
Wire Wire Line
	9900 13750 9900 13800
Connection ~ 9600 13750
Wire Wire Line
	9600 13750 9600 13800
Wire Wire Line
	9250 13750 9450 13750
Wire Wire Line
	8800 13950 8800 14550
Wire Wire Line
	9450 13700 9450 13750
Wire Wire Line
	9400 14550 9400 14650
Connection ~ 9400 14550
Wire Wire Line
	8950 13850 8950 14150
Connection ~ 8950 14150
Wire Wire Line
	8600 13950 8800 13950
Wire Wire Line
	8600 13850 8800 13850
Wire Wire Line
	8800 13850 8800 13750
Wire Wire Line
	8600 13750 8600 13700
Wire Wire Line
	8600 13700 8900 13700
Wire Wire Line
	8900 13700 8900 13850
Wire Wire Line
	8900 13850 8950 13850
$Comp
L eq3tone-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue J1
U 1 1 5CCEB1BC
P 8400 13850
F 0 "J1" H 8400 14050 50  0000 C CNN
F 1 "alim" H 8400 13650 50  0000 C CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 8400 13850 50  0001 C CNN
F 3 "" H 8400 13850 50  0001 C CNN
	1    8400 13850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 1900 3250 1900
Wire Wire Line
	3250 1200 3250 1250
Wire Wire Line
	3200 1200 3250 1200
Wire Wire Line
	3250 1900 3250 1850
Text GLabel 3200 1200 0    60   Input ~ 0
+v
Text GLabel 3200 1900 0    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue U3
U 2 1 5CCEB1BB
P 3350 1550
F 0 "U3" H 3300 1750 50  0000 L CNN
F 1 "ne5532" H 3300 1300 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3350 1550 50  0001 C CNN
F 3 "" H 3350 1550 50  0000 C CNN
	2    3350 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 14100 9600 14550
Wire Wire Line
	9900 14100 9900 14550
Connection ~ 9600 14550
Wire Wire Line
	3050 1450 1800 1450
$Comp
L eq3tone-rescue:Conn_01x03-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue J6
U 1 1 5CCE1688
P 100 1550
F 0 "J6" H 100 1750 50  0000 C CNN
F 1 "input" H 100 1350 50  0000 C CNN
F 2 "resistance-grospad:TO-220-padcarre2mm" H 100 1550 50  0001 C CNN
F 3 "" H 100 1550 50  0001 C CNN
	1    100  1550
	-1   0    0    1   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R21
U 1 1 5CCF460B
P 1550 1450
F 0 "R21" V 1600 1450 50  0000 C CNN
F 1 "220" V 1550 1450 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM15mm-padcarre2mm" V 1480 1450 50  0001 C CNN
F 3 "" H 1550 1450 50  0000 C CNN
	1    1550 1450
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C11
U 1 1 5CD00B6C
P 3200 2100
F 0 "C11" H 3225 2200 50  0000 L CNN
F 1 "22p" H 3225 2000 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3238 1950 50  0001 C CNN
F 3 "" H 3200 2100 50  0000 C CNN
	1    3200 2100
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R23
U 1 1 5CD00C00
P 3200 2400
F 0 "R23" V 3250 2400 50  0000 C CNN
F 1 "10k" V 3200 2400 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 3130 2400 50  0001 C CNN
F 3 "" H 3200 2400 50  0000 C CNN
	1    3200 2400
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R25
U 1 1 5CD00D3A
P 3900 1550
F 0 "R25" V 3950 1550 50  0000 C CNN
F 1 "10k" V 3900 1550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM12mm-PAS2MM" V 3830 1550 50  0001 C CNN
F 3 "" H 3900 1550 50  0000 C CNN
	1    3900 1550
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R18
U 1 1 5CD00DCA
P 1150 1800
F 0 "R18" V 1200 1800 50  0000 C CNN
F 1 "2k2" V 1150 1800 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM7mm-pad2mm" V 1080 1800 50  0001 C CNN
F 3 "" H 1150 1800 50  0000 C CNN
	1    1150 1800
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R3
U 1 1 5CD00F03
P 800 2050
F 0 "R3" V 850 2050 50  0000 C CNN
F 1 "100k" V 800 2050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 730 2050 50  0001 C CNN
F 3 "" H 800 2050 50  0000 C CNN
	1    800  2050
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R19
U 1 1 5CD0100F
P 1150 2300
F 0 "R19" V 1200 2300 50  0000 C CNN
F 1 "2k2" V 1150 2300 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1080 2300 50  0001 C CNN
F 3 "" H 1150 2300 50  0000 C CNN
	1    1150 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 1550 3700 1550
Wire Wire Line
	3700 1550 3700 2100
Wire Wire Line
	3700 2100 3350 2100
Connection ~ 3700 1550
Wire Wire Line
	3700 1550 3650 1550
Wire Wire Line
	3050 2100 2850 2100
Wire Wire Line
	2850 2100 2850 1650
Wire Wire Line
	2850 1650 3050 1650
Wire Wire Line
	3350 2400 3700 2400
Wire Wire Line
	3700 2400 3700 2100
Connection ~ 3700 2100
Wire Wire Line
	3050 2400 2850 2400
Wire Wire Line
	2850 2400 2850 2100
Connection ~ 2850 2100
Wire Wire Line
	1150 1650 1150 1450
Connection ~ 1150 1450
Wire Wire Line
	1150 1450 300  1450
Wire Wire Line
	950  2050 1150 2050
Wire Wire Line
	1150 1950 1150 2050
Connection ~ 1150 2050
Wire Wire Line
	1150 2050 1150 2150
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR?
U 1 1 5CD2B880
P 650 2050
F 0 "#PWR?" H 650 1800 50  0001 C CNN
F 1 "GND" H 650 1900 50  0000 C CNN
F 2 "" H 650 2050 50  0000 C CNN
F 3 "" H 650 2050 50  0000 C CNN
	1    650  2050
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:POT-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue RV8
U 1 1 5CD2B9FB
P 2450 2300
F 0 "RV8" V 2275 2300 50  0000 C CNN
F 1 "Gain" V 2350 2300 50  0000 C CNN
F 2 "resistance-grospad:conn3-grospad-petit" H 2450 2300 50  0001 C CNN
F 3 "" H 2450 2300 50  0001 C CNN
	1    2450 2300
	1    0    0    1   
$EndComp
Wire Wire Line
	3250 3400 3300 3400
Wire Wire Line
	3300 2700 3300 2750
Wire Wire Line
	3250 2700 3300 2700
Wire Wire Line
	3300 3400 3300 3350
Text GLabel 3250 2700 0    60   Input ~ 0
+v
Text GLabel 3250 3400 0    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue U3
U 1 1 5CD379A5
P 3400 3050
F 0 "U3" H 3350 3250 50  0000 L CNN
F 1 "ne5532" H 3350 2800 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 3400 3050 50  0001 C CNN
F 3 "" H 3400 3050 50  0000 C CNN
	1    3400 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2950 1800 2950
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R20
U 1 1 5CD379AC
P 1500 2950
F 0 "R20" V 1550 2950 50  0000 C CNN
F 1 "220" V 1500 2950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 1430 2950 50  0001 C CNN
F 3 "" H 1500 2950 50  0000 C CNN
	1    1500 2950
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C12
U 1 1 5CD379B2
P 3250 3600
F 0 "C12" H 3275 3700 50  0000 L CNN
F 1 "22p" H 3275 3500 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 3288 3450 50  0001 C CNN
F 3 "" H 3250 3600 50  0000 C CNN
	1    3250 3600
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R24
U 1 1 5CD379B8
P 3250 3900
F 0 "R24" V 3300 3900 50  0000 C CNN
F 1 "10k" V 3250 3900 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3180 3900 50  0001 C CNN
F 3 "" H 3250 3900 50  0000 C CNN
	1    3250 3900
	0    -1   -1   0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R26
U 1 1 5CD379BE
P 3950 3050
F 0 "R26" V 4000 3050 50  0000 C CNN
F 1 "10k" V 3950 3050 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 3880 3050 50  0001 C CNN
F 3 "" H 3950 3050 50  0000 C CNN
	1    3950 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3800 3050 3750 3050
Wire Wire Line
	3750 3050 3750 3600
Wire Wire Line
	3750 3600 3400 3600
Connection ~ 3750 3050
Wire Wire Line
	3750 3050 3700 3050
Wire Wire Line
	3100 3600 2900 3600
Wire Wire Line
	2900 3600 2900 3150
Wire Wire Line
	2900 3150 3100 3150
Wire Wire Line
	3400 3900 3750 3900
Wire Wire Line
	3750 3900 3750 3600
Connection ~ 3750 3600
Wire Wire Line
	3100 3900 2900 3900
Wire Wire Line
	2900 3900 2900 3600
Connection ~ 2900 3600
Wire Wire Line
	1150 2450 1150 2950
Wire Wire Line
	1150 2950 1350 2950
Wire Wire Line
	1150 2950 350  2950
Wire Wire Line
	350  2950 350  1650
Wire Wire Line
	350  1650 300  1650
Connection ~ 1150 2950
Text Label 600  2950 0    60   ~ 0
IN-
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C3
U 1 1 5CD4597A
P 1800 2050
F 0 "C3" H 1825 2150 50  0000 L CNN
F 1 "1n" H 1825 1950 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 1838 1900 50  0001 C CNN
F 3 "" H 1800 2050 50  0000 C CNN
	1    1800 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 1900 1800 1450
Connection ~ 1800 1450
Wire Wire Line
	1800 1450 1700 1450
Wire Wire Line
	1800 2200 1800 2950
Connection ~ 1800 2950
Wire Wire Line
	1800 2950 1650 2950
Wire Wire Line
	2600 2300 2700 2300
Wire Wire Line
	2700 2300 2700 2650
Wire Wire Line
	1400 1450 1150 1450
Wire Wire Line
	2850 1650 2450 1650
Wire Wire Line
	2450 1650 2450 2150
Connection ~ 2850 1650
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R22
U 1 1 5CD5698E
P 2450 2950
F 0 "R22" V 2500 2950 50  0000 C CNN
F 1 "100" V 2450 2950 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM16mm-padcarre2mm" V 2380 2950 50  0001 C CNN
F 3 "" H 2450 2950 50  0000 C CNN
	1    2450 2950
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C10
U 1 1 5CD56BE0
P 2650 3150
F 0 "C10" V 2905 3150 50  0000 C CNN
F 1 "470u" V 2500 3500 50  0000 C CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 2688 3000 50  0001 C CNN
F 3 "" H 2650 3150 50  0001 C CNN
	1    2650 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2900 3150 2800 3150
Connection ~ 2900 3150
Wire Wire Line
	2500 3150 2450 3150
Wire Wire Line
	2450 3150 2450 3100
Wire Wire Line
	2450 2450 2450 2650
Wire Wire Line
	2700 2650 2450 2650
Connection ~ 2450 2650
Wire Wire Line
	2450 2650 2450 2800
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R27
U 1 1 5CD6598B
P 4100 1850
F 0 "R27" V 4150 1850 50  0000 C CNN
F 1 "10k" V 4100 1850 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4030 1850 50  0001 C CNN
F 3 "" H 4100 1850 50  0000 C CNN
	1    4100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3050 5050 3050
Wire Wire Line
	5050 2350 5050 2400
Wire Wire Line
	5000 2350 5050 2350
Wire Wire Line
	5050 3050 5050 3000
Text GLabel 5000 2350 0    60   Input ~ 0
+v
Text GLabel 5000 3050 0    60   Input ~ 0
-v
$Comp
L eq3tone-rescue:TL072-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue U1
U 1 1 5CD660AD
P 5150 2700
F 0 "U1" H 5100 2900 50  0000 L CNN
F 1 "ne5532" H 5100 2450 50  0000 L CNN
F 2 "resistance-grospad:DIP-8_W7.62mm_GrosPads" H 5150 2700 50  0001 C CNN
F 3 "" H 5150 2700 50  0000 C CNN
	1    5150 2700
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C13
U 1 1 5CD660B4
P 5000 3250
F 0 "C13" H 5025 3350 50  0000 L CNN
F 1 "22p" H 5025 3150 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 5038 3100 50  0001 C CNN
F 3 "" H 5000 3250 50  0000 C CNN
	1    5000 3250
	0    1    1    0   
$EndComp
$Comp
L eq3tone-rescue:R-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue R28
U 1 1 5CD660BA
P 5000 3550
F 0 "R28" V 5050 3550 50  0000 C CNN
F 1 "10k" V 5000 3550 50  0000 C CNN
F 2 "resistance-grospad:Resistor_Horizontal_RM9mm-padcarre2mm" V 4930 3550 50  0001 C CNN
F 3 "" H 5000 3550 50  0000 C CNN
	1    5000 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 2700 5500 2700
Wire Wire Line
	5500 2700 5500 3250
Wire Wire Line
	5500 3250 5150 3250
Connection ~ 5500 2700
Wire Wire Line
	5500 2700 5450 2700
Wire Wire Line
	4850 3250 4650 3250
Wire Wire Line
	4650 3250 4650 3050
Wire Wire Line
	4650 2800 4850 2800
Wire Wire Line
	5150 3550 5500 3550
Wire Wire Line
	5500 3550 5500 3250
Connection ~ 5500 3250
Wire Wire Line
	4850 3550 4650 3550
Wire Wire Line
	4650 3550 4650 3250
Connection ~ 4650 3250
$Comp
L eq3tone-rescue:CP-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C14
U 1 1 5CD6F649
P 5700 2700
F 0 "C14" V 5955 2700 50  0000 C CNN
F 1 "10u" V 5550 3050 50  0000 C CNN
F 2 "resistance-grospad:C_Radial_D6.3_L11.2_P2.5-GROSPAD-ecarte" H 5738 2550 50  0001 C CNN
F 3 "" H 5700 2700 50  0001 C CNN
	1    5700 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 1550 4100 1550
Wire Wire Line
	4200 1550 4200 2600
Wire Wire Line
	4200 2600 4850 2600
Wire Wire Line
	4100 1700 4100 1550
Connection ~ 4100 1550
Wire Wire Line
	4100 1550 4200 1550
Wire Wire Line
	4100 3050 4650 3050
Connection ~ 4650 3050
Wire Wire Line
	4650 3050 4650 2800
$Comp
L eq3tone-rescue:GND-power-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue #PWR?
U 1 1 5CD7C747
P 4100 2000
F 0 "#PWR?" H 4100 1750 50  0001 C CNN
F 1 "GND" H 4100 1850 50  0000 C CNN
F 2 "" H 4100 2000 50  0000 C CNN
F 3 "" H 4100 2000 50  0000 C CNN
	1    4100 2000
	1    0    0    -1  
$EndComp
$Comp
L eq3tone-rescue:C-Device-eq3tone-rescue-eq3tone-rescue-micro-esp-rescue-eq3tone-rescue C16
U 1 1 59665F3D
P 10100 13950
F 0 "C16" H 10125 14050 50  0000 L CNN
F 1 "100n" H 10125 13850 50  0000 L CNN
F 2 "resistance-grospad:C_Rect_L9.0mm_W2.7mm_P7.50mm_MKT-grospad-padcarre2mm" H 10138 13800 50  0001 C CNN
F 3 "" H 10100 13950 50  0000 C CNN
	1    10100 13950
	1    0    0    -1  
$EndComp
Connection ~ 10100 13750
Wire Wire Line
	10100 13750 10100 13800
Wire Wire Line
	10100 14100 10100 14550
Connection ~ 10100 14550
Wire Wire Line
	6000 2700 5850 2700
Wire Wire Line
	6000 5750 3450 5750
Wire Wire Line
	3450 5750 3450 7350
Wire Wire Line
	6000 2700 6000 5750
Wire Wire Line
	3450 7350 3650 7350
Wire Wire Line
	9600 14550 9900 14550
Wire Wire Line
	9400 14550 9600 14550
Connection ~ 9450 13750
Wire Wire Line
	9600 13750 9900 13750
Wire Wire Line
	9450 13750 9600 13750
Connection ~ 9900 13750
Connection ~ 9900 14550
Wire Wire Line
	9950 13750 10100 13750
Wire Wire Line
	9900 13750 10100 13750
Wire Wire Line
	10100 13750 10400 13750
Wire Wire Line
	10100 14550 10400 14550
$Comp
L eq3tone-rescue:GNDD-power-riaav3-rescue #PWR?
U 1 1 5CCFA5FC
P 8950 14150
F 0 "#PWR?" H 8950 13900 50  0001 C CNN
F 1 "GNDD-power" H 8954 13995 50  0000 C CNN
F 2 "" H 8950 14150 50  0001 C CNN
F 3 "" H 8950 14150 50  0001 C CNN
	1    8950 14150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
